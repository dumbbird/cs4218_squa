# CS4218 Shell #

CS4218 Shell is a command interpreter that provides a set of tools (applications): *cd*, *pwd*, *ls*, *cat*, *echo*, *head*, *tail*, *grep*, *sed*, *find* and *wc*. Apart from that, CS4218 Shell is a language for calling and combining these application. The language supports *quoting* of input data, *semicolon operator* for calling sequences of applications, *command substitution* and *piping* for connecting applications' inputs and outputs, *IO-redirection* to load and save data processed by applications from/to files. More details can be found in "Project Description.pdf" in IVLE.

## Prerequisites ##

CS4218 Shell requires the following versions of software:

1. JDK 7
2. Eclipse 4.3
3. JUnit 4

Compiler compliance level must be <= 1.7

## Implementation ##

CS4218 Shell implementation has the following structure:

`sg.edu.nus.comp.cs4218` is the main package. It provides basic interfaces (`Application`, `Command`, `Shell`) and `Environment` class that is used to manage current working directory. A `Shell` implementation works the following way: it parses and expands user's command line producing an instance of `Command` class. For example, parsing `cat * | grep foo` can yield the following structure: `Pipe(Call(Cat(), ["a.txt", "b.txt"]), Call(Grep(), ["foo"]))`. After that, shell calls `Command.evaluate` method. An implementation of `Command` interface is responsible for calling applications in separate threads and supplying proper input/output streams. Implementations of `Application` interface provide application-specific functionality.

`sg.edu.nus.comp.cs4218.exception` contains definitions of the exceptions that can be thrown by shell and applications. `ShellException` is thrown when an error occurred during the execution of shell's functionality such as command line parsing. For each application, there is a separate exception that is inherited from `AbstractApplicationException` class.

Package `sg.edu.nus.comp.cs4218.impl` contains implementation classes. Implementation of applications is in `sg.edu.nus.comp.cs4218.impl.app` package, inplementation if shell commands is in `sg.edu.nus.comp.cs4218.impl.cmd`.

## Notice ##

We use UTF-8 to encode all our program codes. Therefore, we recommend to set Eclipse's text file encoding to UTF-8 before running the test cases in Junit.

Open "Window" -> "Preference" -> "General" -> "Workspace" and set "Text file encoding" to "UTF-8".

## Milestone 1 Task Assignment ##

###The Basic Functionality includes:###

####Shell:
 * calling applications, 	(test cases) 	- *** (Name hidden)
 * quoting, 				(test cases) 	- *** (Name hidden)
 * semicolon operator.		(test cases) 	- *** (Name hidden)

####Applications:
 * cat, echo, 			(test cases)	- *** (Name hidden)
 * head, tail,			(test cases)	- *** (Name hidden)
 * fmt, date.			(implement and test cases)	- *** (Name hidden)

###The Extended Functionality I includes:###

####Shell:
 * globbing, 				(test cases)	- *** (Name hidden)
 * pipe operator.			(test cases)	- *** (Name hidden)

####Applications:
 * sort, 					(test cases)	- *** (Name hidden)
 * comm.					(test cases)	- *** (Name hidden)

###The Extended Functionality II includes:###

####Shell:
 * IOredirection,			(implement and test cases)	- *** (Name hidden)
 * command substitution.	(implement and test cases)	- *** (Name hidden)

####Applications: 
 * bc, 					(implement and test cases)	- *** (Name hidden)
 * cal.					(implement and test cases)	- *** (Name hidden)

## Internal Timeline (MS1)##
###As of This Friday (19 Feb 2016):
 * Read and understand the structure of the source codes
 * Try to finish the implementation and test cases of BF assigned to each of us; Test cases need to be all passed with no bugs
 
###As of This Weekend (21 Feb 2016):
 * Finish writing test cases for EFI
 
###Next week (from 22 Feb 2016 to 29 Feb 2016):
 * Implement EFII
 * Write test cases for EFII
 * Make all test cases pass for EFII
 
## Milestone 2 Task Assignment ##

####Shell:
 * globbing, 				(implementation)	- *** (Name hidden)
 * pipe operator.			(implementation)	- *** (Name hidden)

####Applications:
 * sort, 					(implementation)	- *** (Name hidden)
 * comm.					(implementation)	- *** (Name hidden)
 
####Integration testing requirements:
 * [Each of us can take one task, more info please refer to Lab5-Integrate.pdf]
 * Chain of interactions integrate applications using command substitution (>=10 tests)
 * Chain of interactions integrate applications using piping ( >=10 tests)
 * Chain of applications connected with at least two pipes (>=4 tests)
 * Chain of applications connected with at least two command substitutions (>=4 tests)
 * For each case above, at least one corresponding negative scenario 
 
####Others [Code coverage, Randoop, etc.]
 * Code coverage			(in charge)			- *** (Name hidden)
 * Randoop					(in charge)			- *** (Name hidden)
 * Checking missing test cases for each func	- *** (Name hidden)

## Internal Timeline (MS2)##
###As of Next Wednesday (16 Mar 2016):
 * Finish all the implementation
 * Add more test cases to the functionalities (especially that are missed in MS1)
 * Improve code coverage (this is not mentioned how much code coverage we need to cover at least. I think I will post this query to IVLE and confirm with Prof.)
 
###Next Week (17 Mar 2016 - 21 Mar 2016):
 * Write integration testing cases
 * Write randoop test cases (to get bonus points)

###Feel free to comment about this internal timeline. Thanks. -- by *** (Name hidden)




