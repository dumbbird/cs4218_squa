package sg.edu.nus.comp.cs4218.exception;

public class CommException extends AbstractApplicationException {
	private static final long serialVersionUID = 2333796686823942499L;

	public CommException(String message) {
		super("comm: " + message);
	}
}
