package sg.edu.nus.comp.cs4218.exception;

public class DateException extends AbstractApplicationException {

	private static final long serialVersionUID = -5206758524312595175L;

	public DateException(String message) {
		super("date: " + message);
	}
}
