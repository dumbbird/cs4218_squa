package sg.edu.nus.comp.cs4218.exception;

public class BcException extends AbstractApplicationException {
	private static final long serialVersionUID = 2333796686823942499L;

	public BcException(String message) {
		super("bc: " + message);
	}
}
