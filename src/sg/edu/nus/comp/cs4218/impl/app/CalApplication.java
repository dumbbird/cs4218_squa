package sg.edu.nus.comp.cs4218.impl.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.CalException;

/**
 * The cal command prints out either the calendar of the current month, or month
 * specified by month and year, or the year specified by the user. There is an
 * option to print the calendar with the starting day of the week being Sunday
 * (default), or Monday.
 */
public class CalApplication implements Application {

	public static String[] months = { "", "January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December" };
	public static int[] days = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	public static boolean isLeapYear = false;
	public static String daysOfWeekSunFirst = "Su Mo Tu We Th Fr Sa\n";
	public static String daysOfWeekMonFirst = "Mo Tu We Th Fr Sa Su\n";
	public static String[] shortFormMonths = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept",
			"Oct", "Nov", "Dec" };
	public static int[] startDayMonth = new int[12];

	/**
	 * Generates the string of the current month to be printed.
	 * 
	 * @param args
	 *            Array of arguments for the application.
	 * 
	 * @return String The string of the current month.
	 */
	public String printCal(String[] args) {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		String result = printFormatMonth(month, year, daysOfWeekSunFirst, true);
		return result;
	}

	/**
	 * Checks if the specified year is a leap year.
	 * 
	 * @param year
	 *            The year which is to be checked.
	 * 
	 * @return boolean True if the year is a leap year and false if not.
	 */
	public boolean checkLeapYear(int year) {
		if ((year % 4 == 0) && (year % 100 != 0)) {
			return true;
		} else if (year % 400 == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Calculates which day of the week a specified day falls on using the given
	 * month in the given year.
	 * 
	 * @param startDay
	 *            Day of the month we want to calculate the day of the week it
	 *            falls on.
	 * 
	 * @param month
	 *            The month of the day to be found.
	 * @param year
	 *            The year when the day occurs in the month.
	 * @return int The day the specified day falls on, 0-Sun, 1-Mon etc.
	 */
	public int getStartDay(int startDay, int month, int year) {
		int y = year - (14 - month) / 12;
		int x = y + y / 4 - y / 100 + y / 400;
		int m = month + 12 * ((14 - month) / 12) - 2;
		int d = (startDay + x + (31 * m) / 12) % 7;
		return d;
	}

	/**
	 * Runs the cal application by sending the arguments to the appropriate
	 * functions.
	 * 
	 * @param args
	 *            Array of arguments for the application.
	 * 
	 * @param stdin
	 *            An InputStream.
	 *
	 * @param stdout
	 *            An OutputStream where the output of the application is written
	 *            to.
	 * 
	 * @throws CalException
	 *             If invalid input is specified for start of week, year or
	 *             there are too many arguments.
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout) throws CalException {
		String output;
		if (args.length == 0) {
			// cal
			output = printCal(args);
		} else if (args.length == 1) {
			if (args[0].contains("-") && args[0].length() == 2) {
				if (args[0].equals("-m")) {
					// cal -m
					output = printCalWithMondayFirst(args);
				} else {
					throw new CalException("Invalid input specified for start of week.");
				}
			} else {
				// cal year
				try {
					Integer.parseInt(args[0]);
				} catch (NumberFormatException e) {
					throw new CalException("Integer value should be specified for year.");
				}
				output = printCalForYear(args);
			}
		} else if (args.length == 2) {
			if (args[0].contains("-") && args[0].length() == 2) {
				// cal -m year
				if (args[0].equals("-m")) {
					output = printCalForYearMondayFirst(args);
				} else {
					throw new CalException("Invalid input specified for start of week.");
				}
			} else {
				// cal month year
				try {
					Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
					throw new CalException("Integer value should be specified for year.");
				}
				output = printCalForMonthYear(args);
			}
		} else if (args.length == 3) {
			// cal -m month year
			output = printCalForMonthYearMondayFirst(args);
		} else {
			throw new CalException("Too many arguments, maximum is three.");
		}
		try {
			stdout.write(output.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generates the string of the month in the given year to be printed.
	 * 
	 * @param args
	 *            Array of arguments specifying month and year.
	 * 
	 * @return result The string of the month in the given year.
	 * 
	 * @throws Calexception
	 *             If there is invalid input for month.
	 */
	public String printCalForMonthYear(String[] args) throws CalException {
		String monthStr = args[0];
		int month = 0;
		for (int i = 1; i < months.length; i++) {
			if ((monthStr.equalsIgnoreCase(months[i])) || (monthStr.equalsIgnoreCase(shortFormMonths[i]))) {
				month = i;
			}
		}

		if (month == 0) {
			throw new CalException("Invalid input specified for month.");
		}
		int year = Integer.parseInt(args[1]);
		String result = printFormatMonth(month, year, daysOfWeekSunFirst, true);
		return result;
	}

	/**
	 * Generates the string of the month in the given year to be printed, with
	 * Monday as first day of the week.
	 * 
	 * @param args
	 *            Array of arguments specifying monday to be first day of week,
	 *            month and year.
	 * 
	 * @return result The string of the month in the given year, with Monday as
	 *         first day of week.
	 * 
	 * @throws CalException
	 *             If there is invalid input for month.
	 */
	public String printCalForMonthYearMondayFirst(String[] args) throws CalException {
		String monthStr = args[1];
		int month = 0;
		for (int i = 0; i < months.length; i++) {
			if ((monthStr.equalsIgnoreCase(months[i])) || (monthStr.equalsIgnoreCase(shortFormMonths[i]))) {
				month = i;
			}
		}

		if (month == 0) {
			throw new CalException("Invalid input specified for month.");
		}
		int year = Integer.parseInt(args[2]);
		String result = printFormatMonth(month, year, daysOfWeekMonFirst, false);
		return result;

	}

	/**
	 * Generates the format of the string for a month.
	 * 
	 * @param args
	 *            Array of arguments specifying month and year.
	 * 
	 * @return result The string of the month in the given year.
	 */
	public String printFormatMonth(int month, int year, String daysOfWeek, boolean dayStartWeek) {
		if (month == 2) {
			isLeapYear = checkLeapYear(year);
		}

		int startDay = getStartDay(1, month, year);
		if (dayStartWeek) {

		} else {
			startDay = startDay - 1;
			if (startDay == -1) {
				startDay = 6;
			}
		}

		String heading = "   " + months[month] + " " + year + "\n";
		String frontSpace = "";

		if (startDay < 3 && startDay != 1 && startDay != 2) {
			for (int i = 0; i < startDay; i++) {
				frontSpace += "  ";
			}
		} else {
			for (int i = 0; i < startDay; i++) {
				frontSpace += "   ";
			}
		}
		int numberOfDays = days[month];

		if (month == 2 && isLeapYear) {
			numberOfDays = numberOfDays + 1;
		}
		String dates = "";
		for (int j = 1; j <= numberOfDays; j++) {
			if (j < 10) {
				dates += j + "  ";
			} else {
				dates += j + " ";
			}
			if (((j + startDay) % 7 == 0) || (j == numberOfDays)) {
				dates += "\n";
			}
		}
		String result = heading + daysOfWeek + frontSpace + dates;
		return result;
	}

	/**
	 * Generates the string of the current month with Monday as first day of the
	 * week.
	 * 
	 * @param args
	 *            Array of arguments specifying monday to be first day of week.
	 * 
	 * @return result The string of the current month with monday as the first
	 *         day of the week.
	 */
	public String printCalWithMondayFirst(String[] args) {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		String result = printFormatMonth(month, year, daysOfWeekMonFirst, false);
		return result;

	}

	/**
	 * Generates the string of the months in the year to be printed.
	 * 
	 * @param args
	 *            Array of arguments specifying year.
	 * 
	 * @return result The string of the months in the given year.
	 * 
	 */
	public String printCalForYear(String[] args) throws CalException {
		String[] jan = null, feb = null, mar = null, apr = null, may = null, jun = null, jul = null, aug = null,
				sept = null, oct = null, nov = null, dec = null;

		// all months in a year get result string and split into weeks
		String allDays;
		String newArgs[] = new String[2];
		newArgs[1] = args[0];

		for (int i = 1; i <= 12; i++) {
			newArgs[0] = shortFormMonths[i];
			allDays = printCalForMonthYear(newArgs);

			switch (i) {
			case 1:
				jan = allDays.split("\n");
				break;
			case 2:
				feb = allDays.split("\n");
				break;
			case 3:
				mar = allDays.split("\n");
				break;
			case 4:
				apr = allDays.split("\n");
				break;
			case 5:
				may = allDays.split("\n");
				break;
			case 6:
				jun = allDays.split("\n");
				break;
			case 7:
				jul = allDays.split("\n");
				break;
			case 8:
				aug = allDays.split("\n");
				break;
			case 9:
				sept = allDays.split("\n");
				break;
			case 10:
				oct = allDays.split("\n");
				break;
			case 11:
				nov = allDays.split("\n");
				break;
			case 12:
				dec = allDays.split("\n");
				break;
			}

		}

		String result = printFormatYear(jan, feb, mar, apr, may, jun, jul, aug, sept, oct, nov, dec);
		return result;
	}

	/**
	 * Generates the format of the string for the months in the given year to be
	 * printed.
	 * 
	 * @param jan,feb,mar,apr,may,jun,jul,aug,sept,oct,nov,dec
	 *            Arrays each containing the string for each month.
	 * 
	 * @return result The string of the months in the given year.
	 */
	public String printFormatYear(String[] jan, String[] feb, String[] mar, String[] apr, String[] may, String[] jun,
			String[] jul, String[] aug, String[] sept, String[] oct, String[] nov, String[] dec) {

		String result = jan[0] + "\t\t" + feb[0] + "\t" + mar[0] + "\n";
		for (int i = 1; i <= 6; i++) {
			if (i == 6 && feb.length >= 7) {
				if (jan[i].length() > 12 && feb[i].length() > 12) {
					result += jan[i] + "\t" + feb[i] + "\t" + mar[i] + "\n";
				} else if (jan[i].length() > 12 && feb[i].length() <= 12) {
					if (feb[i].length() > 6) {
						result += jan[i] + "\t" + feb[i] + "\t\t" + mar[i] + "\n";
					} else {
						result += jan[i] + "\t" + feb[i] + "\t\t\t" + mar[i] + "\n";
					}
				} else if (jan[i].length() <= 12 && feb[i].length() > 12) {
					if (jan[i].length() > 6) {
						result += jan[i] + "\t\t" + feb[i] + "\t" + mar[i] + "\n";
					} else {
						result += jan[i] + "\t\t\t" + feb[i] + "\t\t" + mar[i] + "\n";
					}
				} else {
					result += jan[i] + "\t\t" + feb[i] + "\t\t" + mar[i] + "\n";
				}
			} else if (i == 6 && feb.length < 7) {
				result += jan[i] + "\t\t\t\t" + mar[i] + "\n";
			} else {
				result += jan[i] + "\t" + feb[i] + "\t" + mar[i] + "\n";
			}
		}

		if (jan.length > 7 && feb.length <= 7 && mar.length <= 7) {
			result += jan[7] + "\n";
		}

		if (jan.length <= 7 && feb.length > 7 && mar.length <= 7) {
			result += "\t\t\t" + feb[7] + "\n";
		}

		if (jan.length <= 7 && feb.length <= 7 && mar.length > 7) {
			result += "\t\t\t\t\t\t" + mar[7] + "\n";
		}

		result += "\n";
		result += apr[0] + "\t\t" + may[0] + "\t\t" + jun[0] + "\n";
		for (int i = 1; i <= 6; i++) {
			if (i == 6) {
				if (apr[i].length() > 12 && may[i].length() > 12) {
					if (apr[i].length() < 16 && jun[i].length() < 12) {
						result += apr[i] + "\t\t" + may[i] + "\t" + jun[i] + "\n";
					} else {
						result += apr[i] + "\t" + may[i] + "\t" + jun[i] + "\n";
					}
				} else if (apr[i].length() > 12 && may[i].length() <= 12) {
					if (may[i].length() > 6) {
						result += apr[i] + "\t" + may[i] + "\t\t" + jun[i] + "\n";
					} else {
						result += apr[i] + "\t" + may[i] + "\t\t\t" + jun[i] + "\n";
					}
				} else if (apr[i].length() <= 12 && may[i].length() > 12) {
					if (apr[i].length() > 6) {
						result += apr[i] + "\t\t" + may[i] + "\t" + jun[i] + "\n";
					} else {
						result += apr[i] + "\t\t\t" + may[i] + "\t\t" + jun[i] + "\n";
					}
				} else {
					result += apr[i] + "\t\t" + may[i] + "\t\t" + jun[i] + "\n";
				}
			} else {
				result += apr[i] + "\t" + may[i] + "\t" + jun[i] + "\n";
			}
		}
		if (apr.length > 7 && may.length <= 7 && jun.length <= 7) {
			result += apr[7] + "\n";
		}

		if (apr.length <= 7 && may.length > 7 && jun.length <= 7) {
			result += "\t\t\t" + may[7] + "\n";
		}

		if (apr.length <= 7 && may.length <= 7 && jun.length > 7) {
			result += "\t\t\t\t\t" + jun[7] + "\n";
		}
		result += "\n";
		result += jul[0] + "\t\t" + aug[0] + "\t" + sept[0] + "\n";
		for (int i = 1; i <= 6; i++) {
			if (i == 6) {
				if (jul[i].length() > 12 && aug[i].length() > 12) {
					if (jul[i].length() < 16 && sept[i].length() < 12) {
						result += jul[i] + "\t\t" + aug[i] + "\t" + sept[i] + "\n";
					} else {
						result += jul[i] + "\t" + aug[i] + "\t" + sept[i] + "\n";
					}
				} else if (jul[i].length() > 12 && aug[i].length() <= 12) {
					if (aug[i].length() > 6) {
						result += jul[i] + "\t" + aug[i] + "\t\t" + sept[i] + "\n";
					} else {
						result += jul[i] + "\t" + aug[i] + "\t\t\t" + sept[i] + "\n";
					}
				} else if (jul[i].length() <= 12 && aug[i].length() > 12) {
					if (jul[i].length() > 6) {
						result += jul[i] + "\t\t" + aug[i] + "\t" + sept[i] + "\n";
					} else {
						result += jul[i] + "\t\t\t" + aug[i] + "\t\t" + sept[i] + "\n";
					}
				} else {
					result += jul[i] + "\t\t" + aug[i] + "\t\t" + sept[i] + "\n";
				}
			} else {
				result += jul[i] + "\t" + aug[i] + "\t" + sept[i] + "\n";
			}
		}

		if (jul.length > 7 && aug.length <= 7 && sept.length <= 7) {
			result += jul[7] + "\n";
		}

		if (jul.length <= 7 && aug.length > 7 && sept.length <= 7) {
			result += "\t\t\t" + aug[7] + "\n";
		}

		if (jul.length <= 7 && aug.length <= 7 && sept.length > 7) {
			result += "\t\t\t\t\t" + sept[7] + "\n";
		}
		result += "\n";
		result += oct[0] + "\t\t" + nov[0] + "\t" + dec[0] + "\n";
		for (int i = 1; i <= 6; i++) {
			if (i == 6) {
				if (oct[i].length() > 12 && nov[i].length() > 12) {
					result += oct[i] + "\t" + nov[i] + "\t" + dec[i] + "\n";
				} else if (oct[i].length() > 12 && nov[i].length() <= 12) {
					if (nov[i].length() > 6) {
						result += oct[i] + "\t" + nov[i] + "\t\t" + dec[i] + "\n";
					} else {
						result += oct[i] + "\t" + nov[i] + "\t\t\t" + dec[i] + "\n";
					}
				} else if (oct[i].length() <= 12 && nov[i].length() > 12) {
					if (oct[i].length() > 6) {
						result += oct[i] + "\t\t" + nov[i] + "\t" + dec[i] + "\n";
					} else {
						result += oct[i] + "\t\t\t" + nov[i] + "\t\t" + dec[i] + "\n";
					}
				} else {
					result += oct[i] + "\t\t" + nov[i] + "\t\t" + dec[i] + "\n";
				}
			} else {
				result += oct[i] + "\t" + nov[i] + "\t" + dec[i] + "\n";
			}
		}
		if (oct.length > 7 && nov.length <= 7 && dec.length <= 7) {
			result += oct[7] + "\n";
		}

		if (oct.length <= 7 && nov.length > 7 && dec.length <= 7) {
			result += "\t\t\t" + nov[7] + "\n";
		}

		if (oct.length <= 7 && nov.length <= 7 && dec.length > 7) {
			result += "\t\t\t\t\t" + dec[7] + "\n";
		}
		return result;
	}

	/**
	 * Generates the string of the months in the given year to be printed, with
	 * Monday as the start day of the week.
	 * 
	 * @param args
	 *            Array of arguments specifying Monday to be the start day of
	 *            the week and the year.
	 * 
	 * @return result The string of the months in the given year, with Monday as
	 *         the start day of the week.
	 */
	public String printCalForYearMondayFirst(String[] args) throws CalException {
		String[] jan = null, feb = null, mar = null, apr = null, may = null, jun = null, jul = null, aug = null,
				sept = null, oct = null, nov = null, dec = null;

		// all months in a year get result string and split into weeks
		String allDays;
		String newArgs[] = new String[3];
		newArgs[2] = args[1];

		for (int i = 1; i <= 12; i++) {
			newArgs[1] = shortFormMonths[i];
			allDays = printCalForMonthYearMondayFirst(newArgs);
			switch (i) {
			case 1:
				jan = allDays.split("\n");
				break;
			case 2:
				feb = allDays.split("\n");
				break;
			case 3:
				mar = allDays.split("\n");
				break;
			case 4:
				apr = allDays.split("\n");
				break;
			case 5:
				may = allDays.split("\n");
				break;
			case 6:
				jun = allDays.split("\n");
				break;
			case 7:
				jul = allDays.split("\n");
				break;
			case 8:
				aug = allDays.split("\n");
				break;
			case 9:
				sept = allDays.split("\n");
				break;
			case 10:
				oct = allDays.split("\n");
				break;
			case 11:
				nov = allDays.split("\n");
				break;
			case 12:
				dec = allDays.split("\n");
				break;
			}

		}

		String result = printFormatYear(jan, feb, mar, apr, may, jun, jul, aug, sept, oct, nov, dec);
		return result;
	}
}
