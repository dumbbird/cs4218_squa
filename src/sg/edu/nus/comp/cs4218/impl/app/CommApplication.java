package sg.edu.nus.comp.cs4218.impl.app;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.app.Comm;
import sg.edu.nus.comp.cs4218.exception.CommException;

public class CommApplication implements Comm {

	public static final String EXP_DIRECTORY_EXCEPTION = "This is a directory";
	public static final String EXP_NULL_POINTER = "Null Pointer Exception";
	public static final String EXP_INVALID_ARGS = "Invalid arguments";
	public static final String DOUBLE_TAB = "\t\t";
	public static final String SINGLE_TAB = "\t";
	public static final String EXP_FNF_EXCEPTION = "File not found";
	public static final String EXP_FILE_UNREADABLE_EXCEPTION = "File not readable";

	/**
	 * Runs the comm application with the specified arguments.
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * @param stdin
	 *            An InputStream. The input for the command is read from this
	 *            InputStream if no files are specified.
	 * @param stdout
	 *            An OutputStream. The output of the command is written to this
	 *            OutputStream.
	 * 
	 * @throws CommException
	 *             If the file(s) specified do not exist or are unreadable.
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout) throws CommException {
		if (stdout == null) {
			throw new CommException(EXP_NULL_POINTER);
		} else if (args == null) {
			throw new CommException(EXP_INVALID_ARGS);
		}

		if (args.length != 1 && args.length != 2) {
			throw new CommException(EXP_INVALID_ARGS);
		}

		int numOfFiles = args.length;
		String result = null;

		if (numOfFiles == 1) {
			if (args[0].length() <= 0) {
				throw new CommException(EXP_FNF_EXCEPTION);
			}
			// read from stdin
			ArrayList<Byte> b = new ArrayList<Byte>();
			int num;
			try {
				while ((num = stdin.read()) != -1) {
					b.add((byte) num);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			byte[] byteFileArray1 = new byte[b.size()];
			for (int i = 0; i < b.size(); i++) {
				byteFileArray1[i] = b.get(i);
			}

			Path filePath;
			Path currentDir = Paths.get(Environment.currentDirectory);

			filePath = currentDir.resolve(args[0]);
			checkIfFileIsReadable(filePath);

			byte[] byteFileArray2 = null;

			try {
				byteFileArray2 = Files.readAllBytes(filePath);
			} catch (IOException e) {
				e.printStackTrace();
			}

			result = comm(new ByteArrayInputStream(byteFileArray1), new ByteArrayInputStream(byteFileArray2));
		} else if (numOfFiles == 2) {
			if (args[0].length() <= 0 || args[1].length() <= 0) {
				throw new CommException(EXP_FNF_EXCEPTION);
			}

			Path filePath;
			Path[] filePathArray = new Path[numOfFiles];
			Path currentDir = Paths.get(Environment.currentDirectory);
			boolean isFileReadable = false;

			for (int i = 0; i < numOfFiles; i++) {
				filePath = currentDir.resolve(args[i]);
				isFileReadable = checkIfFileIsReadable(filePath);
				if (isFileReadable) {
					filePathArray[i] = filePath;
				}
			}

			byte[] byteFileArray1 = null;
			byte[] byteFileArray2 = null;

			if (filePathArray.length == 2) {
				try {
					byteFileArray1 = Files.readAllBytes(filePathArray[0]);
					byteFileArray2 = Files.readAllBytes(filePathArray[1]);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			result = comm(new ByteArrayInputStream(byteFileArray1), new ByteArrayInputStream(byteFileArray2));
		}

		try {
			stdout.write(result.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Runs the comm function on the passed ByteArrayInputStream and returns the
	 * string result of doing comm on the two.
	 * 
	 * @param is1
	 *            ByteArrayInputStream of first input
	 * @param is2
	 *            ByteArrayInputStream of second input
	 * @return Result of doing comm on the passed inputs.
	 * @throws CommException
	 *             If passed inputs are not sorted.
	 */
	public String comm(ByteArrayInputStream is1, ByteArrayInputStream is2) throws CommException {
		String file1 = toString(is1), file2 = toString(is2);

		ArrayList<String> fileArr1 = getLines(file1);
		ArrayList<String> fileArr2 = getLines(file2);

		String result = "";

		int pos1 = 0, pos2 = 0;
		String temp1 = "";
		String temp2 = "";

		while (pos1 < fileArr1.size() && pos2 < fileArr2.size()) {
			temp1 = fileArr1.get(pos1);
			temp2 = fileArr2.get(pos2);

			temp1 = temp1.replaceAll("\n|\r| ", "");
			temp2 = temp2.replaceAll("\n|\r| ", "");
			if (temp1.compareTo(temp2) < 0) {
				result += fileArr1.get(pos1) + System.lineSeparator();
				pos1++;
			} else if (temp1.compareTo(temp2) > 0) {
				result += SINGLE_TAB + fileArr2.get(pos2) + System.lineSeparator();
				pos2++;
			} else if (temp1.compareTo(temp2) == 0) {
				result += DOUBLE_TAB + fileArr1.get(pos1) + System.lineSeparator();
				pos1++;
				pos2++;
			}

		}

		if (pos1 < fileArr1.size()) {
			for (; pos1 < fileArr1.size(); pos1++) {
				if (fileArr1.get(pos1).compareTo("\n") == 0) {
					result += System.lineSeparator();
				} else {
					result += fileArr1.get(pos1) + System.lineSeparator();
				}
			}
		} else if (pos2 < fileArr2.size()) {
			for (; pos2 < fileArr2.size(); pos2++) {
				if (fileArr2.get(pos2).compareTo("\n") == 0) {
					result += SINGLE_TAB + System.lineSeparator();
				} else {
					result += SINGLE_TAB + fileArr2.get(pos2) + System.lineSeparator();
				}
			}
		}

		if (result.length() == 0) {
			result += System.lineSeparator();
		}

		return result;
	}

	/**
	 * Converts the passed ByteArrayInputStream to string and returns the
	 * string.
	 * 
	 * @param is
	 *            ByteArrayInputStream to be converted to string.
	 * 
	 * @return The parsed string.
	 */
	private String toString(ByteArrayInputStream is) {
		int n = is.available();
		byte[] bytes = new byte[n];
		is.read(bytes, 0, n);
		String s = new String(bytes, StandardCharsets.UTF_8);

		return s;
	}

	/**
	 * Returns string to print comparisons when there are no matches in both
	 * files
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * 
	 * @return The output of comm application in the passed arguments
	 */
	@Override
	public String commNoMatches(String[] args) {
		try {
			return sendToComm(args);
		} catch (CommException e) {
			return null;
		}
	}

	/**
	 * Returns string to print comparisons when there are only lines in the
	 * first file to match
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * 
	 * @return The output of comm application in the passed arguments
	 */
	@Override
	public String commOnlyFirst(String[] args) {
		try {
			return sendToComm(args);
		} catch (CommException e) {
			return null;
		}
	}

	/**
	 * Returns string to print comparisons when there are only lines in the
	 * second file to match
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * 
	 * @return The output of comm application in the passed arguments
	 */
	@Override
	public String commOnlySecond(String[] args) {
		try {
			return sendToComm(args);
		} catch (CommException e) {
			return null;
		}
	}

	/**
	 * Returns string to print comparisons when some of the lines match
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * 
	 * @return The output of comm application in the passed arguments
	 */
	@Override
	public String commBothMathches(String[] args) {
		try {
			return sendToComm(args);
		} catch (CommException e) {
			return null;
		}
	}

	/**
	 * Returns string to print comparisons when there are all matches in both
	 * files
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * 
	 * @return The output of comm application in the passed arguments
	 */
	@Override
	public String commAllMatches(String[] args) {
		try {
			return sendToComm(args);
		} catch (CommException e) {
			return null;
		}
	}

	/**
	 * Checks if a file is readable.
	 * 
	 * @param filePath
	 *            The path to the file
	 * 
	 * @return True if the file is readable.
	 * 
	 * @throws CommException
	 *             If the file is not readable
	 */
	boolean checkIfFileIsReadable(Path filePath) throws CommException {

		if (Files.isDirectory(filePath)) {
			throw new CommException(EXP_DIRECTORY_EXCEPTION);
		}
		if (Files.exists(filePath) && Files.isReadable(filePath)) {
			return true;
		} else if (Files.notExists(filePath)) {
			throw new CommException(EXP_FNF_EXCEPTION);
		} else {
			throw new CommException(EXP_FILE_UNREADABLE_EXCEPTION);
		}
	}

	/**
	 * Breaks the passed file content by newline and System.lineseparator().
	 * 
	 * @param file
	 *            The file content to be split.
	 * 
	 * @return The split file content.
	 */
	private ArrayList<String> getLines(String file) {
		ArrayList<String> fileArr = new ArrayList<String>();
		int pos1 = 0, pos2 = 0;

		while (pos2 < file.length() - 1 || pos2 != -1) {
			pos2 = file.indexOf('\n', pos1);

			if (pos2 == -1) {
				break;
			} else if (pos2 - 1 >= 0 && file.charAt(pos2 - 1) == '\r') {
				pos2++;
			}

			fileArr.add(file.substring(pos1, pos2));
			pos1 = pos2 + 1;
		}

		if (pos2 == -1 && pos1 < file.length() && file.substring(pos1, file.length()).length() != 0) {
			fileArr.add(file.substring(pos1, file.length()));
		}

		if (file.length() > 0 && file.charAt(file.length() - 1) == '\n' && file.charAt(file.length() - 2) != '\r') {
			fileArr.add("\n");
		}

		return fileArr;
	}

	/**
	 * Runs the comm application with the specified arguments.
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * 
	 * @throws CommException
	 *             If the file(s) specified do not exist or are unreadable.
	 */
	private String sendToComm(String[] args) throws CommException {
		int numOfFiles = args.length;
		Path filePath;
		Path[] filePathArray = new Path[numOfFiles];
		Path currentDir = Paths.get(Environment.currentDirectory);
		boolean isFileReadable = false;

		for (int i = 0; i < numOfFiles; i++) {
			filePath = currentDir.resolve(args[i]);
			isFileReadable = checkIfFileIsReadable(filePath);
			if (isFileReadable) {
				filePathArray[i] = filePath;
			}
		}

		byte[] byteFileArray1 = null;
		byte[] byteFileArray2 = null;

		if (filePathArray.length == 2) {
			try {
				byteFileArray1 = Files.readAllBytes(filePathArray[0]);
				byteFileArray2 = Files.readAllBytes(filePathArray[1]);
				return comm(new ByteArrayInputStream(byteFileArray1), new ByteArrayInputStream(byteFileArray2));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

}
