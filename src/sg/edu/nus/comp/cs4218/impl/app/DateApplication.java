package sg.edu.nus.comp.cs4218.impl.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.exception.DateException;

/**
 * The date command print the current date and time.
 * 
 * <p>
 * <b>Command format:</b> <code>date</code>
 * </p>
 */
public class DateApplication implements Application {

	/**
	 * Runs the date application with the specified arguments.
	 * 
	 * @param args
	 *            Array of arguments for the application. Each array element is
	 *            the path to a file. If no files are specified stdin is used.
	 * @param stdin
	 *            An InputStream. The input for the command is read from this
	 *            InputStream if no files are specified.
	 * @param stdout
	 *            An OutputStream. The output of the command is written to this
	 *            OutputStream.
	 * 
	 * @throws DateException
	 *             If an I/O exception occurs.
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout) throws DateException {
		// TODO Auto-generated method stub
		// changes for hackathon test case
		if (args != null && args.length > 0) {
			throw new DateException("Too many parameters");
		}

		if (stdout == null) {
			throw new DateException("Null input/output stream");
		}

		// end changes
		try {
			DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
			Calendar cal = Calendar.getInstance();
			stdout.write(dateFormat.format(cal.getTime()).getBytes());
			stdout.write(System.lineSeparator().getBytes());
		} catch (IOException e) {
			throw new DateException("Unable to print working directory " + e);
		}

	}

}
