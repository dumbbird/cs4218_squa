package sg.edu.nus.comp.cs4218.impl.app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.app.Sort;
import sg.edu.nus.comp.cs4218.exception.SortException;

/**
 * The sort command sorts lines in the specified file or input and prints the
 * lines in sorted format.
 * 
 * <p>
 * <b>Command format:</b> <code>sort [-n] [FILE]...</code>
 * </p>
 */
public class SortApplication implements Sort {

	public static Path filePath;
	public static Path currentDir = Paths.get(Environment.currentDirectory);
	public static boolean isFileReadable = false;

	/**
	 * Runs the sort application with the specified arguments.
	 * 
	 * @param args
	 *            Array of arguments for the application.
	 * @param stdin
	 *            An InputStream used if file not specified.
	 * @param stdout
	 *            An OutputStream. Sorted lines will be output to stdout,
	 *            separated by a newline character.
	 * 
	 * @throws SortException
	 *             If an I/O exception occurs.
	 * 
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout) throws SortException {
		// TODO Auto-generated method stub
		String[] sorted = null;
		if (stdout == null) {
			throw new SortException("Null Pointer Exception");
		} else if (args == null && stdin == null) {
			throw new SortException("Null Pointer Exception");
		} else if (args != null && args.length == 0 && stdin == null) {
			throw new SortException("No arguments!");
		}

		if ((args == null || args.length == 0) && stdin != null) {
			try {
				BufferedReader bfr = new BufferedReader(new InputStreamReader(stdin, "UTF-8"));
				String str;
				ArrayList<String> strings = new ArrayList<String>();
				while ((str = bfr.readLine()) != null) {
					strings.add(str);
				}
				String[] toSort = strings.toArray(new String[strings.size()]);
				sorted = sort(toSort);
				bfr.close();

			} catch (Exception exIO) {
				throw new SortException("Exception Caught");
			}
		} else {
			String[] toSort;
			// sort file
			if (args.length == 1 && args[0].contains(".txt")) {
				filePath = currentDir.resolve(args[0]);
				isFileReadable = checkIfFileReadable(filePath);
				if (isFileReadable) {
					toSort = readFile();
					sorted = sort(toSort);

				}
				// sort -n file
			} else if (args.length == 2 && args[1].contains(".txt") && args[0].equals("-n")) {
				filePath = currentDir.resolve(args[1]);
				isFileReadable = checkIfFileReadable(filePath);
				if (isFileReadable) {
					toSort = readFile();
					sorted = sortFirstWordLineNumber(toSort);

				}
				// sort <user entered words>
			} else {
				if (args[0].equals("-n") && args.length != 1) {
					toSort = new String[args.length - 1];
					for (int i = 1; i < args.length; i++) {
						toSort[i - 1] = args[i];
					}
					sorted = sortFirstWordLineNumber(toSort);

				} else if (args[0].equals("-n") && args.length == 1) {
					throw new SortException("Invalid arguments!");
				} else {
					sorted = sort(args);
				}
			}
		}

		try {
			for (int i = 0; i < sorted.length; i++) {
				stdout.write(sorted[i].getBytes("UTF-8"));
				stdout.write(System.lineSeparator().getBytes("UTF-8"));
			}
		} catch (IOException e) {

		}
	}

	/**
	 * Sorts lines according to ASCII.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return String[] The array of sorted lines.
	 */
	public String[] sort(String[] toSort) {
		for (int i = 0; i < toSort.length - 1; i++) {
			int min = i;
			for (int j = i + 1; j < toSort.length; j++) {
				if (toSort[j].compareTo(toSort[min]) < 0) {
					min = j;
				}
			}
			String temp = toSort[i];
			toSort[i] = toSort[min];
			toSort[min] = temp;
		}
		return toSort;
	}

	/**
	 * Sorts lines according to ASCII except that first word of lines is treated
	 * as number.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return String[] Array of sorted lines.
	 */
	public String[] sortFirstWordLineNumber(String[] toSort) {
		for (int i = 0; i < toSort.length - 1; i++) {
			int min = i;
			for (int j = i + 1; j < toSort.length; j++) {
				String[] first = toSort[j].split(" ");
				String[] minString = toSort[min].split(" ");
				if (checkBothNumbers(first[0], minString[0])) {
					int firstNum = Integer.parseInt(first[0]);
					int minNum = Integer.parseInt(minString[0]);
					if (firstNum < minNum) {
						min = j;
					}
				} else if (toSort[j].compareTo(toSort[min]) < 0) {

					min = j;

				}
			}
			String temp = toSort[i];
			toSort[i] = toSort[min];
			toSort[min] = temp;
		}
		return toSort;
	}

	/**
	 * Checks if both strings are numbers.
	 * 
	 * @param first
	 *            First string to be checked.
	 * @param second
	 *            Second string to be checked.
	 * 
	 * @return boolean True if both are numbers else false.
	 *
	 * 
	 */
	public boolean checkBothNumbers(String first, String second) {
		try {
			Integer.parseInt(first);
			Integer.parseInt(second);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * Reads a file and stores lines into array.
	 * 
	 * @return String[] Array of lines read.
	 * 
	 */
	public String[] readFile() {
		ArrayList<String> lines = new ArrayList<String>();
		String line;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath.toString()), "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			br.close();
			// fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines.toArray(new String[lines.size()]);
	}

	/**
	 * Checks if a file is readable.
	 * 
	 * @param filePath
	 *            The path to the file
	 * @return True if the file is readable.
	 * @throws SortException
	 *             If the file is not readable
	 */
	boolean checkIfFileReadable(Path filePath) throws SortException {

		if (Files.isDirectory(filePath)) {
			throw new SortException("This is a directory");
		}
		if (Files.exists(filePath) && Files.isReadable(filePath)) {
			return true;
		} else if (Files.notExists(filePath)) {
			throw new SortException("No such file exists");
		} else {
			throw new SortException("Could not read file");
		}
	}

	/**
	 * Process sorting of simple strings.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> The sorted list of strings.
	 */
	@Override
	public List<String> sortStringsSimple(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings containing capital letters.
	 * 
	 * @param toSort
	 *            Array of lines to sort.
	 * 
	 * @return List<String> The sorted list of strings.
	 * 
	 */
	@Override
	public List<String> sortStringsCapital(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of numbers in strings.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return Sorted list of strings.
	 * 
	 */
	@Override
	public List<String> sortNumbers(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings with special characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> Sorted list of strings.
	 * 
	 * 
	 */
	@Override
	public List<String> sortSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings containing capital letters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> Sorted list of strings.
	 * 
	 * 
	 */
	@Override
	public List<String> sortSimpleCapital(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings with numbers.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortSimpleNumbers(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings with special characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortSimpleSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings with capital letters and numbers.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortCapitalNumbers(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes the sorting of strings with capital and special characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortCapitalSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes the sorting of strings with numbers and special characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortNumbersSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes the sorting of strings with capital letters and numbers.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortSimpleCapitalNumber(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes the sorting of strings with capital and special characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortSimpleCapitalSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes the sorting of strings with numbers and special characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortSimpleNumbersSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings with capital, numbers and special
	 * characters.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortCapitalNumbersSpecialChars(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings containing number, capital, special, all
	 * types.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return List<String> List of sorted lines.
	 * 
	 */
	@Override
	public List<String> sortAll(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

	/**
	 * Processes sorting of strings with numbers.
	 * 
	 * @param toSort
	 *            Array of lines to be sorted.
	 * 
	 * @return Object List of sorted lines.
	 * 
	 */
	public Object sortStringsNumbers(String[] toSort) {
		String[] sorted = sort(toSort);
		return Arrays.asList(sorted);
	}

}
