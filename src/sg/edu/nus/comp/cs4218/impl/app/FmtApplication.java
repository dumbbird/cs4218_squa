package sg.edu.nus.comp.cs4218.impl.app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.FmtException;

/**
 * Wrap the given text at the specified maximum width. Do not break a line in
 * between words.
 * 
 * <p>
 * <b>Command format:</b> <code>fmt [OPTIONS] [FILE]</code>
 * <dl>
 * <dt>OPTIONS</dt>
 * <dd>&quot;-w 50&quot; means print the given text where each line has at most
 * 50 characters. Default value is 80.</dd>
 * <dt>FILE</dt>
 * <dd>the name of the file. If not specified, use stdin.</dd>
 * </dl>
 * </p>
 */
public class FmtApplication implements Application {

	/**
	 * Runs the fmt application with the specified arguments.
	 * 
	 * @param args
	 *            Array of arguments for the application. Only one file could be
	 *            specified. If file is specified, the input should be read from
	 *            the file, else, input is read from stdin. If a flag,-n, is
	 *            specified, it should be accompanied by a number to indicate
	 *            the number of lines. If flag is not specified, the first 10
	 *            lines would be printed.
	 * @param stdin
	 *            An InputStream. The input for the command is read from this
	 *            InputStream if no files are specified.
	 * @param stdout
	 *            An OutputStream. The output of the command is written to this
	 *            OutputStream.
	 * 
	 * @throws FmtException
	 *             If the file specified do not exist, are unreadable, or an I/O
	 *             exception occurs. If the args array do not matches the
	 *             criteria.
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout) throws FmtException {
		// TODO Auto-generated method stub
		int numMaxWidth = 0;
		if (args == null || args.length == 0 || args.length == 2) {
			if (args == null || args.length == 0) {
				numMaxWidth = 80;
			} else {
				if (args.length == 2 && args[0].equals("-w")) {
					numMaxWidth = checkNumberOfMaxWidth(args[1]);
				} else {
					throw new FmtException("Invalid Fmt Command for reading from stdin");
				}
			}

			readFromStdinAndWriteToStdout(stdout, numMaxWidth, stdin);

		} else {
			if (args.length == 3 || args.length == 1) {
				if (args.length == 3 && args[0].equals("-w")) {
					numMaxWidth = checkNumberOfMaxWidth(args[1]);
				} else if (args.length == 1) {
					numMaxWidth = 80;
				} else {
					throw new FmtException("Incorrect flag used");
				}
			} else {
				throw new FmtException("Invalid Fmt Command");
			}

			// check file
			Path currentDir = Paths.get(Environment.currentDirectory);
			int filePosition = 0;
			if (args.length == 3) {
				filePosition = 2;
			}
			Path filePath = currentDir.resolve(args[filePosition]);
			boolean isFileReadable = false;
			isFileReadable = checkIfFileIsReadable(filePath);

			if (isFileReadable) {
				readFromFileAndWriteToStdout(stdout, numMaxWidth, filePath);
			}
		}
	}

	/**
	 * Read from stdin and wrap the given text at the specified maximum width.
	 * Output the lines specified to stdout
	 * 
	 * @param stdout
	 *            An Output Stream. The output is written to this stream
	 * @param numMaxWidth
	 *            The number of lines required to output as received in the head
	 *            command or 10 if not specified in the command
	 * @param stdin
	 *            An input Stream. Reading from stdin and not a file
	 * @throws FmtException
	 *             If stdin or stdout is null. I/O exceptions caught when
	 *             reading and writing from input and output streams.
	 */
	void readFromStdinAndWriteToStdout(OutputStream stdout, int numMaxWidth, InputStream stdin) throws FmtException {

		if (stdin == null || stdout == null) {
			throw new FmtException("Null Pointer Exception");
		}

		BufferedReader buffReader;
		try {
			buffReader = new BufferedReader(new InputStreamReader(stdin, "utf-8"));
			String inputString = null;

			while ((inputString = buffReader.readLine()) != null) {
				String[] linesToWrite = splitString(inputString, numMaxWidth);

				for (int i = 0; i < linesToWrite.length; i++) {
					stdout.write(linesToWrite[i].trim().getBytes("UTF-8"));
					stdout.write(System.lineSeparator().getBytes("UTF-8"));
				}
			}
			stdout.write(System.lineSeparator().getBytes("UTF-8"));

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			throw new FmtException("IO Exception");
		}
	}

	/**
	 * Read from one line from the stdin or file and split the line into several
	 * substrings at the specified maximum width. Returns the substring array
	 * 
	 * @param input
	 *            The input string line
	 * @param numMaxWidth
	 *            The number of lines required to output as received in the head
	 *            command or 10 if not specified in the command
	 * @throws FmtException
	 *             If stdin or stdout is null. I/O exceptions caught when
	 *             reading and writing from input and output streams.
	 * @return output.toString().split("\n") The substring array at the
	 *         specified maximum width.
	 */
	public String[] splitString(String input, int numMaxWidth) throws FmtException {

		if (input == null) {
			throw new FmtException("Null Pointer Exception");
		}

		String[] subLine = input.split("\n");
		if (subLine.length > 1)
			throw new FmtException("Too many lines, only one line is allowed once.");

		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;

		try {
			while (tok.hasMoreTokens()) {
				String word = tok.nextToken();

				if (lineLen == 0 && word.length() > numMaxWidth) {
					// First word exceeds max width
					// output.append(word + " ");
				} else if (lineLen + word.length() > numMaxWidth) {
					// Non-first word exceeds max width
					output.append("\n");
					lineLen = 0;
				}
				output.append(word + " ");

				lineLen += word.length() + 1;
			}
		} catch (Exception e) {
			throw new FmtException("Split line Exception");
		}

		return output.toString().split("\n");
	}

	/**
	 * Parse the number of maximum width to break from String to int
	 * 
	 * @param numWidthString
	 *            The number of lines received in String
	 * @return numWidth The number of max width received in int
	 * @throws FmtException
	 *             If the numWidthString in not an Integer or a non-positive
	 *             number.
	 */
	int checkNumberOfMaxWidth(String numWidthString) throws FmtException {
		int numWidth;
		try {
			numWidth = Integer.parseInt(numWidthString);

			if (numWidth <= 0) {
				throw new FmtException("Number of lines cannot be non-positive");
			}
		} catch (NumberFormatException nfe) {
			throw new FmtException("Invalid command, not a number.");
		}

		return numWidth;
	}

	/**
	 * Read from file and output last number of lines specified to stdout
	 * 
	 * @param stdout
	 *            An Output Stream. The output is written to this stream
	 * @param numMaxWidth
	 *            The number of max width required to output as received in the
	 *            fmt command or 80 if not specified in the command
	 * @param filePath
	 *            A Path. Read file from the file path given
	 * @throws FmtException
	 *             If stdout is null. Other exceptions caught when reading and
	 *             writing from input and output streams.
	 */
	void readFromFileAndWriteToStdout(OutputStream stdout, int numMaxWidth, Path filePath) throws FmtException {

		String encoding = "UTF-8";

		if (stdout == null) {
			throw new FmtException("Stdout is null");
		}

		try {
			FileInputStream fileInStream = new FileInputStream(filePath.toString());
			InputStreamReader inputStrReader = new InputStreamReader(fileInStream);
			encoding = inputStrReader.getEncoding();
			BufferedReader buffReader = new BufferedReader(inputStrReader);

			String inputString = null;
			while ((inputString = buffReader.readLine()) != null) {
				String[] linesToWrite = splitString(inputString, numMaxWidth);
				for (int i = 0; i < linesToWrite.length; i++) {
					stdout.write(linesToWrite[i].trim().getBytes(encoding));
					stdout.write(System.lineSeparator().getBytes("UTF-8"));
				}
			}
			buffReader.close();
			stdout.write(System.lineSeparator().getBytes(encoding));
		} catch (IOException e) {
			throw new FmtException("IOException");
		}

	}

	/**
	 * Checks if a file is readable.
	 * 
	 * @param filePath
	 *            The path to the file
	 * @return True if the file is readable.
	 * @throws FmtException
	 *             If the file is not readable
	 */
	boolean checkIfFileIsReadable(Path filePath) throws FmtException {

		if (Files.exists(filePath) && Files.isReadable(filePath) && !Files.isDirectory(filePath)) {
			return true;
		} else if (Files.notExists(filePath)) {
			throw new FmtException("No such file exists");
		} else if (!Files.isReadable(filePath)) {
			throw new FmtException("Could not read file");
		} else if (Files.isDirectory(filePath)) {
			throw new FmtException("This is a directory");
		}
		return false;
	}
}
