package sg.edu.nus.comp.cs4218.impl.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Stack;

import sg.edu.nus.comp.cs4218.app.Bc;
import sg.edu.nus.comp.cs4218.exception.BcException;

/**
 * The bc command is a calculator for the shell that supports addition,
 * multiplication, powers, other binary operators and floating point arithmetic.
 * *
 * <p>
 * <b>Command format:</b> <code>bc expr</code>
 * <dl>
 * <dt>expr</dt>
 * <dd>expression specifying the formula to be computed, e.g., (1 + 2.3) * 4 >=
 * 56 returns 1. Typical rules of precedence and associativity apply.</dd>
 * </dl>
 * </p>
 */

public class BcApplication implements Bc {
	Stack<Integer> ops = new Stack<Integer>();
	Stack<Double> vals = new Stack<Double>();

	/**
	 * Runs the bc application with the specified arguments.
	 * 
	 * @param args
	 *            Array of arguments for the application.
	 * @param stdin
	 *            An InputStream. The input for the command is read from this
	 *            InputStream if no files are specified.
	 * @param stdout
	 *            An OutputStream. The output of the command is written to this
	 *            OutputStream.
	 * 
	 * @throws BcException
	 *             If the file specified do not exist, are unreadable, or an I/O
	 *             exception occurs. If the args array do not matches the
	 *             criteria.
	 */
	@Override
	public void run(String[] args, InputStream stdin, OutputStream stdout) throws BcException {

		if (args == null || args.length == 0) {
			throw new BcException("Null arguments");
		} else if (args.length == 1 && args[0] == null) {
			throw new BcException("Null arguments");
		} else if (args.length > 1) {
			throw new BcException("Too many arguments");
		}

		if (stdout == null) {
			throw new BcException("OutputStream not provided");
		}

		String result = delegate(args[0].trim());

		if (result == null)
			throw new BcException("Invalid expression");

		try {
			stdout.write(result.getBytes());
			stdout.write(System.lineSeparator().getBytes());
		} catch (IOException e) {
			throw new BcException("Unable to print working directory " + e);
		}
	}

	/**
	 * Returns resultant string with expression of the form <number>, where
	 * number can by any natural number or floating point number, evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String number(String[] args) {
		if (isFloat(args[0])) {
			return args[0];
		}

		return null;
	}

	/**
	 * Returns resultant string with expression of the form -
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String negate(String[] args) {
		String num = null;
		try {
			num = delegate(args[0]);

			if (num != null) {
				float posNum = Float.valueOf(num);
				float result = (float) (Math.round((-posNum) * 100000.0) / 100000.0);
				if (result == (long) result) {
					return String.format("%d", (long) result);
				} else
					return String.format("%s", result);
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> +
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String add(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);

			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				float result = (float) (Math.round((n1 + n2) * 100000.0) / 100000.0);
				if (result == (long) result) {
					return String.format("%d", (long) result);
				} else
					return String.format("%s", result);
			} else
				return null;
		} catch (BcException e) {
			return null;
		}

	}

	/**
	 * Returns resultant string with expression of the form <expression> -
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String subtract(String[] args) {
		String num1 = null, num2 = null;
		try {
			if (args[0].isEmpty()) {
				String[] negArgs = new String[1];
				negArgs[0] = args[1];
				return negate(negArgs);
			} else {
				num1 = delegate(args[0]);
				num2 = delegate(args[1]);
				float n1 = Float.valueOf(num1) + 0, n2 = Float.valueOf(num2) + 0;
				float result = (float) (Math.round((n1 - n2) * 100000.0) / 100000.0);
				if (result == (long) result) {
					return String.format("%d", (long) result);
				} else
					return String.format("%s", result);
			}
		} catch (BcException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Returns resultant string with expression of the form <expression> *
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String multiply(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);

			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				float result = (float) (Math.round(n1 * n2 * 100000.0) / 100000.0);
				if (result == (long) result) {
					return String.format("%d", (long) result);
				} else
					return String.format("%s", result);
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> /
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String divide(String[] args) {
		String num1 = null, num2 = null;

		try {

			num1 = delegate(args[0]);
			num2 = delegate(args[1]);

			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n2 == 0)
					return null;

				float result = (float) (Math.round(n1 / n2 * 100000.0) / 100000.0);
				if (result == (long) result) {
					return String.format("%d", (long) result);
				} else
					return String.format("%s", result);
			}

			else
				return null;

		} catch (BcException e) {
			return null;
		}

	}

	/**
	 * Returns resultant string with expression of the form <expression> ^
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String pow(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				float result = (float) (Math.round(Math.pow(n1, n2) * 100000.0) / 100000.0);
				if (result == (long) result) {
					return String.format("%d", (long) result);
				} else
					return String.format("%s", result);
				// return Float.toString((float)Math.pow(n1,n2));
			} else
				return null;
		} catch (BcException e) {
			return null;
		}

	}

	/**
	 * Returns resultant string with expression of the form (<expression>)
	 * evaluated
	 * 
	 * @param args
	 *            args[0] The expression inside the bracket args[1] The
	 *            expression before the bracket args[2] The expression after the
	 *            bracket
	 * @return The calculated string
	 */
	@Override
	public String bracket(String[] args) {
		if (args.length != 3)
			return null;

		String result = null;
		try {
			result = delegate(args[0]);
			if (result != null)
				return delegate(args[1] + result + args[2]);
			else
				return null;
		} catch (BcException e) {
			return null;
		}

	}

	/**
	 * Returns resultant string with expression of the form <expression> >
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String greaterThan(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 > n2) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}

	}

	/**
	 * Returns resultant string with expression of the form <expression> >=
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String greaterThanOrEqual(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 >= n2) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> <
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String lessThan(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 < n2) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> <=
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String lessThanOrEqual(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 <= n2) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> ==
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String equalEqual(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 == n2) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> !=
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String notEqual(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 != n2) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> &&
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String and(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);

			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 > 0 && n2 > 0) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form <expression> ||
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String or(String[] args) {
		String num1 = null, num2 = null;

		try {
			num1 = delegate(args[0]);
			num2 = delegate(args[1]);
			if (num1 != null && num2 != null) {
				float n1 = Float.valueOf(num1), n2 = Float.valueOf(num2);
				if (n1 > 0 || n2 > 0) {
					return "1";
				} else {
					return "0";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Returns resultant string with expression of the form !
	 * <expression> evaluated
	 * 
	 * @param args
	 *            The expression to be evaluated
	 * @return The calculated string
	 */
	@Override
	public String not(String[] args) {
		String num1 = null;

		try {
			num1 = delegate(args[0]);
			if (num1 != null) {
				float n1 = Float.valueOf(num1);
				if (n1 > 0) {
					return "0";
				} else {
					return "1";
				}
			} else
				return null;
		} catch (BcException e) {
			return null;
		}
	}

	/**
	 * Split the original arg string into two substrings
	 * 
	 * @param arg
	 *            The original arg string
	 * @param index
	 *            The splitting position
	 * @param isDouble
	 *            Whether the operator is single char or double char
	 * @return The split substring array
	 */
	public String[] getSubstring(String arg, int index, boolean isDouble) {
		String[] args = new String[2];
		args[0] = arg.substring(0, index).trim();
		if (isDouble) {
			args[1] = arg.substring(index + 2).trim();
		} else {
			args[1] = arg.substring(index + 1).trim();
		}
		return args;
	}

	/**
	 * Parse the expression argument
	 * 
	 * @param arg
	 *            The original expression string
	 * @return The result string
	 * @throws BcException
	 *             If the args array do not matches the criteria.
	 */
	public String delegate(String arg) throws BcException {
		if (arg == null || arg == "" || arg.isEmpty()) {
			throw new BcException("Invalid expression");
		} else if (arg.contains("(")) {
			int index1 = arg.indexOf("(");
			int index2 = getMatchingBracketIndex(arg, index1);
			String[] args = new String[3];
			args[0] = arg.substring(index1 + 1, index2).trim();
			args[1] = arg.substring(0, index1).trim(); // before bracket
			args[2] = arg.substring(index2 + 1).trim(); // after bracket
			return bracket(args);

		} else if (arg.contains("&&")) {
			int index = arg.indexOf("&&");
			return and(getSubstring(arg, index, true));

		} else if (arg.contains("||")) {
			int index = arg.indexOf("||");
			return or(getSubstring(arg, index, true));

		} else if (arg.contains("!=")) {
			int index = arg.indexOf("!=");
			return notEqual(getSubstring(arg, index, true));

		} else if (arg.contains("==")) {
			int index = arg.indexOf("==");
			return equalEqual(getSubstring(arg, index, true));

		} else if (arg.contains("<=")) {
			int index = arg.indexOf("<=");
			return lessThanOrEqual(getSubstring(arg, index, true));

		} else if (arg.contains("<")) {
			int index = arg.indexOf("<");
			return lessThan(getSubstring(arg, index, false));

		} else if (arg.contains(">=")) {
			int index = arg.indexOf(">=");
			return greaterThanOrEqual(getSubstring(arg, index, true));

		} else if (arg.contains(">")) {
			int index = arg.indexOf(">");
			return greaterThan(getSubstring(arg, index, false));

		} else if (arg.contains("+")) {
			int index = arg.indexOf("+");
			return add(getSubstring(arg, index, false));

		} else if (arg.contains("-")) {
			int index = arg.indexOf("-");
			// if (index == 0) {
			// String[] args = new String[1];
			// args[0] = arg.substring(index + 1).trim();
			// return negate(args);
			// } else {
			// return subtract(getSubstring(arg, index, false));
			// }
			return subtract(getSubstring(arg, index, false));
		} else if (arg.contains("/")) {
			int index = arg.indexOf("/");
			return divide(getSubstring(arg, index, false));

		} else if (arg.contains("*")) {
			int index = arg.indexOf("*");
			return multiply(getSubstring(arg, index, false));

		} else if (arg.contains("^")) {
			int index = arg.indexOf("^");
			return pow(getSubstring(arg, index, false));

		} else if (arg.contains("!")) {
			int index = arg.indexOf("!");
			String[] args = new String[1];
			args[0] = arg.substring(index + 1);
			return not(args);

		} else if (isFloat(arg)) {
			String[] args = new String[] { arg };
			return number(args);
		} else {
			throw new BcException("Invalid expression");
		}
	}

	/**
	 * Checks if the argument string is float number
	 * 
	 * @param arg
	 *            The string to be checked
	 * @return true if it is float, false if it is not float
	 */
	private boolean isFloat(String arg) {
		boolean isValidFloat = false;
		if (arg.isEmpty())
			return isValidFloat;
		try {
			Float.parseFloat(arg);
			isValidFloat = true;
		} catch (NumberFormatException ex) {
			// ex.printStackTrace();
			throw new NumberFormatException("Not a valid number");
		}

		return isValidFloat;
	}

	/**
	 * Find the index position for matching bracket
	 * 
	 * @param arg
	 *            The string to be checked
	 * @param index1
	 *            The position where ( locates
	 * @return The position where the paired ) locates
	 * @throws BcException
	 *             If the position not found, invalid expression.
	 */
	private int getMatchingBracketIndex(String arg, int index1) throws BcException {
		Stack<Integer> s = new Stack<Integer>();

		for (int i = index1; i < arg.length(); i++) {
			if (arg.charAt(i) == '(') {
				s.push(0);
			} else if (arg.charAt(i) == ')') {
				s.pop();
			}

			if (s.size() == 0) {
				return i;
			}
		}

		throw new BcException("Invalid expression");
	}

}
