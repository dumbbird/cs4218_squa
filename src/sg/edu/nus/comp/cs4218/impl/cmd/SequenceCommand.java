package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;

/**
 * A Pipe Command is a sub-command consisting of sub-commands separated with
 * semicolons, whereby a sub-command can either be a Pipe Command or a Call
 * Command.
 * 
 * <p>
 * <b>Command format:</b> <code>&lt;call&gt ; &lt;pipe&gt</code>
 * </p>
 */

public class SequenceCommand implements Command {

	Vector<CallCommand> callCommandList;
	String cmdline;

	/**
	 * Constructor for the Pipe Command class.
	 * 
	 * @param cmdline
	 *            String of sub-command to be evaluated.
	 * 
	 */
	public SequenceCommand(String cmdline) {
		this.cmdline = cmdline.trim();
		this.callCommandList = new Vector<CallCommand>();
		// System.out.println("pipe:"+cmdline);
	}

	/**
	 * Evaluates sub-command using data provided through stdin stream. Writes
	 * result to stdout stream.
	 * 
	 * @param stdin
	 *            InputStream to get data from.
	 * @param stdout
	 *            OutputStream to write resultant data to.
	 * 
	 * @throws AbstractApplicationException
	 *             If an exception happens while evaluating the sub-command.
	 * @throws ShellException
	 *             If an exception happens while evaluating the sub-command.
	 */
	@Override
	public void evaluate(InputStream stdin, OutputStream stdout) throws AbstractApplicationException, ShellException {

		InputStream currInputStream;
		OutputStream currOutputStream;

		for (int j = 0; j < callCommandList.size(); j++) {
			CallCommand callCmd = callCommandList.get(j);

			if (j == 0) { // last = write to stdout
				currInputStream = stdin;
			} else { // not last, write to System.out
				currInputStream = stdin;
			}

			if (j == callCommandList.size() - 1) { // last = write to stdout
				currOutputStream = stdout;
			} else { // not last, write to System.out
				currOutputStream = stdout;
			}

			callCmd.evaluate(currInputStream, currOutputStream);

		}
	}

	/**
	 * Parses the sub-command to break them up into Call Commands by semicolons,
	 * only if semicolons is not within the backquote
	 * 
	 * @throws ShellException
	 *             If an exception happens while parsing the sub-command and
	 *             semicolon is the 1st and last index of the cmdline
	 */
	public void parse() throws ShellException {

		// // searches for semicolon
		int indexSemicolon = -1, strStartIdx = 0, searchStartIdx = 0;
		String rightCmd = cmdline;
		Boolean eval = false;
		Boolean proceed = true;
		CallCommand callCommand;
		String patternBQ = "`([^\\n`]*)`";
		Pattern patternBQp = Pattern.compile(patternBQ);

		testInvalidSemiCol(rightCmd);

		do {
			eval = false;
			proceed = true;
			rightCmd = rightCmd.substring(strStartIdx);
			Matcher matcherBQ = patternBQp.matcher(rightCmd);
			indexSemicolon = rightCmd.indexOf(';', searchStartIdx);

			while (matcherBQ.find()) {
				if (matcherBQ.start() < indexSemicolon && matcherBQ.end() > indexSemicolon) {
					proceed = false;
					indexSemicolon = matcherBQ.end() - 1;
					break;
				}
			}

			String subCmd = "";
			subCmd = getSubCmd(indexSemicolon, rightCmd, proceed);

			// send to callCommand
			callCommand = new CallCommand(subCmd);
			Boolean isValid = getIsValid(callCommand);
			if (isValid) {
				callCommandList.add(callCommand);
				strStartIdx = indexSemicolon + 1;
				searchStartIdx = 0;
				eval = true;
			} else {
				strStartIdx = 0;
				searchStartIdx = indexSemicolon + 1;
				eval = false;
			}

		} while (indexSemicolon != -1 && indexSemicolon != rightCmd.length() - 1);
		testInvalidEvalu(eval, callCommand);
	}

	/**
	 * test the invalid parsing in call command
	 * 
	 * @param eval
	 *            boolean true if it is valid
	 * 
	 * @param callcommand
	 *            CallCommand add the class for evulaution when it is valid
	 * @throws AbstractApplicationException
	 *             If an exception happens while processing the content in the
	 *             semicolon.
	 * @throws ShellException
	 *             If an exception happens while processing the content in the
	 *             semicolon.
	 */
	private void testInvalidEvalu(Boolean eval, CallCommand callCommand) throws ShellException {
		if (!eval) {
			callCommandList.add(callCommand);
			throw new ShellException(ShellImpl.EXP_SYNTAX);
		}
	}

	/**
	 * get isValid for the parsing
	 * 
	 * @param callCommand
	 *            the class of the callCommand contain the right input
	 * 
	 * @return Boolean is it valid parsing in callCommand
	 * 
	 */
	private Boolean getIsValid(CallCommand callCommand) {
		Boolean isValid = true;
		try {
			callCommand.parse();
		} catch (Exception e) {
			isValid = false;
		}
		return isValid;
	}

	/**
	 * Process the command inside the backquotes
	 * 
	 * @param indexSemicolon
	 *            the index semicolon is located
	 * 
	 * 
	 * @param rightCmd
	 *            the whole input
	 * 
	 * @param proceed
	 *            when it is true, backqoute does not surround the semicolon
	 * @return String the 1st part after split by semicolon
	 * 
	 * 
	 */
	private String getSubCmd(int indexSemicolon, String rightCmd, Boolean proceed) {
		String subCmd;
		if (indexSemicolon > -1 && proceed) {
			subCmd = rightCmd.substring(0, indexSemicolon);
		} else {
			subCmd = rightCmd;
		}
		return subCmd;
	}

	/**
	 * test Invalid semicolon
	 * 
	 * @param rightCmd
	 *            the whole input
	 * 
	 * @throws ShellException
	 *             If an exception happens when semicolon is the start and last
	 *             of index in the rightcmd
	 * 
	 */
	private void testInvalidSemiCol(String rightCmd) throws ShellException {
		if (rightCmd.indexOf(';') == 0 || rightCmd.lastIndexOf(';') == cmdline.length() - 1) {
			throw new ShellException(cmdline + ": " + "invalid semicolon.");
		}
	}

	/**
	 * For testing purposes, getting of the list of call commands after parsing;
	 * 
	 */
	Vector<CallCommand> getCallCommandList() {
		return callCommandList;
	}

	/**
	 * Terminates current execution of the command (unused for now)
	 */
	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

	public void addCommand(CallCommand callCmd) {
		// TODO Auto-generated method stub

	}

}
