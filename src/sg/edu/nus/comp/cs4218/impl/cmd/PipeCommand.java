package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;

/**
 * A Sequence Command is a sub-command consisting of sub-commands, whereby a
 * sub-command can either be a Pipe Command, Call Command or further be another
 * Sequence Command.
 * 
 * <p>
 * <b>Command format:</b>
 * <code>&lt;command&gt ; &lt;command&gt | &lt;command&gt</code>
 * </p>
 */

public class PipeCommand implements Command {

	public static final String EXP_STDOUT = "Error writing to stdout.";
	public static final String EXP_REDIR_PIPE = "File output redirection and pipe "
			+ "operator cannot be used side by side.";

	public Vector<SequenceCommand> sequenceCmdList;
	String cmdline;

	/**
	 * Constructor for the Sequence Command class.
	 * 
	 * @param cmdline
	 *            String of sub-command to be evaluated.
	 * 
	 */
	public PipeCommand(String cmdline) {
		this.cmdline = cmdline.trim();
		this.sequenceCmdList = new Vector<SequenceCommand>();
	}

	/**
	 * Evaluates command using data provided through stdin stream. Writes result
	 * to stdout stream.
	 * 
	 * @param stdin
	 *            InputStream to get data from.
	 * @param stdout
	 *            OutputStream to write resultant data to.
	 * 
	 * @throws AbstractApplicationException
	 *             If an exception happens while evaluating the sub-command.
	 * @throws ShellException
	 *             If an exception happens while evaluating the sub-command.
	 */
	public void evaluate(InputStream stdin, OutputStream stdout) throws AbstractApplicationException, ShellException {
		InputStream currInputStream = stdin;
		OutputStream currOutputStream = new ByteArrayOutputStream();

		// System.out.println("sequenceCommandList.size() = " +
		// sequenceCmdList.size());
		for (int i = 0; i < sequenceCmdList.size(); i++) {
			SequenceCommand pipeCmd = sequenceCmdList.get(i);

			pipeCmd.evaluate(currInputStream, currOutputStream);

			ByteArrayOutputStream outByte = (ByteArrayOutputStream) currOutputStream;
			byte[] byteArray = outByte.toByteArray();

			// changes made for hackathon
			String bqResult = new String(byteArray).replaceAll("(\r?\n)", "");
			if (i == sequenceCmdList.size() - 1) { // last
				ShellImpl.closeOutputStream(currOutputStream);
			} else {// not last: pipe outputStream to inputStream
				currInputStream = ShellImpl.outputStreamToInputStream(currOutputStream);
				currOutputStream = new ByteArrayOutputStream();
			}
		}

		// stdout = currOutputStream;
		ShellImpl.writeToStdout(currOutputStream, stdout);
	}

	/**
	 * Parses the sub-command to break them up into Call Commands by dividers,
	 * only if divider is not within the backquote
	 * 
	 */
	public void parse() throws ShellException {
		// searches for divider
		int indexDivider = -1, strStartIdx = 0, searchStartIdx = 0;
		String rightCmd = cmdline;
		Boolean eval = false;
		Boolean proceed = true;
		SequenceCommand sequenceCommand;
		String patternBQ = "`([^\\n`]*)`";
		Pattern patternBQp = Pattern.compile(patternBQ);

		testInvalidDivider(rightCmd);

		do {
			proceed = true;
			eval = false;
			rightCmd = rightCmd.substring(strStartIdx);

			Matcher matcherBQ = patternBQp.matcher(rightCmd);
			indexDivider = rightCmd.indexOf('|', searchStartIdx);
			while (matcherBQ.find()) {
				if (matcherBQ.start() < indexDivider && matcherBQ.end() > indexDivider) {
					proceed = false;

					// changes made for hackathon
					if (rightCmd.substring(matcherBQ.end()).contains("|")) {
						proceed = true;
						indexDivider = matcherBQ.end() - 1;
						break;
					} else {
						indexDivider = -1;
						break;
					}
					// end Changes
				}
			}
			String subCmd = "";

			subCmd = getSubCmd(indexDivider, rightCmd, proceed);

			sequenceCommand = new SequenceCommand(subCmd);

			Boolean isValid = getIsValid(sequenceCommand);

			if (isValid) {
				sequenceCmdList.add(sequenceCommand);
				strStartIdx = indexDivider + 1;
				searchStartIdx = 0;
				eval = true;

			} else {
				strStartIdx = 0;
				searchStartIdx = indexDivider + 1;
				eval = false;
			}

		} while (indexDivider != -1 && indexDivider != rightCmd.length() - 1);

		testEvulationInvalid(eval, sequenceCommand);

	}

	/**
	 * test the invalid parsing in sequence command
	 * 
	 * @param eval
	 *            boolean true if it is valid
	 * 
	 * @param sequenceCommand
	 *            sequenceCommand add the class for evulaution when it is valid
	 * @throws AbstractApplicationException
	 *             If an exception happens while processing the content in the
	 *             divider.
	 * @throws ShellException
	 *             If an exception happens while processing the content in the
	 *             divider.
	 */
	private void testEvulationInvalid(Boolean eval, SequenceCommand sequenceCommand) throws ShellException {
		if (!eval) {
			sequenceCmdList.add(sequenceCommand);
			throw new ShellException(ShellImpl.EXP_SYNTAX);
		}
	}

	/**
	 * get isValid for the parsing
	 * 
	 * @param sequenceCommand
	 *            the class of the sequenceCommand contain the right input
	 * 
	 * @return Boolean is it valid parsing in sequenceCommand
	 * 
	 */

	private Boolean getIsValid(SequenceCommand sequenceCommand) {
		Boolean isValid = true;
		try {
			sequenceCommand.parse();
		} catch (Exception e) {
			isValid = false;
		}
		return isValid;
	}

	/**
	 * Process the command inside the backquotes
	 * 
	 * @param indexDivider
	 *            the index divider is located
	 * 
	 * 
	 * @param rightCmd
	 *            the whole input
	 * 
	 * @param proceed
	 *            when it is true, backqoute does not surround the divider
	 * @return String the 1st part after split by divider
	 * 
	 * 
	 */
	private String getSubCmd(int indexDivider, String rightCmd, Boolean proceed) {
		String subCmd;

		if (indexDivider > -1 && proceed) {
			subCmd = rightCmd.substring(0, indexDivider);
		} else {
			subCmd = rightCmd;
		}
		return subCmd;
	}

	/**
	 * test Invalid divider
	 * 
	 * @param rightCmd
	 *            the whole input
	 * 
	 * @throws ShellException
	 *             If an exception happens when divider is the start and last of
	 *             index in the rightcmd
	 * 
	 */
	private void testInvalidDivider(String rightCmd) throws ShellException {
		if (rightCmd.indexOf('|') == 0 || rightCmd.lastIndexOf('|') == cmdline.length() - 1) {
			throw new ShellException(cmdline + ": " + "invalid divider.");
		}
	}

	/**
	 * For testing purposes, getting of the list of pipe commands after parsing;
	 * 
	 */
	Vector<SequenceCommand> getSequenceCommandList() {
		return sequenceCmdList;
	}

	/**
	 * Terminates current execution of the command (unused for now)
	 */
	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

	public void addCommand(SequenceCommand pipeCmd) {
		// TODO Auto-generated method stub

	}

}
