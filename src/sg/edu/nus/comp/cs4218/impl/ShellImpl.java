package sg.edu.nus.comp.cs4218.impl;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.Shell;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.app.*;
import sg.edu.nus.comp.cs4218.impl.cmd.*;

/**
 * A Shell is a command interpreter and forms the backbone of the entire
 * program. Its responsibility is to interpret commands that the user type and
 * to run programs that the user specify in her command lines.
 * 
 * <p>
 * <b>Command format:</b>
 * <code>&lt;Pipe&gt; | &lt;Sequence&gt; | &lt;Call&gt;</code>
 * </p>
 */

public class ShellImpl implements Shell {

	public static final String EXP_INVALID_APP = "Invalid app.";
	public static final String EXP_SYNTAX = "Invalid syntax encountered.";
	public static final String EXP_REDIR_PIPE = "File output redirection and "
			+ "pipe operator cannot be used side by side.";
	public static final String EXP_SAME_REDIR = "Input redirection file same " + "as output redirection file.";
	public static final String EXP_STDOUT = "Error writing to stdout.";
	public static final String EXP_NOT_SUPPORTED = " not supported yet";
	public static String bqResult = "";

	/**
	 * Process the command inside the backquotes
	 * 
	 * @param argsArray
	 *            String array of the individual commands.
	 * 
	 * @return String array with the back quotes command processed.
	 * 
	 * @throws AbstractApplicationException
	 *             If an exception happens while processing the content in the
	 *             back quotes.
	 * @throws ShellException
	 *             If an exception happens while processing the content in the
	 *             back quotes.
	 */
	public static String[] processBQ(String... argsArray) throws AbstractApplicationException, ShellException {

		String[] resultArr = new String[argsArray.length];
		System.arraycopy(argsArray, 0, resultArr, 0, argsArray.length);
		String patternBQ = "`([^\\n`]*)`";
		Pattern patternBQp = Pattern.compile(patternBQ);

		for (int i = 0; i < argsArray.length; i++) {
			Matcher matcherBQ = patternBQp.matcher(argsArray[i]);

			if (matcherBQ.find()) {
				if (argsArray[i].length() - 1 >= 0 && argsArray[i].charAt(argsArray[i].length() - 1) == '\n') {
					argsArray[i] = argsArray[i].replaceAll("\n", "");
				} else {
					argsArray[i] = argsArray[i].replaceAll("\n", " ");
				}

				OutputStream bqOutputStream = new ByteArrayOutputStream();

				ShellImpl shell = new ShellImpl();
				shell.parseAndEvaluate(matcherBQ.group(1), bqOutputStream);

				ByteArrayOutputStream outByte = (ByteArrayOutputStream) bqOutputStream;
				byte[] byteArray = outByte.toByteArray();

				// changes made for hackathon
				if (bqResult.length() - 1 >= 0 && bqResult.charAt(bqResult.length() - 1) == '\n') {
					bqResult = new String(byteArray).replaceAll("(\r?\n)", "");
				} else {
					bqResult = new String(byteArray).replaceAll("(\r?\n)", " ");
				}
				// end changes

				// replace substring of back quote with result
				String replacedStr = argsArray[i].replace("`" + matcherBQ.group(1) + "`", bqResult);
				resultArr[i] = replacedStr.trim();
				resultArr[i] = resultArr[i].trim();
			}
		}

		return resultArr;
	}

	/**
	 * Process the globbing command Collect all the paths to existing files and
	 * directories such that these paths can be obtained by replacing all the
	 * unquoted asterisk symbols in ARG by some (possibly empty) sequences of
	 * nonslash characters.
	 * 
	 * @param argArray
	 *            String of an individual globbing filepath.
	 * 
	 * @return String A string containing the list of resultant paths separated
	 *         by "\" ". For example, GlobbingTest/MultiFileFolder/Globbing
	 *         Test3.py" GlobbingTest/MultiFileFolder/Globbing Test5.txt"
	 *         GlobbingTest/MultiFileFolder/Globbing Test7.html"
	 *         GlobbingTest/MultiFileFolder/GlobbingTest4.cpp
	 * 
	 *         Note: We do not use pure whitespaces to separate the resultant
	 *         paths to support scenarios in which the file name contains
	 *         whitespaces itself.
	 * 
	 * @throws AbstractApplicationException
	 *             If an exception happens while processing the content in the
	 *             back quotes.
	 * @throws ShellException
	 *             If an exception happens while processing the content in the
	 *             back quotes.
	 */
	public static String processGlob(String argArray) throws AbstractApplicationException, ShellException {

		String globDir = "";
		String[] fileList = {};
		int globIdx = 0;
		ShellImpl shell = new ShellImpl();
		String argArrayBuffer = argArray.replaceAll("\\\\", "/");
		globIdx = argArrayBuffer.indexOf("/*");
		if (globIdx != argArrayBuffer.length() - 2 || globIdx == -1 || globIdx == 0)
			throw new ShellException("Invalid globbing scenario");

		globDir = argArrayBuffer.substring(0, globIdx + 1);

		if (globDir.replaceAll("/", "").length() == 0)
			throw new ShellException("Invalid globbing scenario");

		File theDir = new File(globDir);
		if (!theDir.isDirectory() || theDir.canRead() == false) {
			String[] thisArg = new String[1];
			thisArg[0] = globDir;
			return shell.globNoPaths(thisArg);
		} else {
			fileList = theDir.list();
			String[] thisArg = new String[fileList.length + 1];
			thisArg[0] = globDir;
			for (int j = 0; j < fileList.length; j++) {
				thisArg[j + 1] = fileList[j];
			}
			return shell.globMultiLevel(thisArg);
		}
	}

	/**
	 * Static method to run the application as specified by the application
	 * command keyword and arguments.
	 * 
	 * @param app
	 *            String containing the keyword that specifies what application
	 *            to run.
	 * @param args
	 *            String array containing the arguments to pass to the
	 *            applications for running.
	 * @param inputStream
	 *            InputputStream for the application to get arguments from, if
	 *            needed.
	 * @param outputStream
	 *            OutputStream for the application to print its output to.
	 * 
	 * @throws AbstractApplicationException
	 *             If an exception happens while running any of the
	 *             application(s).
	 * @throws ShellException
	 *             If an unsupported or invalid application command is detected.
	 */
	public static void runApp(String app, String[] argsArray, InputStream inputStream, OutputStream outputStream)
			throws AbstractApplicationException, ShellException {
		Application absApp = null;
		if (("cat").equals(app)) {// cat [FILE]...
			absApp = new CatApplication();
		} else if (("echo").equals(app)) {// echo [args]...
			absApp = new EchoApplication();
		} else if (("head").equals(app)) {// head [OPTIONS] [FILE]
			absApp = new HeadApplication();
		} else if (("tail").equals(app)) {// tail [OPTIONS] [FILE]
			absApp = new TailApplication();
		} else if (("fmt").equals(app)) {// fmt [OPTIONS] [FILE]
			absApp = new FmtApplication();
		} else if (("date").equals(app)) {// date
			absApp = new DateApplication();
		} else if (("ls").equals(app)) {// ls
			absApp = new LsApplication();
		} else if (("bc").equals(app)) {// bc [expr]
			absApp = new BcApplication();
		} else if (("comm").equals(app)) {// comm
			absApp = new CommApplication();
		} else if (("cal").equals(app)) {// cal
			absApp = new CalApplication();
		} else if (("sort").equals(app)) {
			absApp = new SortApplication();
		} else { // invalid command
			throw new ShellException(app + ": " + EXP_INVALID_APP);
		}
		// System.out.println("test" + argsArray.length + inputStream.toString()
		// + outputStream.toString());
		absApp.run(argsArray, inputStream, outputStream);
	}

	/**
	 * Static method to creates an inputStream based on the file name or file
	 * path.
	 * 
	 * @param inputStreamS
	 *            String of file name or file path
	 * 
	 * @return InputStream of file opened
	 * 
	 * @throws ShellException
	 *             If file is not found.
	 */
	public static InputStream openInputRedir(String inputStreamS) throws ShellException {
		File inputFile = new File(inputStreamS);
		FileInputStream fInputStream = null;
		try {
			fInputStream = new FileInputStream(inputFile);
		} catch (FileNotFoundException e) {
			throw new ShellException(e.getMessage());
		}
		return fInputStream;
	}

	/**
	 * Static method to creates an outputStream based on the file name or file
	 * path.
	 * 
	 * @param onputStreamS
	 *            String of file name or file path.
	 * 
	 * @return OutputStream of file opened.
	 * 
	 * @throws ShellException
	 *             If file destination cannot be opened or inaccessible.
	 */
	public static OutputStream openOutputRedir(String outputStreamS) throws ShellException {
		File outputFile = new File(outputStreamS);
		FileOutputStream fOutputStream = null;
		try {
			fOutputStream = new FileOutputStream(outputFile);
		} catch (FileNotFoundException e) {
			throw new ShellException(e.getMessage());
		}
		return fOutputStream;
	}

	/**
	 * Static method to close an inputStream.
	 * 
	 * @param inputStream
	 *            InputStream to be closed.
	 * 
	 * @throws ShellException
	 *             If inputStream cannot be closed successfully.
	 */
	public static void closeInputStream(InputStream inputStream) throws ShellException {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				throw new ShellException(e.getMessage());
			}
		}
	}

	/**
	 * Static method to close an outputStream. If outputStream provided is
	 * System.out, it will be ignored.
	 * 
	 * @param outputStream
	 *            OutputStream to be closed.
	 * 
	 * @throws ShellException
	 *             If outputStream cannot be closed successfully.
	 */
	public static void closeOutputStream(OutputStream outputStream) throws ShellException {
		if (outputStream != System.out) {
			try {
				outputStream.close();
			} catch (IOException e) {
				throw new ShellException(e.getMessage());
			}
		}
	}

	/**
	 * Static method to write output of an outputStream to another outputStream,
	 * usually System.out.
	 * 
	 * @param outputStream
	 *            Source outputStream to get stream from.
	 * @param stdout
	 *            Destination outputStream to write stream to.
	 * @throws ShellException
	 *             If exception is thrown during writing.
	 */
	public static void writeToStdout(OutputStream outputStream, OutputStream stdout) throws ShellException {
		if (outputStream instanceof FileOutputStream) {
			return;
		}
		try {
			stdout.write(((ByteArrayOutputStream) outputStream).toByteArray());
		} catch (IOException e) {
			throw new ShellException(EXP_STDOUT);
		}
	}

	/**
	 * Static method to pipe data from an outputStream to an inputStream, for
	 * the evaluation of the Pipe Commands.
	 * 
	 * @param outputStream
	 *            Source outputStream to get stream from.
	 * 
	 * @return InputStream with data piped from the outputStream.
	 * 
	 * @throws ShellException
	 *             If exception is thrown during piping.
	 */
	public static InputStream outputStreamToInputStream(OutputStream outputStream) throws ShellException {
		if (outputStream.getClass().getName().equals("java.io.OutputStream")
				|| outputStream.getClass().getName().equals("java.io.ByteArrayOutputStream")) {
			return new ByteArrayInputStream(((ByteArrayOutputStream) outputStream).toByteArray());
		} else {
			throw new ShellException("Error in output redirection");

		}
	}

	/**
	 * Main method for the Shell Interpreter program.
	 * 
	 * @param args
	 *            List of strings arguments, unused.
	 */

	public static void main(String... args) {
		ShellImpl shell = new ShellImpl();

		BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));
		String readLine = null;
		String currentDir;

		while (true) {
			try {
				currentDir = Environment.currentDirectory;
				System.out.print(currentDir + ">");
				readLine = bReader.readLine();
				if (readLine == null) {
					break;
				}
				if (("").equals(readLine)) {
					continue;
				}
				shell.parseAndEvaluate(readLine, System.out);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * 
	 * parse and evaluate the input based on command sub, pipe,semicolon, call
	 * function
	 * 
	 * @param outputStream
	 *            Source outputStream to get stream from.
	 * 
	 * @param input
	 *            the input to be parse and evaluate
	 * 
	 * @throws ShellException
	 *             If exception is thrown during piping, calling function,
	 *             sequenceCommand.
	 */
	@Override
	public void parseAndEvaluate(String input, OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		String cmdline = input.replaceAll("\n", "");
		PipeCommand command = new PipeCommand(cmdline);
		command.parse();
		command.evaluate(null, stdout);

	}

	/**
	 * 
	 * evaluate the pipe operator of one command
	 * 
	 * @param args
	 *            each index contain the string split by |
	 * 
	 * @return the output after the evaluation
	 * 
	 * @throws ShellException
	 *             If exception is thrown during piping, calling function,
	 *             sequenceCommand.
	 */
	@Override
	public String pipeTwoCommands(String[] args) throws AbstractApplicationException, ShellException {

		String result = pipeWithException(args);

		return result;

	}

	/**
	 * 
	 * evaluate the pipe operator of multiple commands
	 * 
	 * @param args
	 *            each index contain the string split by |
	 * 
	 * @return the output after the evaluation
	 * 
	 * @throws ShellException
	 *             If exception is thrown during piping, calling function,
	 *             sequenceCommand.
	 */
	@Override
	public String pipeMultipleCommands(String[] args) throws ShellException, AbstractApplicationException {

		String result = pipeWithException(args);

		return result;
	}

	/**
	 * 
	 * evaluate the pipe operator and throws exception from evaluating the pipe
	 * operator
	 * 
	 * @param args
	 *            each index contain the string split by |
	 * 
	 * @return the output after the evaluation
	 * 
	 * @throws ShellException
	 *             If exception is thrown during evaluating the pipe operator
	 */
	@Override
	public String pipeWithException(String[] args) throws AbstractApplicationException, ShellException {

		String cmdline = "";
		String result = "";
		for (int i = 0; i < args.length; i++) {
			if (i < args.length - 1 || args.length == 1) {
				cmdline += args[i] + " | ";
			} else {
				cmdline += args[i];
			}
		}

		OutputStream pipeOutputStream = new ByteArrayOutputStream();
		ShellImpl shellInPipe = new ShellImpl();
		shellInPipe.parseAndEvaluate(cmdline, pipeOutputStream);
		ByteArrayOutputStream outByte = (ByteArrayOutputStream) pipeOutputStream;
		byte[] byteArray = outByte.toByteArray();
		result = new String(byteArray);

		return result;
	}

	/**
	 * Evaluate globbing with no files or directories
	 * 
	 * @param args
	 *            String array with only one element contains the globbing path
	 *            but without the * at the end
	 * 
	 * @return the same globbing path string
	 */
	@Override
	public String globNoPaths(String[] args) {
		if (args.length != 1)
			return null;
		// leave argument unchanged, but discard the * at the end
		return args[0];
	}

	/**
	 * Evaluate globbing with one file
	 * 
	 * @param args
	 *            String array with only two element args[0] contains the
	 *            globbing path. args[1] contains the filename inside the
	 *            globbing path Since only one file is found, therefore there is
	 *            only one argument storing its filename
	 * 
	 * @return the complete path string for this file
	 */
	@Override
	public String globOneFile(String[] args) {
		if (args.length != 2)
			return null;
		return args[0] + args[1];
	}

	/**
	 * Evaluate globbing with files and directories one level down
	 * 
	 * @param args
	 *            String array with N element. args[0] contains the globbing
	 *            path args[1]:args[N-1] contains the filenames inside the
	 *            globbing path
	 *
	 * 
	 * @return The collection of path strings for the files found, separated by
	 *         "\" "
	 * 
	 */
	@Override
	public String globFilesDirectories(String[] args) {

		String result = "";
		for (int i = 1; i < args.length; i++) {
			result += args[0] + args[i];
			if (i != args.length - 1)
				result += "\" ";
		}
		return result;
	}

	/**
	 * Evaluate globbing with files and directories multiple levels down
	 * 
	 * @param args
	 *            String array with N element args[0] contains the globbing path
	 *            args[1]:args[N-1] contains the foldernames or filenames inside
	 *            the globbing path
	 *
	 * 
	 * @return The collection of path strings for all the files found at all
	 *         levels, separated by "\" "
	 * 
	 */
	@Override
	public String globMultiLevel(String[] args) {
		// call globFilesDirectories, globOneFile
		File theDir;
		String result = "";
		boolean hasFolders = false;
		for (int i = 1; i < args.length; i++) {

			theDir = new File(args[0] + args[i]);
			if (theDir.isDirectory()) {
				hasFolders = true;
				try {
					result += processGlob(args[0] + args[i] + "/*");
				} catch (AbstractApplicationException e) {
					e.printStackTrace();
				} catch (ShellException e) {
					e.printStackTrace();
				}
			} else {
				result += args[0] + args[i];
			}
			if (i < args.length - 1)
				result += "\" ";
		}

		if (!hasFolders) {
			if (args.length == 2)
				return globOneFile(args);
			else if (args.length > 2)
				return globFilesDirectories(args);
		}

		return result;
	}

}
