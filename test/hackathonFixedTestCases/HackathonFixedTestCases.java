package hackathonFixedTestCases;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.CatException;
import sg.edu.nus.comp.cs4218.exception.EchoException;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;
import sg.edu.nus.comp.cs4218.impl.app.EchoApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class HackathonFixedTestCases {

    ShellImpl mockShell;
    ByteArrayOutputStream mockOutput;
    private static OutputStream outputStream;
    
	final static String TEST_STR = "Testing Stream";
	final static String TEST_FILE_NAME = "testShell.txt";
	final static String TEST_FILE_NAME2 = "testShell2.txt";
	final static String TEST_FOLDER_NAME = "testShellFolder";
	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception for valid command.";
	final static String VALID_FILE_NO_EXP = "Not supposed to have exception for valid file.";
	final static String VALID_STRM_NO_EXP = "Not supposed to have exception for valid streams.";
	final static String READONLY_EXP = "Supposed to have exception opening outputstream to read-only file.";
	final static String VALID_EXP = "Valid Exception thrown";
	final static String MISSING_EXP = "Should have exception thrown";
	static String originalFilePath;
    
    public HackathonFixedTestCases() {
        mockShell = new ShellImpl();
        mockOutput = new ByteArrayOutputStream();
    }

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		outputStream = System.out;
		createTestFile(TEST_FILE_NAME);
		createTestFolder(TEST_FOLDER_NAME);
		originalFilePath = Environment.currentDirectory;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		removeTestFile(TEST_FILE_NAME);
		removeTestFile(TEST_FILE_NAME2);
		removeTestFile("file1.txt");
		removeTestFile("file2.txt");
		removeTestFile("file3.txt");
		removeTestFile("comsub1.txt");
		removeTestFile("comsub2.txt");
		removeTestFile("flag.txt");
		removeTestFile("test1.txt");
		removeTestFile("numbersort.txt");
		removeTestFile("sort1.txt");
		removeTestFolder(TEST_FOLDER_NAME);
	}
	
	public static void createTestFile(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR.getBytes());
	}
	
	public static void createTestFileWithText(String fileName, String text) throws IOException {
		Files.write(Paths.get(fileName), text.getBytes());
	}

	public static void removeTestFile(String fileName) throws IOException {
		File file = new File(fileName);
		file.setWritable(true);
		file.delete();
	}

	public static void createTestFolder(String folderName) throws IOException {
		new File(folderName).mkdir();
	}

	public static void removeTestFolder(String folderName) throws IOException {
		File file = new File(folderName + "\\\\");

		String[] entries = file.list();
		if (entries != null) {
			for (String s : entries) {
				File currentFile = new File(file.getPath(), s);
				currentFile.delete();
			}
		}

		file.delete();
		Environment.currentDirectory = originalFilePath;
	}

	public void writeToStream(OutputStream myoutputStream) throws IOException {
		myoutputStream.write(TEST_STR.getBytes());
		myoutputStream.flush();
		myoutputStream.close();
	}

	public String fileToString(String fileName) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(fileName));
		String fileStr = scanner.useDelimiter("\\Z").next();
		scanner.close();

		return fileStr;
	}
	
    /**
     * When formatting with minimal width (1), the number 1, 2, 3 from test case was not found in output.
     *
     * This bug is due to not conforming "Wrap the given text at the specified maximum width." Ref spec page
     * 11 section fmt paragraph 1.
     */
    @Test
    public void testFmtMinimalWidth() throws Exception {
        String cmdline = "fmt -w 1 resource-hack/test1.txt";
        mockShell.parseAndEvaluate(cmdline, mockOutput);
        String expected = "line" + System.lineSeparator()
                + "1" + System.lineSeparator()
                + "line" + System.lineSeparator()
                + "2" + System.lineSeparator() + System.lineSeparator()
                + "line" + System.lineSeparator()
                + "3" + System.lineSeparator() + System.lineSeparator();
        assertEquals(expected, new String(mockOutput.toByteArray()));
    }
    
	/**
	 * Echo ends the output with a '\n' and not System.lineSeperator() which fails
     * the test case.
     *
     * The bug is due to failing the requirement "Use Java properties "file.separator" and
     * "path.separator" when working with file system. Use “line.separator” property when working with
     * newlines." Ref specs page 1 section coding paragraph 1.
     *
	 */
    @Test
    public void gotNewLineAfterOutputIfUseSlashN() throws EchoException {
        EchoApplication eApp = new EchoApplication();;
        OutputStream output = new ByteArrayOutputStream();;
        
        String temp = "This is a trap";
        String[] args = temp.split(" ");
        eApp.run(args, null, output);
        assertEquals(output.toString(), "This is a trap" + System.lineSeparator());
    }
    
    /**
     * Running cat without any arguments should throw an exception since there is also no expected stdin that is provided.
     * However, the program does not terminate. This also occurs for other command as well.
     *
     * The bug is due to invalid handling of " If required command line arguments are not provided or arguments are wrong or
     * inconsistent, the application throws an exception as well. " Ref Page 10 Section Applications Specification Paragraph 1.
     *
     * Zexian: The actual reason for this bug is due to we take System.in as stdin if there is no stdin provided, which is 
     * incorrect. We should take null value as stdin if no input provided.
     * 
     */
    @Test(expected = CatException.class)
    public void testCatWithoutArgument() throws Exception {
        String cmdline = "cat";
        mockShell.parseAndEvaluate(cmdline, mockOutput);
    }
    
    /**
     * When input cal 2015, returns an integer 6. Further inspection reveals an ArrayIndexOutOfBoundsException : 6
     * was thrown.
     *
     * This bug is due to not "print the calendar for each month in the specified year in a grid 3 wide
     * and 4 down"  Ref. spec Page 12, Section cal
     *
     * 	Failing code at sg.edu.nus.comp.cs4218.impl.app.CalApplication.printFormatYear(CalApplication.java:372)
     */
    @Test
    public void testCalYearSpecific() throws Exception {
        String cmdline = "cal 2015";
        mockShell.parseAndEvaluate(cmdline, mockOutput);
        // Output copy pasted from UNIX. Feel free to edit to your own calendar format.
        String expectedOutput = "   January 2015\t\t   February 2015\t   March 2015\n"
                + "Su Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\n"
                + "            1  2  3  \t1  2  3  4  5  6  7  \t1  2  3  4  5  6  7  \n"
                + "4  5  6  7  8  9  10 \t8  9  10 11 12 13 14 \t8  9  10 11 12 13 14 \n"
                + "11 12 13 14 15 16 17 \t15 16 17 18 19 20 21 \t15 16 17 18 19 20 21 \n"
                + "18 19 20 21 22 23 24 \t22 23 24 25 26 27 28 \t22 23 24 25 26 27 28 \n"
                + "25 26 27 28 29 30 31 				29 30 31 \n"
                + "\n"
      
                + "   April 2015\t\t   May 2015\t\t   June 2015\n"
                + "Su Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\n"
                + "         1  2  3  4  \t               1  2  \t   1  2  3  4  5  6  \n" 
                + "5  6  7  8  9  10 11 \t3  4  5  6  7  8  9  \t7  8  9  10 11 12 13 \n" 
                + "12 13 14 15 16 17 18 \t10 11 12 13 14 15 16 \t14 15 16 17 18 19 20 \n" 
                + "19 20 21 22 23 24 25 \t17 18 19 20 21 22 23 \t21 22 23 24 25 26 27 \n" 
                + "26 27 28 29 30 \t\t24 25 26 27 28 29 30 \t28 29 30 \n" 
                + "\t\t\t31 \n"
                + "\n"
                + "   July 2015\t\t   August 2015\t   September 2015\n"
                + "Su Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\n"
                + "         1  2  3  4  \t                  1  \t      1  2  3  4  5  \n"
                + "5  6  7  8  9  10 11 \t2  3  4  5  6  7  8  \t6  7  8  9  10 11 12 \n" 
                + "12 13 14 15 16 17 18 \t9  10 11 12 13 14 15 \t13 14 15 16 17 18 19 \n" 
                + "19 20 21 22 23 24 25 \t16 17 18 19 20 21 22 \t20 21 22 23 24 25 26 \n" 
                + "26 27 28 29 30 31 \t23 24 25 26 27 28 29 \t27 28 29 30 \n" 
                + "\t\t\t30 31 \n"
                + "\n"
                + "   October 2015\t\t   November 2015\t   December 2015\n"
                + "Su Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\tSu Mo Tu We Th Fr Sa\n" 
                + "            1  2  3  \t1  2  3  4  5  6  7  \t      1  2  3  4  5  \n" 
                + "4  5  6  7  8  9  10 \t8  9  10 11 12 13 14 \t6  7  8  9  10 11 12 \n" 
                + "11 12 13 14 15 16 17 \t15 16 17 18 19 20 21 \t13 14 15 16 17 18 19 \n" 
                + "18 19 20 21 22 23 24 \t22 23 24 25 26 27 28 \t20 21 22 23 24 25 26 \n"
                + "25 26 27 28 29 30 31 \t29 30 \t\t\t27 28 29 30 31 \n";
        assertEquals(expectedOutput, new String(mockOutput.toByteArray()));
    }
    
	/*
	* Test hackaton testcases in commandSubHack.txt
	*
	**/
	   /**
     * 1st in the list
     * newlines in command sub output are not replaced with spaces.
     *
     * This bug is due to violating "Whitespace characters are used during the argument splitting step. Since our shell
     * does not support multi­line commands, newlines in OUT should be replaced with spaces." Ref spec Page 9 Section
     * Semantics Paragraph 3.
     */
    @Test
    public void testCommandSub() throws Exception {
        String cmdline = "echo `cat test1.txt`";
        createTestFileWithText("test1.txt", "line 1" + System.lineSeparator() + "line 2"
        		+ System.lineSeparator() + System.lineSeparator() + "line 3");
        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
        mockShell.parseAndEvaluate(cmdline, mockOutput);
        String expected = "line 1 line 2  line 3";
     
        assertEquals(expected, new String(mockOutput.toByteArray()).trim());
    }
    

    /**
     * 3rd in the list
     * (Head/Tail problem found via Command Sub)
     * Command sub returns -n with a space at the back which cannot be process as a flag by head/tail.
     *
     * This bug is due to displaying invalid args despite valid args as seen from
     * "OPTIONS – “­n 15” means printing 15 lines. Print first 10 lines if not specified." ref
     * spec page 10 section head paragraph 2.
     *
     * Problematic code at HeadApplication.java line 77.
     */
	 @Test
	    public void testSubPipeCatFmt() throws Exception {
	        String cmd = "head `cat flag.txt | fmt` 1 sort1.txt";
	        createTestFileWithText("flag.txt", "-n");
	        createTestFileWithText("sort1.txt", "sort1.txt");
	        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
	        mockShell.parseAndEvaluate(cmd, mockOutput);
	        
	        String expected = "sort1.txt" + System.lineSeparator();
	  
	        assertEquals(expected, mockOutput.toString());
	    }
	 
	 /**
	     * Echo is not properly evaluated as part of the output of the command sub but rather a print to the command line.
	     *4) 
	     * This bug is due to not conforming to "SUBCMD is evaluated as a separate shell command yielding the output OUT.
	     * SUBCMD together with the backquotes is substituted in CALL with OUT." Ref Spec Page 9 Section Semantics Paragraph 2.
	     */
	    @Test
	    public void testSubSequenceEchoCat() throws Exception {
	        String command = "echo testing 123: `echo showing content of file1.txt; cat file1.txt`";
	        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
	        createTestFileWithText("file1.txt", "apple" + System.lineSeparator() + "banana" + System.lineSeparator() +"eggplant");
	        mockShell.parseAndEvaluate(command, mockOutput);
	        assertEquals("testing 123: showing content of file1.txt apple banana eggplant" ,
	                mockOutput.toString().trim());
	    }
	    
	 /**
	  * 5th in line
	     * Plus and is are read as applications rather than arguments to the main echo. Echo supports multiple argument and
	     * should not be parsing the arguments as an app.
	     *
	     * This bug violates "The echo command writes its arguments separated by spaces and terminates by a newline on the
	     * standard output." ref specs page 10 section echo paragraph 1.
	     */
	    @Test
	    public void testMultipleSub() throws Exception {
	        String command = "echo `cat numbersort.txt| head -n 1` plus `cat numbersort.txt|sort|head -n 1` is `echo 65+1000` ";
	        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
	        createTestFileWithText("numbersort.txt", "65" + System.lineSeparator() +"9" + System.lineSeparator() + "1000" + System.lineSeparator() + "23" + System.lineSeparator() + "11");
	        mockShell.parseAndEvaluate(command, mockOutput);
	        assertEquals("65 plus 1000 is 65+1000" , mockOutput.toString().replaceAll("  ", " ").trim());
	    }
	    
	 /**
	  * 6th in the list
	     * Invalid syntax despite valid sequence command. Seems to occur when semicolon is used after a command
	     * substitution.
	     *
	     * This bug does not conform to the syntax "<seq> ::= <command> “;” <command>" ref specs page 8 section semi-colon
	     * paragraph 2.
	     *
	     */
	    @Test
	    public void testSemiColonAfterSub() throws Exception {
	        String command = "cat `cat comsub1.txt| head -n 1`; cat `cat comsub2.txt| head -n 1`";
	        createTestFileWithText("comsub1.txt", "file1.txt" + "\n" + "file2.txt" + "\n"+ "file3.txt");
	        createTestFileWithText("file1.txt", "apple" + System.lineSeparator() + "banana" + System.lineSeparator() +"eggplant");
	        createTestFileWithText("file2.txt", "apple" + System.lineSeparator() + "banana" + System.lineSeparator() + "banana"+ System.lineSeparator() + "zucchini");
	        createTestFileWithText("file3.txt", "18" + System.lineSeparator() + "15" + System.lineSeparator() + "14" + System.lineSeparator() + "11" + System.lineSeparator() + "13" + System.lineSeparator() + "8" +
	        		 System.lineSeparator() + "5"+ System.lineSeparator() + "1" + System.lineSeparator() + "6"+ System.lineSeparator() + "7" + System.lineSeparator() + "2" + System.lineSeparator() +"4"
	        				 + System.lineSeparator() + "16" + System.lineSeparator() + "3" + System.lineSeparator() + "17"+ System.lineSeparator() +"20"+ System.lineSeparator() +"19"+ System.lineSeparator() +"12"+ System.lineSeparator() +"10"
	        						 + System.lineSeparator() +"9");
	       
	        createTestFileWithText("comsub2.txt", "numbersort.txt");
	        createTestFileWithText("numbersort.txt", "65" + System.lineSeparator() +"9" + System.lineSeparator() + "1000" + System.lineSeparator() + "23" + System.lineSeparator() + "11");
	        
	        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
	        mockShell.parseAndEvaluate(command, mockOutput);
	        assertEquals("apple" + System.lineSeparator() + "banana" + System.lineSeparator() + "eggplant" + System.lineSeparator() + "65"
	                        + System.lineSeparator() + "9" + System.lineSeparator() + "1000" + System.lineSeparator() + "23"
	                        + System.lineSeparator() + "11"+ System.lineSeparator(), mockOutput.toString());
	    }
}


