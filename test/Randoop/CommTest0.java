package Randoop;

import junit.framework.*;

public class CommTest0 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test1"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var8 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var10 = new java.lang.String[] { ""};
    java.lang.String var11 = var8.commAllMatches(var10);
    java.lang.String var12 = var4.commOnlySecond(var10);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var13 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var15 = new java.lang.String[] { ""};
    java.lang.String var16 = var13.commAllMatches(var15);
    java.lang.String var17 = var4.commOnlyFirst(var15);
    java.lang.String var18 = var0.commOnlySecond(var15);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var19 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var21 = new java.lang.String[] { ""};
    java.lang.String var22 = var19.commAllMatches(var21);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var23 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var25 = new java.lang.String[] { ""};
    java.lang.String var26 = var23.commAllMatches(var25);
    java.lang.String var27 = var19.commOnlySecond(var25);
    java.lang.String var28 = var0.commAllMatches(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var8 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var10 = new java.lang.String[] { ""};
    java.lang.String var11 = var8.commAllMatches(var10);
    java.lang.String var12 = var4.commOnlySecond(var10);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var13 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var15 = new java.lang.String[] { ""};
    java.lang.String var16 = var13.commAllMatches(var15);
    java.lang.String var17 = var4.commOnlyFirst(var15);
    java.lang.String var18 = var0.commOnlySecond(var15);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var19 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var21 = new java.lang.String[] { ""};
    java.lang.String var22 = var19.commAllMatches(var21);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var23 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var25 = new java.lang.String[] { ""};
    java.lang.String var26 = var23.commAllMatches(var25);
    java.lang.String var27 = var19.commOnlySecond(var25);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var28 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var30 = new java.lang.String[] { ""};
    java.lang.String var31 = var28.commAllMatches(var30);
    java.lang.String var32 = var19.commOnlyFirst(var30);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var33 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var35 = new java.lang.String[] { ""};
    java.lang.String var36 = var33.commAllMatches(var35);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var37 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var39 = new java.lang.String[] { ""};
    java.lang.String var40 = var37.commAllMatches(var39);
    java.lang.String var41 = var33.commOnlySecond(var39);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var42 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var44 = new java.lang.String[] { ""};
    java.lang.String var45 = var42.commAllMatches(var44);
    java.lang.String var46 = var33.commOnlyFirst(var44);
    java.lang.String var47 = var19.commNoMatches(var44);
    java.lang.String[] var49 = new java.lang.String[] { ""};
    java.lang.String var50 = var19.commNoMatches(var49);
    java.lang.String var51 = var0.commOnlySecond(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test3"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var1 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var3 = new java.lang.String[] { ""};
    java.lang.String var4 = var1.commAllMatches(var3);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var5 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var7 = new java.lang.String[] { ""};
    java.lang.String var8 = var5.commAllMatches(var7);
    java.lang.String var9 = var1.commOnlySecond(var7);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var10 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var12 = new java.lang.String[] { ""};
    java.lang.String var13 = var10.commAllMatches(var12);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var14 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var16 = new java.lang.String[] { ""};
    java.lang.String var17 = var14.commAllMatches(var16);
    java.lang.String var18 = var10.commOnlySecond(var16);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var19 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var21 = new java.lang.String[] { ""};
    java.lang.String var22 = var19.commAllMatches(var21);
    java.lang.String var23 = var10.commOnlyFirst(var21);
    java.lang.String var24 = var1.commNoMatches(var21);
    java.lang.String var25 = var0.commOnlyFirst(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test4"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var8 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var10 = new java.lang.String[] { ""};
    java.lang.String var11 = var8.commAllMatches(var10);
    java.lang.String var12 = var4.commOnlySecond(var10);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var13 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var15 = new java.lang.String[] { ""};
    java.lang.String var16 = var13.commAllMatches(var15);
    java.lang.String var17 = var4.commOnlyFirst(var15);
    java.lang.String var18 = var0.commOnlySecond(var15);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var19 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var21 = new java.lang.String[] { ""};
    java.lang.String var22 = var19.commAllMatches(var21);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var23 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var25 = new java.lang.String[] { ""};
    java.lang.String var26 = var23.commAllMatches(var25);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var27 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var29 = new java.lang.String[] { ""};
    java.lang.String var30 = var27.commAllMatches(var29);
    java.lang.String var31 = var23.commOnlySecond(var29);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var32 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var34 = new java.lang.String[] { ""};
    java.lang.String var35 = var32.commAllMatches(var34);
    java.lang.String var36 = var23.commOnlyFirst(var34);
    java.lang.String var37 = var19.commOnlySecond(var34);
    java.lang.String var38 = var0.commOnlyFirst(var34);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var39 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var41 = new java.lang.String[] { ""};
    java.lang.String var42 = var39.commAllMatches(var41);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var43 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var45 = new java.lang.String[] { ""};
    java.lang.String var46 = var43.commAllMatches(var45);
    java.lang.String var47 = var39.commOnlySecond(var45);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var48 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var50 = new java.lang.String[] { ""};
    java.lang.String var51 = var48.commAllMatches(var50);
    java.lang.String var52 = var39.commOnlyFirst(var50);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var53 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var55 = new java.lang.String[] { ""};
    java.lang.String var56 = var53.commAllMatches(var55);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var57 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var59 = new java.lang.String[] { ""};
    java.lang.String var60 = var57.commAllMatches(var59);
    java.lang.String var61 = var53.commOnlySecond(var59);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var62 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var64 = new java.lang.String[] { ""};
    java.lang.String var65 = var62.commAllMatches(var64);
    java.lang.String var66 = var53.commOnlyFirst(var64);
    java.lang.String var67 = var39.commNoMatches(var64);
    java.lang.String var68 = var0.commOnlyFirst(var64);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var52);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var59);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var64);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var65);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var66);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var67);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var68);

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var8 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var10 = new java.lang.String[] { ""};
    java.lang.String var11 = var8.commAllMatches(var10);
    java.lang.String var12 = var4.commOnlySecond(var10);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var13 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var15 = new java.lang.String[] { ""};
    java.lang.String var16 = var13.commAllMatches(var15);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var17 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var19 = new java.lang.String[] { ""};
    java.lang.String var20 = var17.commAllMatches(var19);
    java.lang.String var21 = var13.commOnlySecond(var19);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var22 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var24 = new java.lang.String[] { ""};
    java.lang.String var25 = var22.commAllMatches(var24);
    java.lang.String var26 = var13.commOnlyFirst(var24);
    java.lang.String var27 = var4.commNoMatches(var24);
    java.lang.String var28 = var0.commOnlyFirst(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test6"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var8 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var10 = new java.lang.String[] { ""};
    java.lang.String var11 = var8.commAllMatches(var10);
    java.lang.String var12 = var4.commOnlySecond(var10);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var13 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var15 = new java.lang.String[] { ""};
    java.lang.String var16 = var13.commAllMatches(var15);
    java.lang.String var17 = var4.commOnlyFirst(var15);
    java.lang.String[] var19 = new java.lang.String[] { "hi!"};
    java.lang.String var20 = var4.commOnlyFirst(var19);
    java.lang.String var21 = var0.commBothMathches(var19);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var22 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var24 = new java.lang.String[] { ""};
    java.lang.String var25 = var22.commAllMatches(var24);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var26 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var28 = new java.lang.String[] { ""};
    java.lang.String var29 = var26.commAllMatches(var28);
    java.lang.String var30 = var22.commOnlySecond(var28);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var31 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var33 = new java.lang.String[] { ""};
    java.lang.String var34 = var31.commAllMatches(var33);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var35 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var37 = new java.lang.String[] { ""};
    java.lang.String var38 = var35.commAllMatches(var37);
    java.lang.String var39 = var31.commOnlySecond(var37);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var40 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var42 = new java.lang.String[] { ""};
    java.lang.String var43 = var40.commAllMatches(var42);
    java.lang.String var44 = var31.commOnlyFirst(var42);
    java.lang.String var45 = var22.commNoMatches(var42);
    java.lang.String var46 = var0.commBothMathches(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    java.lang.String var8 = var0.commOnlySecond(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var9 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var11 = new java.lang.String[] { ""};
    java.lang.String var12 = var9.commAllMatches(var11);
    java.lang.String var13 = var0.commOnlyFirst(var11);
    java.lang.String[] var15 = new java.lang.String[] { "hi!"};
    java.lang.String var16 = var0.commOnlyFirst(var15);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var17 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var19 = new java.lang.String[] { ""};
    java.lang.String var20 = var17.commAllMatches(var19);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var21 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var23 = new java.lang.String[] { ""};
    java.lang.String var24 = var21.commAllMatches(var23);
    java.lang.String var25 = var17.commOnlySecond(var23);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var26 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var28 = new java.lang.String[] { ""};
    java.lang.String var29 = var26.commAllMatches(var28);
    java.lang.String var30 = var17.commOnlyFirst(var28);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var31 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var33 = new java.lang.String[] { ""};
    java.lang.String var34 = var31.commAllMatches(var33);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var35 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var37 = new java.lang.String[] { ""};
    java.lang.String var38 = var35.commAllMatches(var37);
    java.lang.String var39 = var31.commOnlySecond(var37);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var40 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var42 = new java.lang.String[] { ""};
    java.lang.String var43 = var40.commAllMatches(var42);
    java.lang.String var44 = var31.commOnlyFirst(var42);
    java.lang.String var45 = var17.commNoMatches(var42);
    java.lang.String var46 = var0.commNoMatches(var42);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var47 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var49 = new java.lang.String[] { ""};
    java.lang.String var50 = var47.commAllMatches(var49);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var51 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var53 = new java.lang.String[] { ""};
    java.lang.String var54 = var51.commAllMatches(var53);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var55 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var57 = new java.lang.String[] { ""};
    java.lang.String var58 = var55.commAllMatches(var57);
    java.lang.String var59 = var51.commOnlySecond(var57);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var60 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var62 = new java.lang.String[] { ""};
    java.lang.String var63 = var60.commAllMatches(var62);
    java.lang.String var64 = var51.commOnlyFirst(var62);
    java.lang.String var65 = var47.commOnlySecond(var62);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var66 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var68 = new java.lang.String[] { ""};
    java.lang.String var69 = var66.commAllMatches(var68);
    java.lang.String var70 = var47.commAllMatches(var68);
    java.lang.String var71 = var0.commBothMathches(var68);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var72 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var74 = new java.lang.String[] { ""};
    java.lang.String var75 = var72.commAllMatches(var74);
    java.lang.String var76 = var0.commOnlyFirst(var74);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var57);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var58);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var59);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var62);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var63);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var64);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var65);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var68);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var69);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var70);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var71);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var74);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var75);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var76);

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


    sg.edu.nus.comp.cs4218.impl.app.CommApplication var0 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var2 = new java.lang.String[] { ""};
    java.lang.String var3 = var0.commAllMatches(var2);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var4 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var6 = new java.lang.String[] { ""};
    java.lang.String var7 = var4.commAllMatches(var6);
    java.lang.String var8 = var0.commOnlySecond(var6);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var9 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var11 = new java.lang.String[] { ""};
    java.lang.String var12 = var9.commAllMatches(var11);
    java.lang.String var13 = var0.commOnlyFirst(var11);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var14 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var16 = new java.lang.String[] { ""};
    java.lang.String var17 = var14.commAllMatches(var16);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var18 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var20 = new java.lang.String[] { ""};
    java.lang.String var21 = var18.commAllMatches(var20);
    java.lang.String var22 = var14.commOnlySecond(var20);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var23 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var25 = new java.lang.String[] { ""};
    java.lang.String var26 = var23.commAllMatches(var25);
    java.lang.String var27 = var14.commOnlyFirst(var25);
    java.lang.String var28 = var0.commNoMatches(var25);
    java.lang.String[] var30 = new java.lang.String[] { ""};
    java.lang.String var31 = var0.commNoMatches(var30);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var32 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var34 = new java.lang.String[] { ""};
    java.lang.String var35 = var32.commAllMatches(var34);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var36 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var38 = new java.lang.String[] { ""};
    java.lang.String var39 = var36.commAllMatches(var38);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var40 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var42 = new java.lang.String[] { ""};
    java.lang.String var43 = var40.commAllMatches(var42);
    java.lang.String var44 = var36.commOnlySecond(var42);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var45 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var47 = new java.lang.String[] { ""};
    java.lang.String var48 = var45.commAllMatches(var47);
    java.lang.String var49 = var36.commOnlyFirst(var47);
    java.lang.String var50 = var32.commOnlySecond(var47);
    java.lang.String var51 = var0.commAllMatches(var47);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var52 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var54 = new java.lang.String[] { ""};
    java.lang.String var55 = var52.commAllMatches(var54);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var56 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var58 = new java.lang.String[] { ""};
    java.lang.String var59 = var56.commAllMatches(var58);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var60 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var62 = new java.lang.String[] { ""};
    java.lang.String var63 = var60.commAllMatches(var62);
    java.lang.String var64 = var56.commOnlySecond(var62);
    sg.edu.nus.comp.cs4218.impl.app.CommApplication var65 = new sg.edu.nus.comp.cs4218.impl.app.CommApplication();
    java.lang.String[] var67 = new java.lang.String[] { ""};
    java.lang.String var68 = var65.commAllMatches(var67);
    java.lang.String var69 = var56.commOnlyFirst(var67);
    java.lang.String[] var71 = new java.lang.String[] { "hi!"};
    java.lang.String var72 = var56.commOnlyFirst(var71);
    java.lang.String var73 = var52.commBothMathches(var71);
    java.lang.String var74 = var0.commBothMathches(var71);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var58);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var59);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var62);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var63);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var64);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var67);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var68);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var69);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var71);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var72);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var73);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var74);

  }

}
