package Randoop;


import junit.framework.*;

public class PipeCommandTestCases4 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test1"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test2"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test3"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test4"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test5"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test6"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test7"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test8"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test9"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test10"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test11"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test12() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test12"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test13"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test14"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test15"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test16"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test17"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test18() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test18"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test19"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test20"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test21"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test22"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test23"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test24"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test25"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test26"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test27"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test28"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test29"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test30"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test31"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test32"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test33() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test33"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test34"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test35() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test35"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test36"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test37"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test38"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test39"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test40"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test41"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test42() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test42"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test43"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test44"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test45"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test46"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test47"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test48"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test49"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test50() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test50"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test51"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test52"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test53() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test53"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test54"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test55() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test55"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test56"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test57() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test57"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test58() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test58"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test59() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test59"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test60() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test60"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test61() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test61"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test62() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test62"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test63() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test63"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test64() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test64"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test65() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test65"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test66() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test66"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test67() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test67"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test68() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test68"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test69() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test69"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test70() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test70"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test71() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test71"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test72() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test72"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test73() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test73"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test74() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test74"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test75() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test75"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test76() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test76"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test77() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test77"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test78() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test78"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test79() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test79"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test80() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test80"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test81() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test81"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test82() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test82"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test83() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test83"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test84() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test84"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test85() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test85"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test86() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test86"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test87() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test87"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test88() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test88"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test89() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test89"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test90() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test90"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test91() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test91"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test92() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test92"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test93() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test93"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test94() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test94"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test95() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test95"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test96() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test96"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test97() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test97"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test98() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test98"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test99() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test99"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test100() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test100"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test101() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test101"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test102() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test102"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test103() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test103"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test104() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test104"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test105() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test105"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test106() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test106"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test107() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test107"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test108() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test108"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test109() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test109"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test110() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test110"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test111() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test111"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test112() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test112"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test113() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test113"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test114() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test114"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test115() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test115"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test116() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test116"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test117() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test117"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test118() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test118"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test119() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test119"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test120() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test120"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test121() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test121"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test122() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test122"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test123() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test123"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test124() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test124"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test125() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test125"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test126() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test126"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test127() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test127"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test128() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test128"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test129() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test129"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test130() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test130"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test131() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test131"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test132() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test132"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test133() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test133"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test134() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test134"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test135() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test135"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test136() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test136"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test137() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test137"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test138() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test138"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test139() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test139"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test140() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test140"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test141() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test141"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test142() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test142"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test143() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test143"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test144() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test144"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test145() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test145"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test146() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test146"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test147() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test147"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test148() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test148"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test149() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test149"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test150() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test150"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test151() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test151"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test152() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test152"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test153() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test153"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test154() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test154"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test155() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test155"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test156() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test156"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test157() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test157"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test158() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test158"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test159() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test159"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test160() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test160"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test161() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test161"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test162() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test162"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test163() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test163"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test164() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test164"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test165() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test165"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test166() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test166"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test167() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test167"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test168() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test168"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test169() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test169"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test170() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test170"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test171() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test171"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test172() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test172"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test173() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test173"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test174() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test174"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test175() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test175"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test176() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test176"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test177() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test177"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test178() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test178"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test179() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test179"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test180() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test180"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test181() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test181"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test182() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test182"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test183() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test183"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test184() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test184"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test185() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test185"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test186() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test186"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test187() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test187"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test188() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test188"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test189() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test189"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test190() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test190"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test191() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test191"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test192() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test192"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test193() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test193"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test194() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test194"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test195() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test195"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test196() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test196"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test197() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test197"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test198() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test198"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test199() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test199"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test200() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test200"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test201() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test201"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test202() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test202"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test203() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test203"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test204() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test204"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test205() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test205"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test206() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test206"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test207() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test207"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test208() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test208"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test209() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test209"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test210() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test210"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test211() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test211"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test212() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test212"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test213() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test213"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test214() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test214"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test215() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test215"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test216() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test216"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test217() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test217"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test218() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test218"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test219() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test219"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test220() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test220"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test221() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test221"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test222() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test222"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test223() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test223"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test224() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test224"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test225() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test225"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test226() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test226"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test227() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test227"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test228() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test228"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test229() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test229"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test230() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test230"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test231() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test231"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test232() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test232"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test233() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test233"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test234() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test234"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test235() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test235"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test236() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test236"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test237() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test237"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test238() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test238"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test239() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test239"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test240() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test240"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test241() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test241"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test242() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test242"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test243() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test243"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test244() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test244"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test245() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test245"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test246() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test246"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test247() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test247"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test248() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test248"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test249() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test249"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test250() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test250"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test251() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test251"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test252() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test252"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test253() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test253"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test254() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test254"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test255() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test255"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test256() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test256"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test257() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test257"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test258() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test258"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test259() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test259"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test260() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test260"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test261() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test261"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test262() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test262"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test263() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test263"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test264() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test264"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test265() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test265"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test266() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test266"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test267() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test267"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test268() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test268"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test269() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test269"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test270() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test270"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test271() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test271"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test272() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test272"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test273() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test273"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test274() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test274"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test275() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test275"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test276() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test276"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test277() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test277"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test278() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test278"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test279() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test279"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test280() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test280"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test281() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test281"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test282() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test282"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test283() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test283"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test284() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test284"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test285() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test285"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test286() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test286"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test287() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test287"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test288() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test288"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test289() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test289"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test290() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test290"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test291() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test291"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test292() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test292"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test293() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test293"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test294() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test294"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test295() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test295"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test296() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test296"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test297() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test297"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test298() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test298"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test299() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test299"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test300() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test300"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test301() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test301"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test302() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test302"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test303() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test303"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test304() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test304"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test305() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test305"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test306() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test306"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test307() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test307"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test308() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test308"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test309() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test309"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test310() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test310"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test311() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test311"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test312() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test312"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test313() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test313"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test314() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test314"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test315() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test315"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test316() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test316"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test317() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test317"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test318() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test318"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test319() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test319"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test320() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test320"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test321() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test321"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test322() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test322"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test323() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test323"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test324() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test324"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test325() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test325"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test326() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test326"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test327() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test327"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test328() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test328"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test329() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test329"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test330() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test330"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test331() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test331"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test332() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test332"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test333() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test333"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test334() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test334"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test335() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test335"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test336() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test336"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test337() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test337"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test338() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test338"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test339() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test339"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test340() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test340"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test341() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test341"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test342() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test342"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test343() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test343"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test344() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test344"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test345() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test345"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test346() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test346"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test347() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test347"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test348() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test348"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test349() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test349"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test350() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test350"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test351() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test351"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test352() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test352"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test353() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test353"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test354() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test354"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test355() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test355"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test356() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test356"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test357() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test357"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test358() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test358"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test359() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test359"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test360() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test360"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test361() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test361"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test362() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test362"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test363() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test363"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test364() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test364"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test365() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test365"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test366() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test366"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test367() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test367"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test368() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test368"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test369() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test369"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test370() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test370"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test371() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test371"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test372() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test372"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test373() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test373"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test374() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test374"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test375() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test375"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test376() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test376"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test377() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test377"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test378() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test378"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test379() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test379"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test380() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test380"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test381() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test381"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test382() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test382"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test383() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test383"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test384() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test384"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test385() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test385"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test386() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test386"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test387() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test387"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test388() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test388"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test389() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test389"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test390() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test390"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test391() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test391"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test392() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test392"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test393() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test393"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test394() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test394"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test395() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test395"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test396() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test396"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test397() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test397"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test398() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test398"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test399() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test399"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test400() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test400"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test401() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test401"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test402() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test402"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test403() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test403"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test404() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test404"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test405() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test405"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test406() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test406"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test407() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test407"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test408() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test408"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test409() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test409"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test410() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test410"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test411() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test411"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test412() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test412"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test413() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test413"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test414() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test414"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test415() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test415"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test416() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test416"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test417() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test417"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test418() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test418"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test419() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test419"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test420() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test420"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test421() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test421"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test422() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test422"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test423() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test423"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test424() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test424"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test425() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test425"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test426() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test426"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test427() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test427"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test428() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test428"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test429() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test429"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test430() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test430"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test431() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test431"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test432() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test432"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test433() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test433"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test434() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test434"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test435() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test435"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test436() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test436"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test437() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test437"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test438() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test438"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test439() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test439"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test440() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test440"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test441() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test441"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test442() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test442"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test443() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test443"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test444() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test444"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test445() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test445"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test446() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test446"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test447() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test447"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test448() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test448"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test449() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test449"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test450() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test450"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test451() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test451"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test452() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test452"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test453() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test453"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test454() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test454"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test455() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test455"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test456() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test456"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test457() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test457"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test458() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test458"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test459() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test459"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test460() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test460"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test461() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test461"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test462() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test462"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test463() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test463"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test464() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test464"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test465() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test465"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test466() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test466"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test467() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test467"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test468() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test468"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test469() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test469"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test470() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test470"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test471() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test471"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test472() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test472"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test473() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test473"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test474() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test474"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test475() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test475"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test476() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test476"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test477() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test477"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test478() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test478"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test479() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test479"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test480() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test480"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test481() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test481"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();

  }

  public void test482() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test482"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test483() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test483"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test484() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test484"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test485() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test485"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();

  }

  public void test486() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test486"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test487() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test487"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test488() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test488"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test489() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test489"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test490() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test490"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test491() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test491"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test492() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test492"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test493() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test493"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();

  }

  public void test494() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test494"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();

  }

  public void test495() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test495"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test496() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test496"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();

  }

  public void test497() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test497"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

  public void test498() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test498"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();

  }

  public void test499() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test499"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();
    var1.parse();

  }

  public void test500() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest2.test500"); }


    sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand var1 = new sg.edu.nus.comp.cs4218.impl.cmd.PipeCommand("hi!");
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.parse();
    var1.parse();
    var1.terminate();
    var1.terminate();
    var1.parse();
    var1.parse();

  }

}
