package sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.io.OutputStream;

import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;

public class PipeCommandTest {

	private PipeCommand pipeCmd;
	private static InputStream stdin;
	private static OutputStream stdout;
	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception " + "for valid command.";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		stdin = System.in;
		stdout = System.out;
	}

	@Test
	public void testParseNoSemicolon() {
		pipeCmd = new PipeCommand("ls");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 1);
	}

	@Test
	public void testParseValidSemicolon() {
		pipeCmd = new PipeCommand("ls; ls");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			// TODO Auto-generated catch block
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 1);
	}

	@Test
	public void testParseValidDivider() {
		pipeCmd = new PipeCommand("ls | cat");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 2);
	}

	@Test
	public void testParseValidDividerInDoubleQuote() {
		pipeCmd = new PipeCommand("echo \"token1| token 2 \" | cat");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 2);
	}

	@Test
	public void testParseValidSemiColonInDoubleQuote() {
		pipeCmd = new PipeCommand("echo \"token1; token 2 \" | cat");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 2);
	}

	@Test
	public void testParseValidDoubleQuote() {
		pipeCmd = new PipeCommand("echo \"token1 | token2; token3\" | cat");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 2);
	}

	@Test
	public void testEvaluateNoSemicolon() {
		pipeCmd = new PipeCommand("ls");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidSemicolon() {
		pipeCmd = new PipeCommand("ls; ls");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidDivider() {
		pipeCmd = new PipeCommand("ls | cat");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidDividerInDoubleQuote() {
		pipeCmd = new PipeCommand("echo \"token1| token 2 \" | cat");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidSemiColonInDoubleQuote() {
		pipeCmd = new PipeCommand("echo \"token1; token 2 \" | cat");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidDoubleQuote() {
		pipeCmd = new PipeCommand("echo \"token1 | token2; token3\" | cat");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidDoubleQuote() throws AbstractApplicationException, ShellException {
		pipeCmd = new PipeCommand("echo \"token1 | token2; token3 | cat;");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidArgs() throws ShellException, AbstractApplicationException {
		pipeCmd = new PipeCommand("ls token1;");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidSemiColon() throws ShellException, AbstractApplicationException {
		pipeCmd = new PipeCommand("echo \" token1; ");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidSemiColons() throws ShellException, AbstractApplicationException {
		pipeCmd = new PipeCommand("echo token1; ;");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidApp() throws ShellException, AbstractApplicationException {
		pipeCmd = new PipeCommand("invalidApp token1;");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

	/***************************************************************************************
	 * *************************************************************************
	 * ************
	 * *************************************************************************
	 * ************
	 * *************************************************************************
	 * ************
	 * *************************************************************************
	 * ************ add more test case for pipe operator
	 * *************************************************************************
	 * ************
	 * *************************************************************************
	 * ************
	 * *************************************************************************
	 * ************
	 * *************************************************************************
	 * ************
	 ****************************************************************************************/
	@Test
	public void testParseNoDivider() {
		pipeCmd = new PipeCommand("ls");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			// TODO Auto-generated catch block
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 1);
	}

	@Test
	public void testParseDividerInSingleQuotes() {
		pipeCmd = new PipeCommand("echo 'token1| echo'");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 1);
	}

	@Test
	public void testParseDivider() {
		pipeCmd = new PipeCommand("cat files.txt | sort");
		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(pipeCmd.getSequenceCommandList().size(), 2);
	}

	@Test
	public void testParseDividerInBackQuotes() {
		pipeCmd = new PipeCommand("echo `token1| echo`");

		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}

		assertEquals(pipeCmd.getSequenceCommandList().size(), 1);
	}

	@Test
	public void testParseMutipleDivider() {
		pipeCmd = new PipeCommand("echo token1| echo | head -n 1");

		try {
			pipeCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}

		assertEquals(pipeCmd.getSequenceCommandList().size(), 3);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInvalidPipe() throws ShellException, AbstractApplicationException {
		pipeCmd = new PipeCommand("|echo token1");
		pipeCmd.parse();
		assertEquals(pipeCmd.getSequenceCommandList().size(), 1);
		pipeCmd.evaluate(stdin, stdout);
	}

	@Test
	public void testEvaluateDivider() {
		pipeCmd = new PipeCommand("ls");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidDividerInDoubleQuote1() {
		pipeCmd = new PipeCommand("echo \"token1; token 2 |\"");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateValidDividerInBackQuote() {
		pipeCmd = new PipeCommand("echo `echo lalal`|cat");
		try {
			pipeCmd.parse();
			pipeCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidSingleQuote() throws AbstractApplicationException, ShellException {
		pipeCmd = new PipeCommand("echo 'token1 | token2; token3 | cat;");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidBackQuote() throws AbstractApplicationException, ShellException {
		pipeCmd = new PipeCommand("echo `token1 | token2; token3 | cat;");
		pipeCmd.parse();
		pipeCmd.evaluate(stdin, stdout);
	}

}
