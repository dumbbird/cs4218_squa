package sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.io.OutputStream;

import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;

public class SequenceCommandTest {

	private SequenceCommand sequenceCmd;
	private static InputStream stdin;
	private static OutputStream stdout;
	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception " + "for valid command.";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		stdin = System.in;
		stdout = System.out;
	}

	@Test
	public void testParseNoSemicolon() {
		sequenceCmd = new SequenceCommand("ls");
		try {
			sequenceCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(sequenceCmd.getCallCommandList().size(), 1);
	}

	@Test
	public void testParseValidSemicolon() {
		sequenceCmd = new SequenceCommand("ls; ls");
		try {
			sequenceCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(sequenceCmd.getCallCommandList().size(), 2);
	}

	@Test
	public void testParseSemicolonInDoubleQuotes() {
		sequenceCmd = new SequenceCommand("echo \"token1; token2\"");
		try {
			sequenceCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(sequenceCmd.getCallCommandList().size(), 1);
	}

	@Test
	public void testParseSemicolonInSingleQuotes() {
		sequenceCmd = new SequenceCommand("echo 'token1; token2'");
		try {
			sequenceCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(sequenceCmd.getCallCommandList().size(), 1);
	}

	@Test
	public void testParseSemicolonInBackQuotes() {
		sequenceCmd = new SequenceCommand("echo `ls; ls`");
		try {
			sequenceCmd.parse();
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		}
		assertEquals(sequenceCmd.getCallCommandList().size(), 1);
	}

	@Test
	public void testEvaluateNoSemicolon() {
		sequenceCmd = new SequenceCommand("ls");
		try {
			sequenceCmd.parse();
			sequenceCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test(expected = ShellException.class)
	public void testParseInValidSemiColon() throws ShellException {
		sequenceCmd = new SequenceCommand("echo \" token1; ");
		sequenceCmd.parse();
	}

	@Test
	public void testEvaluateValidSemicolon() {
		sequenceCmd = new SequenceCommand("ls; ls");
		try {
			sequenceCmd.parse();
			sequenceCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateSemicolonInDoubleQuotes() {
		sequenceCmd = new SequenceCommand("echo \"token1; token2\"");
		try {
			sequenceCmd.parse();
			sequenceCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateSemicolonInSingleQuotes() {
		sequenceCmd = new SequenceCommand("echo 'token1; token2'");
		try {
			sequenceCmd.parse();
			sequenceCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testEvaluateSemicolonInBackQuotes() {
		sequenceCmd = new SequenceCommand("echo `ls; ls`");
		try {
			sequenceCmd.parse();
			sequenceCmd.evaluate(stdin, stdout);
		} catch (ShellException | AbstractApplicationException e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidSemiColon() throws ShellException, AbstractApplicationException {
		sequenceCmd = new SequenceCommand("echo \" token1; ");
		sequenceCmd.parse();
		sequenceCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidSemiColons() throws ShellException, AbstractApplicationException {
		sequenceCmd = new SequenceCommand("echo token1; ;");
		sequenceCmd.parse();
		sequenceCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidApp() throws ShellException, AbstractApplicationException {
		sequenceCmd = new SequenceCommand("invalidApp token1;");
		sequenceCmd.parse();
		sequenceCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInvalidPipe() throws ShellException, AbstractApplicationException {
		sequenceCmd = new SequenceCommand("echo token1 | cat");
		sequenceCmd.parse();
		sequenceCmd.evaluate(stdin, stdout);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidArgs() throws ShellException, AbstractApplicationException {
		sequenceCmd = new SequenceCommand("ls token1;");
		sequenceCmd.parse();
		sequenceCmd.evaluate(stdin, stdout);
	}
}
