package sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;

public class GlobbingTest {
	private static ShellImpl shell;
	private static OutputStream outputStream;
	final static String TEST_STR = "Testing Stream";
	final static String TEST_FILE_NAME = "testShell.txt";
	final static String TEST_FILE_NAME2 = "testShell2.txt";
	final static String TEST_FILE_NAME1 = "Globbing Test.txt";
	final static String TEST_FILE_NAME3 = "GlobbingTest1.txt";
	final static String TEST_FILE_NAME4 = "GlobbingTest2.txt";
	final static String TEST_FILE_NAME5 = "Globbing Test3.py";
	final static String TEST_FILE_NAME6 = "GlobbingTest4.cpp";
	final static String TEST_FILE_NAME7 = "Globbing Test5.txt";
	final static String TEST_FILE_NAME8 = "GlobbingTest6.txt";
	final static String TEST_FILE_NAME9 = "Globbing Test7.html";
	final static String TEST_FILE_NAME10 = "GlobbingTest8.css";
	final static String TEST_FILE_NAME11 = "GlobbingTest9.js";
	final static String TEST_FILE_NAME12 = "GlobbingTest10.xml";
	final static String TEST_FOLDER_NAME = "GlobbingTest";
	final static String TEST_FOLDER_NAME2 = "MultiFileFolder";
	final static String TEST_FOLDER_NAME3 = "SingleFileFolder";
	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception for valid command.";
	final static String VALID_FILE_NO_EXP = "Not supposed to have exception for valid file.";
	final static String VALID_STRM_NO_EXP = "Not supposed to have exception for valid streams.";
	final static String READONLY_EXP = "Supposed to have exception opening outputstream to read-only file.";
	final static String VALID_EXP = "Valid Exception thrown";
	final static String MISSING_EXP = "Should have exception thrown";
	static String originalFilePath;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		shell = new ShellImpl();
		outputStream = System.out;
		
		createTestFolder(TEST_FOLDER_NAME);
		createTestFolder(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2);
		createTestFolder(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME3);
		
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME3 + "/" + TEST_FILE_NAME1);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME2);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME3);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME4);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME5);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME6);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME7);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME8);
		createTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME9);
		originalFilePath = Environment.currentDirectory;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME3 + "/" + TEST_FILE_NAME1);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME2);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME3);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FILE_NAME4);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME5);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME6);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME7);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME8);
		removeTestFile(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2 + "/" + TEST_FILE_NAME9);
		removeTestFolder(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME2);
		removeTestFolder(TEST_FOLDER_NAME + "/" + TEST_FOLDER_NAME3);
		removeTestFolder(TEST_FOLDER_NAME);		
	}

	public static void createTestFile(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR.getBytes());
	}

	public static void removeTestFile(String fileName) throws IOException {
		File file = new File(fileName);
		file.setWritable(true);
		file.delete();
	}

	public static void createTestFolder(String folderName) throws IOException {
		new File(folderName).mkdir();
	}

	public static void removeTestFolder(String folderName) throws IOException {
		File file = new File(folderName + "\\\\");

		String[] entries = file.list();
		if (entries != null) {
			for (String s : entries) {
				File currentFile = new File(file.getPath(), s);
				currentFile.delete();
			}
		}

		file.delete();
		Environment.currentDirectory = originalFilePath;
	}

	public void writeToStream(OutputStream myoutputStream) throws IOException {
		myoutputStream.write(TEST_STR.getBytes());
		myoutputStream.flush();
		myoutputStream.close();
	}

	public String fileToString(String fileName) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(fileName));
		String fileStr = scanner.useDelimiter("\\Z").next();
		scanner.close();

		return fileStr;
	}

	@Test
	public void testInvalidGlob() {
		String readLine = "cat *";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
			fail(MISSING_EXP);
		} catch (Exception e) {
			String exceptionMsg = "shell: Invalid globbing scenario";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
	
	@Test
	public void testInvalidGlob2() {
		String readLine = "cat /*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
			fail(MISSING_EXP);
		} catch (Exception e) {
			String exceptionMsg = "shell: Invalid globbing scenario";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
	
	@Test
	public void testInvalidGlob3() {
		String readLine = "cat " + TEST_FOLDER_NAME + "/*/invalid";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
			fail(MISSING_EXP);
		} catch (Exception e) {
			String exceptionMsg = "shell: Invalid globbing scenario";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
	
	@Test
	public void testInvalidPath() {
		String readLine = "cat ///*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
			fail(MISSING_EXP);
		} catch (Exception e) {
			String exceptionMsg = "shell: Invalid globbing scenario";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
	
	@Test
	public void testGlobNoPaths() {
		String readLine = "cat srctest/*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
			fail(MISSING_EXP);
		} catch (Exception e) {
			String exceptionMsg = "cat: No such file exists";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testGlobOneFile() {
		String readLine = "cat " + TEST_FOLDER_NAME + "/SingleFileFolder/*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			//System.out.println(e.getMessage());
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testGlobFilesDirectories() {
		String readLine = "cat " + TEST_FOLDER_NAME + "/MultiFileFolder/*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			//System.out.println(e.getMessage());
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testGlobMultiLevel() {
		String readLine = "cat " + TEST_FOLDER_NAME + "/*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testPathWithBackSlash() {
		String readLine = "cat " + TEST_FOLDER_NAME + "\\*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);			
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	@Test
	public void testPathWithMultiSlash() {
		String readLine = "cat " + TEST_FOLDER_NAME + "//\\/\\*";
		try {
			shell.parseAndEvaluate(readLine, outputStream);			
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}
}
