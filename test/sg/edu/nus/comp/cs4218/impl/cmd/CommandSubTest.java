package sg.edu.nus.comp.cs4218.impl.cmd;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;

public class CommandSubTest {
	private static ShellImpl shell;
	private static OutputStream outputStream;
	final static String TEST_STR = "Testing Stream";
	final static String TEST_FILE_NAME = "testShell.txt";
	final static String TEST_FILE_NAME2 = "testShell2.txt";
	final static String TEST_FOLDER_NAME = "testShellFolder";
	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception for valid command.";
	final static String VALID_FILE_NO_EXP = "Not supposed to have exception for valid file.";
	final static String VALID_STRM_NO_EXP = "Not supposed to have exception for valid streams.";
	final static String READONLY_EXP = "Supposed to have exception opening outputstream to read-only file.";
	final static String VALID_EXP = "Valid Exception thrown";
	final static String MISSING_EXP = "Should have exception thrown";
	static String originalFilePath;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		shell = new ShellImpl();
		outputStream = System.out;
		createTestFile(TEST_FILE_NAME);
		createTestFolder(TEST_FOLDER_NAME);
		originalFilePath = Environment.currentDirectory;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		removeTestFile(TEST_FILE_NAME);
		removeTestFile(TEST_FILE_NAME2);
		removeTestFile("files1.txt");
		removeTestFile("files2.txt");
		removeTestFile("files3.txt");
		removeTestFile("files4.txt");
		removeTestFile("files5.txt");
		removeTestFile("files6.txt");
		removeTestFile("text1.txt");
		removeTestFile("test1.txt");
		removeTestFile("sort1.txt");
		removeTestFile("flag.txt");
		removeTestFolder(TEST_FOLDER_NAME);
	}

	public static void createTestFile(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR.getBytes());
	}
	
	public static void createTestFileWithText(String fileName, String text) throws IOException {
		Files.write(Paths.get(fileName), text.getBytes());
	}

	public static void removeTestFile(String fileName) throws IOException {
		File file = new File(fileName);
		file.setWritable(true);
		file.delete();
	}

	public static void createTestFolder(String folderName) throws IOException {
		new File(folderName).mkdir();
	}

	public static void removeTestFolder(String folderName) throws IOException {
		File file = new File(folderName + "\\\\");

		String[] entries = file.list();
		if (entries != null) {
			for (String s : entries) {
				File currentFile = new File(file.getPath(), s);
				currentFile.delete();
			}
		}

		file.delete();
		Environment.currentDirectory = originalFilePath;
	}

	public void writeToStream(OutputStream myoutputStream) throws IOException {
		myoutputStream.write(TEST_STR.getBytes());
		myoutputStream.flush();
		myoutputStream.close();
	}

	public String fileToString(String fileName) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(fileName));
		String fileStr = scanner.useDelimiter("\\Z").next();
		scanner.close();

		return fileStr;
	}

	// Test command Substitution
	@Test
	public void testOneCommandSubOfPipe() {
		String readLine = "echo `cat files1.txt | head -n 1`";
		try {
			createTestFile("files1.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testOneCommandSubOfSemiColon() {
		String readLine = "echo `echo Showing contents of text1.txt ; cat text1.txt`";
		try {

			createTestFile("text1.txt");
			createTestFile(TEST_FILE_NAME2);
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testOneCommandSub() {
		String readLine = "echo `cat files2.txt`";
		try {

			createTestFile("files2.txt");
			createTestFile(TEST_FILE_NAME2);
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testMultipleCommandSub() {
		String readLine = "echo `cat files3.txt` ; echo `cat " + TEST_FILE_NAME2 + "`";
		try {

			createTestFile("files3.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testMultiplePipeSub() {
		String readLine = "echo `cat files4.txt | head -n 1` | cat";
		try {

			createTestFile("files4.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testMultipleSemicolonSub() {
		String readLine = "echo `echo Showing contents of text1.txt cat text1.txt` ; echo yea";
		try {

			createTestFile("text1.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testMultipleCommandSubOfPipeAndSemiC() {
		String readLine = "echo `cat files3.txt` ; echo `cat " + TEST_FILE_NAME2 + "` | echo";
		try {

			createTestFile("files3.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testInputWithDoubleQuote() {
		String readLine = "echo \"`echo vggg`\"";
		try {
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testInputWithNewLine() {
		String readLine = "echo `cat " + "\n" + "files5.txt`";
		try {

			createTestFile("files5.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}
	}

	@Test
	public void testInputSingleQuote() throws IOException, AbstractApplicationException, ShellException {
		String readLine = "echo '`cat files6.txt`' ";

		try {
			createTestFile("files6.txt");
			shell.parseAndEvaluate(readLine, outputStream);
		} catch (Exception e) {
			fail(VALID_CMD_NO_EXP);
		}

	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidBackQuote() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo ` token1 ";
		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidBackQuotes() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo ` token1 ```";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidApp() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "invalidApp `token1;token2`";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInvalidPipe() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo `echo token1 |`";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInvalidSemicolon() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo `echo token1 ;`";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInvalidCmdSub() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo `token1`";
		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidArgs() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "`echo token1`";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidArg() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo `token1 echo` echo";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	@Test(expected = ShellException.class)
	public void testEvaluateInValidBQContainBQ() throws ShellException, AbstractApplicationException, IOException {
		String readLine = "echo `echo `echo token 1``";

		shell.parseAndEvaluate(readLine, outputStream);
	}

	
	/*Test hackaton testcases in commandSubHack.txt
	*
	*
	*
	*
	**/
	   /**
     * 1st in the list
     * newlines in command sub output are not replaced with spaces.
     *
     * This bug is due to violating "Whitespace characters are used during the argument splitting step. Since our shell
     * does not support multi­line commands, newlines in OUT should be replaced with spaces." Ref spec Page 9 Section
     * Semantics Paragraph 3.
     */
    @Test
    public void testCommandSub() throws Exception {
        String cmdline = "echo `cat test1.txt`";
        createTestFileWithText("test1.txt", "line 1" + "\n" + "line 2"
        +"\n" + "\n" + "line 3");
        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
        shell.parseAndEvaluate(cmdline, mockOutput);
        String expected = "line 1 line 2  line 3";
     
        assertEquals(expected, new String(mockOutput.toByteArray()).trim());
    }
    

    /**
     * 3rd in the list
     * (Head/Tail problem found via Command Sub)
     * Command sub returns -n with a space at the back which cannot be process as a flag by head/tail.
     *
     * This bug is due to displaying invalid args despite valid args as seen from
     * "OPTIONS – “­n 15” means printing 15 lines. Print first 10 lines if not specified." ref
     * spec page 10 section head paragraph 2.
     *
     * Problematic code at HeadApplication.java line 77.
     */
	 @Test
	    public void testSubPipeCatFmt() throws Exception {
	        String cmd = "head `cat flag.txt | fmt` 1 sort1.txt";
	        createTestFileWithText("flag.txt", "-n");
	        createTestFileWithText("sort1.txt", "sort1.txt");
	        ByteArrayOutputStream mockOutput = new ByteArrayOutputStream();
	        shell.parseAndEvaluate(cmd, mockOutput);
	        
	        String expected = "sort1.txt" + System.lineSeparator();
	  
	        assertEquals(expected, mockOutput.toString());
	    }
	 
	 //
}
