package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.SortException;

public class SortApplicationTest {

	private SortApplication sortApplication;
	private InputStream stdin;
	private OutputStream stdout;
	
	private static final String FILE1 = "sort_file1.txt";
	private static final Path PATH_FILE1 = Paths.get(FILE1);
	private static final String FILE1_CONTENT = "1\n2\n10";
	private static final String FILE1_SORT_DEFAULT = "1\r\n10\r\n2\r\n";
	private static final String FILE1_SORT_NUM = "1\r\n2\r\n10\r\n";
	
	private static final String FILE2 = "sort_file2.txt";
	private static final Path PATH_FILE2 = Paths.get(FILE2);
	private static final String FILE2_CONTENT = "yO1\\n\n2Yo\\t\n1yO0\\r\n2yO0\\n\n";
	private static final String FILE2_SORT_DEFAULT = "1yO0\\r\r\n2Yo\\t\r\n2yO0\\n\r\nyO1\\n\r\n";
	private static final String FILE2_SORT_NUM = "1yO0\\r\r\n2Yo\\t\r\n2yO0\\n\r\nyO1\\n\r\n";
	
	private static final String FILE3 = "sort_file3.txt";
	private static final Path PATH_FILE3 = Paths.get(FILE3);
	private static final String FILE3_CONTENT = "一\\n\n二\\t\n三\\r\n四\\n\n";
	private static final String FILE3_SORT_DEFAULT = "一\\n\r\n三\\r\r\n二\\t\r\n四\\n\r\n";
	//private static final String FILE3_SORT_NUM = "1yO0\\r\r\n2Yo\\t\r\n2yO0\\n\r\nyO1\\n\r\n";
	
	private static final String EXP_NULL_POINTER = "Null Pointer Exception";
	private static final String EXP_FILE_NOT_FOUND = "No such file exists";
	private static final String EXP_NO_ARGS = "No arguments!";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Files.deleteIfExists(PATH_FILE1);
		Files.deleteIfExists(PATH_FILE2);
		
		Files.createFile(PATH_FILE1);
		File file1 = new File(PATH_FILE1.toString());	
		FileWriter fileWriter = new FileWriter(file1);
		fileWriter.write(FILE1_CONTENT);
		fileWriter.flush();
		fileWriter.close();
		
		Files.createFile(PATH_FILE2);
		File file2 = new File(PATH_FILE2.toString());	
		fileWriter = new FileWriter(file2);
		fileWriter.write(FILE2_CONTENT);
		fileWriter.flush();
		fileWriter.close();
		
		Files.createFile(PATH_FILE3);
		File file3 = new File(PATH_FILE3.toString());	
		fileWriter = new FileWriter(file3);
		fileWriter.write(FILE3_CONTENT);
		fileWriter.flush();
		fileWriter.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Files.deleteIfExists(PATH_FILE1);
		Files.deleteIfExists(PATH_FILE2);
		Files.deleteIfExists(PATH_FILE3);
	}

	@Before
	public void setUp() throws Exception {
		sortApplication = new SortApplication();
		stdin = null;
		stdout = new ByteArrayOutputStream();
	}

	@After
	public void tearDown() throws Exception {
		sortApplication = null;
		stdin = null;
		stdout = null;
	}

	@Test
	public void testStdoutNull() {
		String[] args = null;
		stdout = null;
		try {
			sortApplication.run(args, stdin, stdout);
			fail ("Should throw exception");
		} catch (SortException e) {
			assertEquals("sort: " + EXP_NULL_POINTER, e.getMessage());
		}
	}
	
	@Test
	public void testArgsNullStdinNull() {
		String[] args = null;
		try {
			sortApplication.run(args, stdin, stdout);
			fail ("Should throw exception");
		} catch (SortException e) {
			assertEquals("sort: " + EXP_NULL_POINTER, e.getMessage());
		}
	}
	
	@Test
	public void testArgsEmptyStdinNull() {
		String[] args = {};
		try {
			sortApplication.run(args, stdin, stdout);
			fail ("Should throw exception");
		} catch (SortException e) {
			assertEquals("sort: " + EXP_NO_ARGS, e.getMessage());
		}
	}
	
	@Test
	public void testSortStdin() throws SortException {
		String[] args = {};
		//System.out.println(stdout.toString());
		stdin = new ByteArrayInputStream("1\n2\n10".getBytes());
		sortApplication.run(args, stdin, stdout);
		assertEquals("1\r\n10\r\n2\r\n", stdout.toString());
	}
	
	@Test
	public void testSortStdinWithUTF8() throws SortException {
		String[] args = {};
		//System.out.println(stdout.toString());
		stdin = new ByteArrayInputStream("1 。一\n2 。二\n10 。十".getBytes());
		sortApplication.run(args, stdin, stdout);
		assertEquals("1 。一\r\n10 。十\r\n2 。二\r\n", stdout.toString());
	}
	
	@Test
	public void testSortUniqueStringsOddArgs() throws SortException {
		String[] toSort = {"1", "2", "10"};
		sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1","10","2"}), Arrays.toString(toSort));
	}
	
	@Test
	public void testSortUniqueStringsEvenArgs() throws SortException {
		String[] toSort = {"1", "2", "10", "101"};
		sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1","10","101","2"}), Arrays.toString(toSort));
	}
	
	@Test
	public void testSortOneArg() throws SortException {
		String[] toSort = {"1"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortNullArg() throws SortException {
		try {String[] toSort = null;
		sortApplication.run(toSort,null,stdout);
		} catch (SortException e) {
		assertEquals("sort: " + EXP_NULL_POINTER, e.getMessage());
		}
	}
	
	@Test
	public void testSortZeroArg() throws SortException {
		
		try{
		String[] toSort = new String[0];	
		sortApplication.run(toSort, null,stdout);
		} catch (SortException e) {
			assertEquals("sort: " + EXP_NO_ARGS, e.getMessage());
		}
	}
	
	@Test
	public void testSortNonUniqueStrings() throws SortException {
		String[] toSort = {"5", "2", "5", "3", "2", "1"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1", "2", "2", "3", "5", "5"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortUniqueNumbers() throws SortException {
		String[] toSort = {"1", "2", "10"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1","10", "2"}),Arrays.toString(sorted));
	}
	
	@Test
	public void testSortNonUniqueNumbers() throws SortException {
		String[] toSort = {"1", "10", "2", "10"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1", "10", "10", "2"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortCapitalLetters() throws SortException {
		String[] toSort = {"sad", "sAd", "SaD", "SAD"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"SAD", "SaD", "sAd", "sad"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortCapitalAndNumbers() throws SortException {
		String[] toSort = {"sad1", "sAd2", "SaD3", "SAD4", "1"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1", "SAD4", "SaD3", "sAd2", "sad1"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortNonAlphabeticCharacters() throws SortException {
		String[] toSort = {"!", "#", "$", "%"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"!", "#", "$", "%"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortSpecialCharacters() throws SortException {
		String[] toSort = {"\\n", "\\t", "\\r", "\\\\"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"\\\\", "\\n", "\\r", "\\t"}),Arrays.toString(sorted));
	}
	
	@Test
	public void testSortCapitalAndSpecialCharacters() throws SortException {
		String[] toSort = {"hEy\\n", "hEy\\t", "hEy\\r", "Hey\\n"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"Hey\\n", "hEy\\n","hEy\\r" ,"hEy\\t"}),Arrays.toString(sorted));
	}
	
	@Test
	public void testSortNumberAndSpecialCharacters() throws SortException {
		String[] toSort = {"1\\n", "2\\t", "10\\r", "20\\n"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"10\\r", "1\\n","20\\n" ,"2\\t"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortCapitalAndNumberAndSpecialCharacters() throws SortException {
		String[] toSort = {"yO1\\n", "2Yo\\t", "1yO0\\r", "2yO0\\n"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1yO0\\r", "2Yo\\t","2yO0\\n" ,"yO1\\n"}), Arrays.toString(sorted));
	}
	
	@Test
	public void testSortCapitalAndNumberAndSpecialCharactersAndSpaces() throws SortException {
		String[] toSort = {"yO1\\n", "2Yo\\t", "1yO0\\r", "2yO0\\n"};
		String[] sorted = sortApplication.sort(toSort);
		assertEquals(Arrays.toString(new String[]{"1yO0\\r", "2Yo\\t","2yO0\\n" ,"yO1\\n"}),Arrays.toString(sorted));
	}
	
	@Test
	public void testSortDefaultModeSimpleFile() throws SortException {
		String[] args = {PATH_FILE1.toString()};
		sortApplication.run(args, stdin, stdout);
		assertEquals(FILE1_SORT_DEFAULT, stdout.toString());
	}
	
	@Test
	public void testSortNumberModeSimpleFile() throws SortException {
		String[] args = {"-n", PATH_FILE1.toString()};
		sortApplication.run(args, stdin, stdout);
		assertEquals(FILE1_SORT_NUM, stdout.toString());
	}
	
	@Test
	public void testSortDefaultModeComplexFile() throws SortException {
		String[] args = {PATH_FILE2.toString()};
		sortApplication.run(args, stdin, stdout);
		assertEquals(FILE2_SORT_DEFAULT, stdout.toString());
	}
	
	@Test
	public void testSortDefaultModeUTF8File() throws SortException {
		String[] args = {PATH_FILE3.toString()};
		sortApplication.run(args, stdin, stdout);
		assertEquals(FILE3_SORT_DEFAULT, stdout.toString());
	}
	
	@Test
	public void testSortNumberModeComplexFile() throws SortException {
		String[] args = {"-n", PATH_FILE2.toString()};
		sortApplication.run(args, stdin, stdout);
		assertEquals(FILE2_SORT_NUM, stdout.toString());
	}
	
	
	@Test
	public void testSortNonExistentFile() {
		String[] args = {"ghost.txt"};
		try {
			sortApplication.run(args, stdin, stdout);
			fail ("Should throw exception");
		} catch (SortException e) {
			assertEquals("sort: " + EXP_FILE_NOT_FOUND, e.getMessage());
		}
	}
	
	@Test
	public void testSortInsufficientArguments() {
		String[] args = {"-n"};
		try{
			sortApplication.run(args, null, stdout);
		} catch (SortException e){
			assertEquals("sort: " + "Invalid arguments!", e.getMessage());
		}
	}
	
	@Test
	public void testSortUserEnteredWordsTreatFirstWordAsNumber() {
		String[] args = {"-n", "1", "100", "2"};
		String expectedResult = "1" + System.lineSeparator()+ "2" + System.lineSeparator() + "100" + System.lineSeparator();
		try {
			sortApplication.run(args, null, stdout);
			assertEquals(expectedResult,stdout.toString());
		} catch (SortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
