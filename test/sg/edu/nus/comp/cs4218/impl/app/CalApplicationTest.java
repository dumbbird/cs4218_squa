package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sg.edu.nus.comp.cs4218.exception.CalException;
import sg.edu.nus.comp.cs4218.impl.app.CalApplication;

public class CalApplicationTest {
	private CalApplication calApp;
	private String[] args;
	private ByteArrayOutputStream outStream;
	public static final String APP_EXCEPTION = "cal: ";
	public static final String FAIL_MSG = "Should throw exception";

	@Before
	public void setUp() {
		calApp = new CalApplication();
		args = null;
	}

	@After
	public void tearDown() {
		calApp = null;
		args = null;
	}

	@Test
	public void testPrintCal() {
		String expectedResult = null;
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		
		switch (month){
		case 3:
		expectedResult = "   March 2016\n" + "Su Mo Tu We Th Fr Sa\n" + "      1  2  3  4  5  \n"
				+ "6  7  8  9  10 11 12 \n" + "13 14 15 16 17 18 19 \n" + "20 21 22 23 24 25 26 \n" + "27 28 29 30 31 \n"; 
		break;
		
		case 4:
		expectedResult = "   April 2016\n" + "Su Mo Tu We Th Fr Sa\n" + "               1  2  \n"
				+ "3  4  5  6  7  8  9  \n" + "10 11 12 13 14 15 16 \n" + "17 18 19 20 21 22 23 \n" + "24 25 26 27 28 29 30 \n"; 
		break;
		}
		String actualResult = calApp.printCal(args);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testPrintCalWithMondayFirst() throws CalException {
		args = new String[1];
		args[0] = "-m";
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		String expectedResult = null;
		
		switch (month) {
		case 3: expectedResult = "   March 2016\n" + "Mo Tu We Th Fr Sa Su\n" + "   1  2  3  4  5  6  \n" 
				+ "7  8  9  10 11 12 13 \n" + "14 15 16 17 18 19 20 \n" + "21 22 23 24 25 26 27 \n" + "28 29 30 31 \n";
		break;
		
		case 4: expectedResult = "   April 2016\n" + "Mo Tu We Th Fr Sa Su\n" + "            1  2  3  \n"
				+ "4  5  6  7  8  9  10 \n" + "11 12 13 14 15 16 17 \n" + "18 19 20 21 22 23 24 \n" + "25 26 27 28 29 30 \n";
		break;
		}
		String actualResult = calApp.printCalWithMondayFirst(args);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testPrintCalMondayFirstWithInvalidStartDayOfWeekException() {
		args = new String[1];
		args[0] = "-t";

		try {
			calApp.run(args, null, outStream);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid input specified for start of week.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testPrintCalForMonthYear() throws CalException {
		String expectedResult = "   January 2016\n" + "Su Mo Tu We Th Fr Sa\n" + "               1  2  \n"
				+ "3  4  5  6  7  8  9  \n" + "10 11 12 13 14 15 16 \n" + "17 18 19 20 21 22 23 \n"
				+ "24 25 26 27 28 29 30 \n" + "31 \n";
		args = new String[2];
		args[0] = "Jan";
		args[1] = "2016";
		String actualResult = calApp.printCalForMonthYear(args);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testInvalidInputMonthForPrintMonthYear() {
		args = new String[2];
		args[0] = "abcde";
		args[1] = "2016";
		try {
			calApp.printCalForMonthYear(args);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid input specified for month.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
	
	@Test
	public void testInvalidStartOfWeek() {
		args = new String[2];
		args[0] = "-t";
		args[1] = "2016";
		
		try {
			calApp.run(args,null,outStream);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid input specified for start of week.";
			assertEquals(exceptionMsg, e.getMessage());
		}
		
		
	}
	
	@Test
	public void testInvalidYearNotInteger() {
		args = new String[2];
		args [0] = "Mar";
		args[1] = "2016ab";
		
		try {
			calApp.run(args,null,outStream);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Integer value should be specified for year.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testPrintCalForYear() throws CalException {
		String expectedResult = "   January 2016\t\t   February 2016\t   March 2016\n" + "Su Mo Tu We Th Fr Sa" + "\t"
				+ "Su Mo Tu We Th Fr Sa" + "\t" + "Su Mo Tu We Th Fr Sa\n" + "               1  2"
				+ "  \t   1  2  3  4  5  6  \t      1  2  3  4  5  \n"
				+ "3  4  5  6  7  8  9  \t7  8  9  10 11 12 13 \t6  7  8  9  10 11 12 \n"
				+ "10 11 12 13 14 15 16 \t14 15 16 17 18 19 20 \t13 14 15 16 17 18 19 \n"
				+ "17 18 19 20 21 22 23 \t21 22 23 24 25 26 27 \t20 21 22 23 24 25 26 \n"
				+ "24 25 26 27 28 29 30 \t28 29 \t\t\t27 28 29 30 31 \n" + "31 \n\n"
				+ "   April 2016\t\t   May 2016\t\t   June 2016\n" + "Su Mo Tu We Th Fr Sa" + "\t"
				+ "Su Mo Tu We Th Fr Sa" + "\t" + "Su Mo Tu We Th Fr Sa\n" + "               1  2"
				+ "  \t1  2  3  4  5  6  7  \t         1  2  3  4  \n"
				+ "3  4  5  6  7  8  9  \t8  9  10 11 12 13 14 \t5  6  7  8  9  10 11 \n"
				+ "10 11 12 13 14 15 16 \t15 16 17 18 19 20 21 \t12 13 14 15 16 17 18 \n"
				+ "17 18 19 20 21 22 23 \t22 23 24 25 26 27 28 \t19 20 21 22 23 24 25 \n"
				+ "24 25 26 27 28 29 30 \t29 30 31 \t\t26 27 28 29 30 \n\n"
				+ "   July 2016\t\t   August 2016\t   September 2016\n" + "Su Mo Tu We Th Fr Sa" + "\t"
				+ "Su Mo Tu We Th Fr Sa" + "\t" + "Su Mo Tu We Th Fr Sa\n" + "               1  2  "
				+ "\t   1  2  3  4  5  6  \t            1  2  3  \n"
				+ "3  4  5  6  7  8  9  \t7  8  9  10 11 12 13 \t4  5  6  7  8  9  10 \n"
				+ "10 11 12 13 14 15 16 \t14 15 16 17 18 19 20 \t11 12 13 14 15 16 17 \n"
				+ "17 18 19 20 21 22 23 \t21 22 23 24 25 26 27 \t18 19 20 21 22 23 24 \n"
				+ "24 25 26 27 28 29 30 \t28 29 30 31 \t\t25 26 27 28 29 30 \n" + "31 \n\n"
				+ "   October 2016\t\t   November 2016\t   December 2016\n" + "Su Mo Tu We Th Fr Sa" + "\t"
				+ "Su Mo Tu We Th Fr Sa" + "\t" + "Su Mo Tu We Th Fr Sa\n"
				+ "                  1  \t      1  2  3  4  5  \t            1  2  3  \n"
				+ "2  3  4  5  6  7  8  \t6  7  8  9  10 11 12 \t4  5  6  7  8  9  10 \n"
				+ "9  10 11 12 13 14 15 \t13 14 15 16 17 18 19 \t11 12 13 14 15 16 17 \n"
				+ "16 17 18 19 20 21 22 \t20 21 22 23 24 25 26 \t18 19 20 21 22 23 24 \n"
				+ "23 24 25 26 27 28 29 \t27 28 29 30 \t\t25 26 27 28 29 30 31 \n" + "30 31 \n";
		args = new String[1];
		args[0] = "2016";
		String actualResult = calApp.printCalForYear(args);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testPrintCalForYearInvalidInputFloatingPointYear() {
		args = new String[1];
		args[0] = "2016.5";

		try {
			calApp.run(args, null, outStream);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Integer value should be specified for year.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testPrintCalForYearInvalidInputNonNumericYear() {
		args = new String[1];
		args[0] = "abcde";

		try {
			calApp.run(args, null, outStream);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Integer value should be specified for year.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testPrintCalForYearInvalidInputAlphaNumericYear() {
		args = new String[1];
		args[0] = "abcde2016";

		try {
			calApp.run(args, null, outStream);
			fail(FAIL_MSG);
		} catch (CalException e) {
			String exceptionMsg = APP_EXCEPTION + "Integer value should be specified for year.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testPrintCalForMonthYearMondayFirst() throws CalException {
		String expectedResult = "   January 2016\n" + "Mo Tu We Th Fr Sa Su\n" + "            1  2  3  \n"
				+ "4  5  6  7  8  9  10 \n" + "11 12 13 14 15 16 17 \n" + "18 19 20 21 22 23 24 \n"
				+ "25 26 27 28 29 30 31 \n";
		args = new String[3];
		args[0] = "-m";
		args[1] = "Jan";
		args[2] = "2016";
		String actualResult = calApp.printCalForMonthYearMondayFirst(args);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testPrintCalForYearMondayFirst() throws CalException {
		String expectedResult = "   January 2016\t\t   February 2016\t   March 2016\n" + "Mo Tu We Th Fr Sa Su" + "\t"
				+ "Mo Tu We Th Fr Sa Su" + "\t" + "Mo Tu We Th Fr Sa Su\n" + "            1  2  3"
				+ "  \t1  2  3  4  5  6  7  \t   1  2  3  4  5  6  \n"
				+ "4  5  6  7  8  9  10 \t8  9  10 11 12 13 14 \t7  8  9  10 11 12 13 \n"
				+ "11 12 13 14 15 16 17 \t15 16 17 18 19 20 21 \t14 15 16 17 18 19 20 \n"
				+ "18 19 20 21 22 23 24 \t22 23 24 25 26 27 28 \t21 22 23 24 25 26 27 \n"
				+ "25 26 27 28 29 30 31 \t29 \t\t\t28 29 30 31 \n" + "\n"
				+ "   April 2016\t\t   May 2016\t\t   June 2016\n" + "Mo Tu We Th Fr Sa Su" + "\t"
				+ "Mo Tu We Th Fr Sa Su" + "\t" + "Mo Tu We Th Fr Sa Su\n" + "            1  2  3"
				+ "  \t                  1  \t      1  2  3  4  5  \n"
				+ "4  5  6  7  8  9  10 \t2  3  4  5  6  7  8  \t6  7  8  9  10 11 12 \n"
				+ "11 12 13 14 15 16 17 \t9  10 11 12 13 14 15 \t13 14 15 16 17 18 19 \n"
				+ "18 19 20 21 22 23 24 \t16 17 18 19 20 21 22 \t20 21 22 23 24 25 26 \n"
				+ "25 26 27 28 29 30 \t23 24 25 26 27 28 29 \t27 28 29 30 \n" + "\t\t\t30 31 \n\n"
				+ "   July 2016\t\t   August 2016\t   September 2016\n" + "Mo Tu We Th Fr Sa Su" + "\t"
				+ "Mo Tu We Th Fr Sa Su" + "\t" + "Mo Tu We Th Fr Sa Su\n" + "            1  2  3  "
				+ "\t1  2  3  4  5  6  7  \t         1  2  3  4  \n"
				+ "4  5  6  7  8  9  10 \t8  9  10 11 12 13 14 \t5  6  7  8  9  10 11 \n"
				+ "11 12 13 14 15 16 17 \t15 16 17 18 19 20 21 \t12 13 14 15 16 17 18 \n"
				+ "18 19 20 21 22 23 24 \t22 23 24 25 26 27 28 \t19 20 21 22 23 24 25 \n"
				+ "25 26 27 28 29 30 31 \t29 30 31 \t\t26 27 28 29 30 \n" + "\n"
				+ "   October 2016\t\t   November 2016\t   December 2016\n" + "Mo Tu We Th Fr Sa Su" + "\t"
				+ "Mo Tu We Th Fr Sa Su" + "\t" + "Mo Tu We Th Fr Sa Su\n"
				+ "               1  2  \t   1  2  3  4  5  6  \t         1  2  3  4  \n"
				+ "3  4  5  6  7  8  9  \t7  8  9  10 11 12 13 \t5  6  7  8  9  10 11 \n"
				+ "10 11 12 13 14 15 16 \t14 15 16 17 18 19 20 \t12 13 14 15 16 17 18 \n"
				+ "17 18 19 20 21 22 23 \t21 22 23 24 25 26 27 \t19 20 21 22 23 24 25 \n"
				+ "24 25 26 27 28 29 30 \t28 29 30 \t\t26 27 28 29 30 31 \n" + "31 \n";
		args = new String[2];
		args[0] = "-m";
		args[1] = "2016";
		String actualResult = calApp.printCalForYearMondayFirst(args);
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testExceedMaximumNumberOfArguments() {
		args = new String[5];
		args[0] = "-m";
		args[1] = "Jan";
		args[2] = "2016";
		args[3] = "abcd";
		args[4] = "abcde";

		try {
			calApp.run(args, null, outStream);
		} catch (CalException e) {
			String expectedResult = APP_EXCEPTION + "Too many arguments, maximum is three.";
			assertEquals(expectedResult, e.getMessage());
		}

	}
	
	@Test
	public void testCheckLeapYearValidLeapYear() {
		assertTrue(calApp.checkLeapYear(2000));
	}
	
	@Test
	public void testCheckLeapYearInvalidLeapYear() {
		assertFalse(calApp.checkLeapYear(2005));
	}
}
