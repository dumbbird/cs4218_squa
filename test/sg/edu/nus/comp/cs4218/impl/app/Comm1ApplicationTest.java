package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.OSCheck;
import sg.edu.nus.comp.cs4218.WindowsPermission;
import sg.edu.nus.comp.cs4218.exception.CommException;
import sg.edu.nus.comp.cs4218.impl.app.CommApplication;

public class Comm1ApplicationTest {
    private CommApplication commApp;
    private String[] args;
    static String tempFilePath1 = "file1.txt";
    static String tempFilePath2 = "file2.txt";
    static String tempFileNotExisting = "fileNotExisting.txt";
    private File tempFile1;
    private File tempFile2;
    private FileOutputStream fos1, fos2;
    private ByteArrayOutputStream outStream;
    public static final String FAIL_MSG = "Should throw an exception!";
    public static final String APP_EXCEPTION = "comm: ";

    @Before
    public void setUp() throws Exception {
        commApp = new CommApplication();
        args = null;

        try {
            tempFile1 = new File(tempFilePath1);
            tempFile1.createNewFile();
            tempFile2 = new File(tempFilePath2);
            tempFile2.createNewFile();
        } catch (SecurityException se) {
            fail("Cannot create temporary file to test");
        }
        outStream = new ByteArrayOutputStream();
        fos1 = new FileOutputStream(tempFile1);
        fos2 = new FileOutputStream(tempFile2);

    }

    @After
    public void tearDown() throws Exception {
        commApp = null;
        args = null;
        fos1.close();
        fos2.close();
        tempFile1.delete();
        tempFile2.delete();

    }

    @Test
    public void testNullInputStreamException() {
        try {
            commApp.run(args, null, outStream);
            fail(FAIL_MSG);
        } catch (Exception e) {
            assertEquals(APP_EXCEPTION + CommApplication.EXP_INVALID_ARGS,
                    e.getMessage());
        }
    }

    @Test
    public void testBothValidFilesNoMatches() throws CommException {
        ByteArrayInputStream is1 = new ByteArrayInputStream(
                "apple\nbanana\npear".getBytes());
        ByteArrayInputStream is2 = new ByteArrayInputStream(
                "mango\norange\nwatermelon".getBytes());
        String result = commApp.comm(is1, is2);
        assertEquals(
                "apple" + System.lineSeparator() + "banana"
                        + System.lineSeparator() + CommApplication.SINGLE_TAB
                        + "mango" + System.lineSeparator()
                        + CommApplication.SINGLE_TAB + "orange"
                        + System.lineSeparator() + "pear"
                        + System.lineSeparator() + CommApplication.SINGLE_TAB
                        + "watermelon" + System.lineSeparator(), result);

    }

    @Test
    public void testBothValidFilesFirstLineMatches() throws IOException,
            CommException {
        ByteArrayInputStream is1 = new ByteArrayInputStream(
                "apple\nbanana\npear".getBytes());
        ByteArrayInputStream is2 = new ByteArrayInputStream(
                "apple\norange\nwatermelon".getBytes());
        String result = commApp.comm(is1, is2);
        assertEquals(
                CommApplication.DOUBLE_TAB + "apple" + System.lineSeparator()
                        + "banana" + System.lineSeparator()
                        + CommApplication.SINGLE_TAB + "orange"
                        + System.lineSeparator() + "pear"
                        + System.lineSeparator() + CommApplication.SINGLE_TAB
                        + "watermelon" + System.lineSeparator(), result);
    }

    @Test
    public void testBothValidFilesFirstSecondLineMatches() throws IOException,
            CommException {
        ByteArrayInputStream is1 = new ByteArrayInputStream(
                "apple\nbanana\neggplant".getBytes());
        ByteArrayInputStream is2 = new ByteArrayInputStream(
                "apple\nbanana\nbanana\nzucchini".getBytes());
        String result = commApp.comm(is1, is2);
        assertEquals(
                CommApplication.DOUBLE_TAB + "apple" + System.lineSeparator()
                        + CommApplication.DOUBLE_TAB + "banana"
                        + System.lineSeparator() + CommApplication.SINGLE_TAB
                        + "banana" + System.lineSeparator() + "eggplant"
                        + System.lineSeparator() + CommApplication.SINGLE_TAB
                        + "zucchini" + System.lineSeparator(), result);
    }

    @Test
    public void testBothValidFilesAllMatch() throws IOException, CommException {
        ByteArrayInputStream is1 = new ByteArrayInputStream(
                "apple\nbanana\nwatermelon".getBytes());
        ByteArrayInputStream is2 = new ByteArrayInputStream(
                "apple\nbanana\nwatermelon".getBytes());
        String result = commApp.comm(is1, is2);
        assertEquals(
                CommApplication.DOUBLE_TAB + "apple" + System.lineSeparator()
                        + CommApplication.DOUBLE_TAB + "banana"
                        + System.lineSeparator() + CommApplication.DOUBLE_TAB
                        + "watermelon" + System.lineSeparator(), result);
    }

    @Test
    public void testOneFileNotExistingException() {
        try {
            args = new String[] { tempFileNotExisting, tempFilePath2 };
            commApp.run(args, null, outStream);
            fail(FAIL_MSG);
        } catch (CommException e) {
            String exceptionMsg = APP_EXCEPTION
                    + CommApplication.EXP_FNF_EXCEPTION;
            assertEquals(exceptionMsg, e.getMessage());
        }
    }

    @Test
    public void testOneFileIsDirException() {
        File fileDir = new File("tempCommDir");
        fileDir.mkdir();

        try {
            args = new String[] { "tempCommDir", tempFilePath2 };
            commApp.run(args, null, outStream);
            fail(FAIL_MSG);
        } catch (CommException e) {
            String exceptionMsg = APP_EXCEPTION + "This is a directory";
            assertEquals(exceptionMsg, e.getMessage());
        }

        fileDir.delete();
    }

    @Test
    public void testNotReadableFile() throws IOException {
        tempFile1.setReadable(false); // Unix
        if (OSCheck.isWindows()) {
            WindowsPermission.setReadable(tempFile1, false); // Windows
        }

        args = new String[] { tempFilePath1, tempFilePath2 };
        try {
            commApp.run(args, null, outStream);
            fail(FAIL_MSG);
        } catch (CommException e) {
            assertEquals(APP_EXCEPTION
                    + CommApplication.EXP_FILE_UNREADABLE_EXCEPTION,
                    e.getMessage());
        }
    }

    @Test
    public void testUnsortedOrder() throws IOException, CommException {
        ByteArrayInputStream is1 = new ByteArrayInputStream(
                "mango\nbanana\napple".getBytes());
        ByteArrayInputStream is2 = new ByteArrayInputStream(
                "apple\nbanana\nwatermelon".getBytes());

        String result = commApp.comm(is1, is2);
        
        assertEquals(
                CommApplication.SINGLE_TAB + "apple" + System.lineSeparator()
                        + CommApplication.SINGLE_TAB + "banana" + System.lineSeparator() 
                        + "mango"  + System.lineSeparator()
                        + "banana"  + System.lineSeparator()
                        + "apple"  + System.lineSeparator()
                        + CommApplication.SINGLE_TAB + "watermelon" + System.lineSeparator(), result);
    }

    @Test
    public void testOneEmptyFile() throws IOException, CommException {
        ByteArrayInputStream is1 = new ByteArrayInputStream(
                "apple\nbanana\nmango".getBytes());
        ByteArrayInputStream is2 = new ByteArrayInputStream("".getBytes());
        String result = commApp.comm(is1, is2);
        assertEquals(
                "apple" + System.lineSeparator() + "banana"
                        + System.lineSeparator() + "mango"
                        + System.lineSeparator(), result);

    }

    @Test
    public void testBothEmptyFiles() {
        args = new String[] { tempFilePath1, tempFilePath2 };
        try {
            commApp.run(args, null, outStream);
            // fail(FAIL_MSG);
        } catch (CommException e) {
            assertEquals(APP_EXCEPTION
                    + CommApplication.EXP_FILE_UNREADABLE_EXCEPTION,
                    e.getMessage());
        }
    }
}
