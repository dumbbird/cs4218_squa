package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.DateException;

public class DateApplicationTest {

	private DateApplication app;
	private ByteArrayInputStream bais;
	private ByteArrayOutputStream baos;

	@Before
	public void setUp() throws Exception {
		app = new DateApplication();
		bais = new ByteArrayInputStream(new byte[1]);
		baos = new ByteArrayOutputStream();
	}

	// Test null parameters
	@Test
	public void testNullParams() throws DateException {
		String[] params = null;
		app.run(params, bais, baos);
	}

	// Test zero parameters
	@Test
	public void testZeroParams() throws DateException {
		String[] params = {};
		app.run(params, bais, baos);
	}

	// Test one parameter, but null value
	@Test(expected = DateException.class)
	public void testOneNullParams() throws DateException {
		String[] params = { null };
		app.run(params, bais, baos);
	}

	// Test that date prints correct date format
	@Test
	public void testExpectedBehavior() throws DateException, IOException {
		String[] params = {};
		Date currentT = Calendar.getInstance().getTime();
		try {
			app.run(params, bais, baos);
		} catch (DateException e) {
			fail("Unknown error occured");
		}
		String output = new String(baos.toByteArray());
		String timeStamp = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy").format(currentT);
		assertEquals(timeStamp.trim(), output.trim());
		
	}

}
