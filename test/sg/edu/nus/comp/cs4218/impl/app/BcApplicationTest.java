package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.exception.BcException;
import sg.edu.nus.comp.cs4218.impl.app.BcApplication;

public class BcApplicationTest {
	private BcApplication bcApp;
	private ByteArrayInputStream inStream;
	private ByteArrayOutputStream outStream;

	public static final String APP_EXCEPTION = "bc: ";
	public static final String SHOULDNOT_FAIL = "Should not throw exception";

	@Before
	public void setUp() throws Exception {
		bcApp = new BcApplication();
		inStream = new ByteArrayInputStream(new byte[1]);
		outStream = new ByteArrayOutputStream();
	}

	// Test null parameters
	@Test
	public void testNullParams() throws BcException {
		String[] params = null;
		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Null arguments";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	// Test zero parameters
	@Test
	public void testZeroParams() throws BcException {
		String[] params = {};
		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Null arguments";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	// Test one parameter, but null value
	@Test
	public void testOneNullParams() throws BcException {
		String[] params = { null };
		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Null arguments";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	// Test more than one parameters
	@Test
	public void testMultiParams() throws BcException {
		String[] params = { "1+2", "9-7" };
		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Too many arguments";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	// Test methods
	// method number
	public void testMethodNumber() {
		String[] args = {"1"};
		String result = bcApp.number(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodNegatePositive() {
		String[] args = {"123"};
		String result = bcApp.negate(args);
		String expected = "-123";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodNegateNegative() {
		String[] args = {"-123"};
		String result = bcApp.negate(args);
		String expected = "123";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodAdd() {
		String[] args = {"1", "2"};
		String result = bcApp.add(args);
		String expected = "3";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodSubtract() {
		String[] args = {"1", "2"};
		String result = bcApp.subtract(args);
		String expected = "-1";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodMultiply() {
		String[] args = {"1", "2"};
		String result = bcApp.multiply(args);
		String expected = "2";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodDivide() {
		String[] args = {"1", "2"};
		String result = bcApp.divide(args);
		String expected = "0.5";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodPow() {
		String[] args = {"2", "3"};
		String result = bcApp.pow(args);
		String expected = "8";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodBracket() {
		String[] args = {"(1)", "", ""};
		String result = bcApp.bracket(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodGreaterThanTrue() {
		String[] args = {"2", "1"};
		String result = bcApp.greaterThan(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodGreaterThanTFalse() {
		String[] args = {"1", "2"};
		String result = bcApp.greaterThan(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodGreaterThanOrEqualTrue1() {
		String[] args = {"2", "1"};
		String result = bcApp.greaterThanOrEqual(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodGreaterThanOrEqualTrue2() {
		String[] args = {"1", "1"};
		String result = bcApp.greaterThanOrEqual(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodGreaterThanOrEqualFalse() {
		String[] args = {"1", "2"};
		String result = bcApp.greaterThanOrEqual(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodLessThanTrue() {
		String[] args = {"1", "2"};
		String result = bcApp.lessThan(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodLessThanFalse() {
		String[] args = {"2", "1"};
		String result = bcApp.lessThan(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodLessThanOrEqualTrue1() {
		String[] args = {"1", "2"};
		String result = bcApp.lessThanOrEqual(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodLessThanOrEqualTrue2() {
		String[] args = {"1", "1"};
		String result = bcApp.lessThanOrEqual(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodLessThanOrEqualFalse() {
		String[] args = {"2", "1"};
		String result = bcApp.lessThanOrEqual(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodEqualEqualTrue() {
		String[] args = {"2", "2"};
		String result = bcApp.equalEqual(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodEqualEqualFalse() {
		String[] args = {"1", "2"};
		String result = bcApp.equalEqual(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodNotEqualTrue() {
		String[] args = {"1", "2"};
		String result = bcApp.notEqual(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodNotEqualFalse() {
		String[] args = {"2", "2"};
		String result = bcApp.notEqual(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodAndTT() {
		String[] args = {"123", "123"};
		String result = bcApp.and(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodAndTF() {
		String[] args = {"123", "0"};
		String result = bcApp.and(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodAndFT() {
		String[] args = {"0", "123"};
		String result = bcApp.and(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodAndFF() {
		String[] args = {"0", "0"};
		String result = bcApp.and(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodOrTT() {
		String[] args = {"123", "123"};
		String result = bcApp.or(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodOrTF() {
		String[] args = {"123", "0"};
		String result = bcApp.or(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodOrFT() {
		String[] args = {"0", "123"};
		String result = bcApp.or(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodOrFF() {
		String[] args = {"0", "0"};
		String result = bcApp.or(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}

	@Test
	public void testMethodNotF() {
		String[] args = {"0"};
		String result = bcApp.not(args);
		String expected = "1";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testMethodNotT() {
		String[] args = {"123"};
		String result = bcApp.not(args);
		String expected = "0";
		
		assertEquals(expected, result);
	}
	
	// Test real command cases
	// number
	@Test
	public void testNumber() throws BcException, IOException {

		String[] params = { "3.8989" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("3.8989"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// +
	@Test
	public void testAdd() throws BcException, IOException {

		String[] params = { "4.9 + 6.1" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("11"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// -
	@Test
	public void testSubstract() throws BcException, IOException {

		String[] params = { "3 - 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("-1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// *
	@Test
	public void testMultiply() throws BcException, IOException {

		String[] params = { "3 * 4.1" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("12.3"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// /
	@Test
	public void testDivide() throws BcException, IOException {

		String[] params = { "32 / 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("8"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// ^
	@Test
	public void testPow() throws BcException, IOException {

		String[] params = { "2 ^ 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("16"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// +, *, ()
	@Test
	public void testBracket() throws BcException, IOException {

		String[] params = { "(3 + 5) * 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("32"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// >
	@Test
	public void testGreaterThan() throws BcException, IOException {

		String[] params = { "3 > 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("0"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// >=
	@Test
	public void testGreaterThanOrEqual() throws BcException, IOException {

		String[] params = { "30 >= 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// <
	@Test
	public void testLessThan() throws BcException, IOException {

		String[] params = { "2.3 < 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// <=
	@Test
	public void testLessThanOrEqual() throws BcException, IOException {

		String[] params = { "1.5 <= 2.4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// ==
	@Test
	public void testEqualEqual() throws BcException, IOException {

		String[] params = { "3 == 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("0"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// !=
	@Test
	public void testNotEqual() throws BcException, IOException {

		String[] params = { "3 != 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// !=, &&, >
	@Test
	public void testAnd() throws BcException, IOException {

		String[] params = { "(3 != 4) && (5 > 2)" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// ==, ||, <
	@Test
	public void testOr() throws BcException, IOException {

		String[] params = { "(3 == 4) || (2 < 5)" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// !, !=
	@Test
	public void testNot() throws BcException, IOException {

		String[] params = { "!(3 != 4)" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("0"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// -x, >
	// note: we assume Not deals with boolean values
	// and Negate only deals with real numbers
	// so !(true) = 0, -(true) = -1
	@Test
	public void testNegateBoolean() throws BcException, IOException {

		String[] params = { "-(3 > -4)" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("-1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// -x, +
	@Test
	public void testNegativeNumber() throws BcException, IOException {

		String[] params = { "-3.3 + 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("0.7"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// -x, <
	@Test
	public void testNegativeNumberWithExpression() throws BcException, IOException {

		String[] params = { "-3 < 4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// -x, >
	@Test
	public void testNegativeNumberWithExpression2() throws BcException, IOException {

		String[] params = { "3 > -4" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testAddMultiplyMix() throws BcException, IOException {

		String[] params = { "3.5+4*6.5 >= 12" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testSubstractPowerDivideMix() throws BcException, IOException {

		String[] params = { "5-3^2/3" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("2"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testAndLtMix() throws BcException, IOException {

		String[] params = { "7<9 && 5.7<5.69" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("0"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// +, -, !=, >, ||, ()
	@Test
	public void testNotEqGtOrMix() throws BcException, IOException {

		String[] params = { "2+7!=9 || 2-(7>9)" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("1"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	// +, -, !=, >, ||, ()
	@Test
	public void testNestedBracket() throws BcException, IOException {

		String[] params = { "4^(3*(2.0-1.9)+(2-0.3))" };

		try {
			bcApp.run(params, inStream, outStream);
			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals("16"+System.lineSeparator(), outStream.toString());
		} catch (BcException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testInvalidChar() throws BcException, IOException {

		String[] params = { "4#(3*(2.0-1.9)+(2-0.3)" };

		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid expression";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testInvalidBracket() throws BcException, IOException {

		String[] params = { "4^(3*(2.0-1.9)+(2-0.3)" };

		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid expression";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testInvalidOperatorMix() throws BcException, IOException {

		String[] params = { "4+-*/3" };

		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid expression";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testInvalidOperatorMix2() throws BcException, IOException {

		String[] params = { ">!+&&===3" };

		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid expression";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
	
	@Test
	public void testDivideByZero() throws BcException, IOException {

		String[] params = { "6.9 / 0" };

		try {
			bcApp.run(params, inStream, outStream);
			fail("Should have thrown exception but did not!");
		} catch (BcException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid expression";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}
}
