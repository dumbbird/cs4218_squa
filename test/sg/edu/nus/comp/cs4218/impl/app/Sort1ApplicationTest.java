package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.OSCheck;
import sg.edu.nus.comp.cs4218.WindowsPermission;
import sg.edu.nus.comp.cs4218.exception.SortException;
import sg.edu.nus.comp.cs4218.impl.app.SortApplication;

public class Sort1ApplicationTest {

	private SortApplication sortApp;
	private String[] args;
	static String tempFilePath1 = "testSort.txt";
	private File tempFile1;
	private ByteArrayOutputStream outStream;

	public static final String APP_EXCEPTION = "sort: ";
	public static final String FAIL_MSG = "Should not throw exception";

	@Before
	public void setUp() throws Exception {
		sortApp = new SortApplication();
		args = null;

		try {
			tempFile1 = new File(tempFilePath1);
			tempFile1.createNewFile();
		} catch (SecurityException se) {
			fail("Cannot create temporary file to test");
		}

		outStream = new ByteArrayOutputStream();

		try {
			FileOutputStream fos = new FileOutputStream(new File(tempFilePath1));
			BufferedWriter buffWriter = new BufferedWriter(new OutputStreamWriter(fos));
			buffWriter.write("10");
			buffWriter.newLine();
			buffWriter.write("1");
			buffWriter.newLine();
			buffWriter.write("2");
			buffWriter.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		sortApp = null;
		args = null;
		tempFile1.delete();
		outStream = null;
	}

	@Test
	public void testNullInputStreamException() {
		try {
			sortApp.run(args, null, System.out);
			fail(FAIL_MSG);
		} catch (Exception e) {
			String exceptionMsg = APP_EXCEPTION + "Null Pointer Exception";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testNullOutputStreamException() {
		try {
			sortApp.run(args, System.in, null);
			fail(FAIL_MSG);
		} catch (Exception e) {
			String exceptionMsg = APP_EXCEPTION + "Null Pointer Exception";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testFileNotExistException() {

		try {
			sortApp.checkIfFileReadable(Paths.get("fileNotExist"));
			fail(FAIL_MSG);
		} catch (SortException e) {
			String exceptionMsg = APP_EXCEPTION + "No such file exists";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testFileIsDirException() {
		File fileDir = new File("tempSortDir");
		fileDir.mkdir();

		try {
			sortApp.checkIfFileReadable(Paths.get("tempSortDir"));
			fail(FAIL_MSG);
		} catch (SortException e) {
			String exceptionMsg = APP_EXCEPTION + "This is a directory";
			assertEquals(exceptionMsg, e.getMessage());
		}

		fileDir.delete();
	}

	@Test
	public void testFileIsVaild() {

		boolean flag = false;
		try {
			flag = sortApp.checkIfFileReadable(Paths.get(tempFilePath1));
			assertTrue(flag);
		} catch (SortException e) {
		}
	}

	@Test
	public void testFileNotReadable() throws SortException, IOException {
		// Make file not readable
		tempFile1.setReadable(false); // Unix
		if (OSCheck.isWindows()) {
			WindowsPermission.setReadable(tempFile1, false); // Windows
		}

		// Try to Sort file again
		args = new String[] { tempFilePath1 };
		try {
			sortApp.run(args, null, outStream);
		} catch (SortException e) {
			assertEquals(APP_EXCEPTION + "Could not read file", e.getLocalizedMessage());
		}
	}

	@Test
	public void testReadFromStdin() throws IOException, SortException {

		ByteArrayInputStream inputStream = new ByteArrayInputStream("yay\nyahoo".getBytes());

		try {
			sortApp.run(args, inputStream, outStream);
			String expected = "yahoo" + System.lineSeparator()+  "yay" + System.lineSeparator();
			assertEquals(expected, outStream.toString());
		} catch (SortException e) {
			fail(FAIL_MSG);
		}
	}

	@Test
	public void testReadFromFileAndWriteToStdOut() throws SortException, IOException {
		outStream.reset();
		outStream.flush();
		args = new String[] { tempFilePath1 };
		StringBuilder expected = new StringBuilder();
		expected.append("1").append(System.lineSeparator()).append("10").append(System.lineSeparator()).append("2").append(System.lineSeparator());

		try {
			sortApp.run(args, null, outStream);
			assertEquals(expected.toString(), outStream.toString());
		} catch (SortException e) {
			fail(FAIL_MSG);
		}

	}

	@Test
	public void testTreatFirstWordOfLineAsNumber() throws SortException, IOException {
		outStream.reset();
		outStream.flush();		
		args = new String[] { "-n", tempFilePath1 };
		StringBuilder expected = new StringBuilder();
		expected.append("1").append(System.lineSeparator()).append("2").append(System.lineSeparator()).append("10").append(System.lineSeparator());

		try {
			sortApp.run(args, null, outStream);
			assertEquals(expected.toString(), outStream.toString());
		} catch (SortException e) {
			fail(FAIL_MSG);
		}

	}

	@Test
	public void testSortStringsSimple() {
		String[] arr = new String[] { "cat", "apple", "banana" };
		List<String> expected = Arrays.asList(new String[] { "apple", "banana", "cat" });

		assertEquals(expected, sortApp.sortStringsSimple(arr));
	}

	@Test
	public void testSortStringsCapital() {
		String[] arr = new String[] { "CAT", "APPLE", "BANANA" };
		List<String> expected = Arrays.asList(new String[] { "APPLE", "BANANA", "CAT" });

		assertEquals(expected, sortApp.sortStringsCapital(arr));
	}

	@Test
	public void testSortNumbers() {
		String[] arr = new String[] { "1", "2", "10" };
		List<String> expected = Arrays.asList(new String[] { "1", "10", "2" });

		assertEquals(expected, sortApp.sortStringsNumbers(arr));
	}

	@Test
	public void testSortSpecialChars() {
		String[] arr = new String[] { "\n", "{|", "?)", "@", " " };
		List<String> expected = Arrays.asList(new String[] { "\n", " ", "?)", "@", "{|" });

		assertEquals(expected, sortApp.sortSpecialChars(arr));
	}

	@Test
	public void testSortSimpleCapital() {
		String[] arr = new String[] { "yaY", "yay", "yahoo", "Hello", "HELLO" };
		List<String> expected = Arrays.asList(new String[] { "HELLO", "Hello", "yaY", "yahoo", "yay" });

		assertEquals(expected, sortApp.sortSimpleCapital(arr));
	}

	@Test
	public void testSortSimpleNumbers() {
		String[] arr = new String[] { "1", "10", "2", "yay", "ya1", "1ya" };
		List<String> expected = Arrays.asList(new String[] { "1", "10", "1ya", "2", "ya1", "yay" });

		assertEquals(expected, sortApp.sortSimpleNumbers(arr));
	}

	@Test
	public void testSortSimpleSpecialChars() {
		String[] arr = new String[] { "\n", ".", "?|", "ya/y", "!ya", "yahoo'" };
		List<String> expected = Arrays.asList(new String[] { "\n", "!ya", ".", "?|", "ya/y", "yahoo'" });

		assertEquals(expected, sortApp.sortSimpleSpecialChars(arr));
	}

	@Test
	public void testSortCapitalNumbers() {
		String[] arr = new String[] { "10", "2H", "2", "HELLO", "ABC1", "A1BC" };
		List<String> expected = Arrays.asList(new String[] { "10", "2", "2H", "A1BC", "ABC1", "HELLO" });

		assertEquals(expected, sortApp.sortCapitalNumbers(arr));
	}

	@Test
	public void testsortCapitalSpecialChars() {
		String[] arr = new String[] { "HELLO", "\n", "HE.", "{JKL", "U\\I", "****" };
		List<String> expected = Arrays.asList(new String[] { "\n", "****", "HE.", "HELLO", "U\\I", "{JKL" });

		assertEquals(expected, sortApp.sortCapitalSpecialChars(arr));
	}

	@Test
	public void testsSortNumbersSpecialChars() {
		String[] arr = new String[] { "1;23", "(10", "1 =", "234", "|||", "^08" };
		List<String> expected = Arrays.asList(new String[] {"(10", "1 =", "1;23", "234", "^08", "|||" });

		assertEquals(expected, sortApp.sortNumbersSpecialChars(arr));
	}

	@Test
	public void testSortSimpleCapitalNumber() {
		String[] arr = new String[] { "cHUI8", "k34sjd", "1yahoo", "10sldUI", "2Ff", "000" };
		List<String> expected = Arrays.asList(new String[] { "000", "10sldUI", "1yahoo", "2Ff", "cHUI8", "k34sjd" });

		assertEquals(expected, sortApp.sortSimpleCapitalNumber(arr));
	}

	@Test
	public void testSortSimpleCapitalSpecialChars() {
		String[] arr = new String[] { "c++;", "(ksjd}", "yahoo =", "sldUI", "|Ff", "^fF" };
		List<String> expected = Arrays.asList(new String[] {  "(ksjd}","^fF", "c++;", "sldUI", "yahoo =", "|Ff", });

		assertEquals(expected, sortApp.sortSimpleSpecialChars(arr));
	}

	@Test
	public void testSortSimpleNumbersSpecialChars() {
		String[] arr = new String[] { "jdhf", "78{9", "&50%", "he'll do it", "< 90", "wasn't that true?" };
		List<String> expected = Arrays
				.asList(new String[] { "&50%", "78{9", "< 90", "he'll do it", "jdhf", "wasn't that true?" });

		assertEquals(expected, sortApp.sortSimpleNumbersSpecialChars(arr));
	}

	@Test
	public void testSortCapitalNumbersSpecialChars() {
		String[] arr = new String[] { "KJS", "78{9UIUI", "&5THE0%", "JUST DO IT", "< 90", "HELP!" };
		List<String> expected = Arrays
				.asList(new String[] { "&5THE0%", "78{9UIUI", "< 90", "HELP!", "JUST DO IT", "KJS" });

		assertEquals(expected, sortApp.sortCapitalNumbersSpecialChars(arr));
	}
	
	@Test
	public void testSortAll() {
		String[] arr = new String[] { "KJSdhf", "78{9UIUI", "&5the0%", "JUST do it", "< 90", "wasn't that MEAN?" };
		List<String> expected = Arrays
				.asList(new String[] { "&5the0%","78{9UIUI", "< 90","JUST do it", "KJSdhf", "wasn't that MEAN?" });

		assertEquals(expected, sortApp.sortAll(arr));
	}
}
