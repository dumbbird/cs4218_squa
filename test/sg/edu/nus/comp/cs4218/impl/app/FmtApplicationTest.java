package sg.edu.nus.comp.cs4218.impl.app;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.lang.String;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.OSCheck;
import sg.edu.nus.comp.cs4218.WindowsPermission;
import sg.edu.nus.comp.cs4218.exception.FmtException;
import sg.edu.nus.comp.cs4218.impl.app.FmtApplication;

public class FmtApplicationTest {
	private FmtApplication fmtApp;
	private String[] args;
	private File file;
	private ByteArrayOutputStream outStream;

	public static String tempFilePath = "testFmt.txt";
	public static final String APP_EXCEPTION = "fmt: ";
	public static final String SHOULDNOT_FAIL = "Should not throw exception";

	@Before
	public void setUp() throws Exception {
		fmtApp = new FmtApplication();
		args = null;
		outStream = new ByteArrayOutputStream();
		try {
			file = new File(tempFilePath);
			file.createNewFile();
		} catch (SecurityException se) {
			fail("Cannot create temporary file to test");
		}

		try {
			FileOutputStream fos = new FileOutputStream(new File(tempFilePath));

			BufferedWriter buffWriter = new BufferedWriter(new OutputStreamWriter(fos));

			buffWriter.write("CS4218 ");
			buffWriter.write(System.lineSeparator());
			buffWriter.write("Shell ");
			buffWriter.write(System.lineSeparator());
			buffWriter.write("is a");
			buffWriter.write(System.lineSeparator());
			buffWriter.write(System.lineSeparator());
			buffWriter.write("啦啦啦啦");
			buffWriter.write(System.lineSeparator());
			buffWriter.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		fmtApp = null;
		args = null;
		file.delete();
	}

	@Test
	public void testReadFromStdinAndWriteToStdouNullInputStreamException() {

		try {
			fmtApp.readFromStdinAndWriteToStdout(System.out, 3, null);
			fail("Should have thrown Null Pointer Exception but did not!");
		} catch (Exception e) {
			String exceptionMsg = APP_EXCEPTION + "Null Pointer Exception";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testReadFromStdinWithNullArgs() throws FmtException, IOException {

		StringBuilder input = new StringBuilder();
		input.append("CS4218 Shell is a command interpreter that provides a set of tools (applications): "
				+ "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
				+ "Apart from that, CS4218 Shell is a language for calling and combining these application. "
				+ "The language supports quoting of input data, semicolon operator for calling sequences of "
				+ "applications, command substitution and piping for connecting applications' inputs and "
				+ "outputs, IO-redirection to load and save data processed by applications from/to files. "
				+ "More details can be found in \"Project Description.pdf\" in IVLE.");

		ByteArrayInputStream inStream = new ByteArrayInputStream(input.toString().getBytes("UTF-8"));

		try {
			fmtApp.run(args, inStream, outStream);
			String inputStr = "";
			String[] inputStrArr = fmtApp.splitString(input.toString(), 80);
			for (int i = 0; i < inputStrArr.length; i++)
				inputStr += inputStrArr[i].trim() + System.lineSeparator();
			assertEquals(inputStr + System.lineSeparator(), outStream.toString());
		} catch (FmtException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testReadFromStdinWithValidNumber() throws FmtException, IOException {

		StringBuilder input = new StringBuilder();
		// input.append(1).append(System.lineSeparator());
		input.append("CS4218 Shell is a command interpreter that provides a set of tools (applications): "
				+ "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
				+ "Apart from that, CS4218 Shell is a language for calling and combining these application. "
				+ "The language supports quoting of input data, semicolon operator for calling sequences of "
				+ "applications, command substitution and piping for connecting applications' inputs and "
				+ "outputs, IO-redirection to load and save data processed by applications from/to files. "
				+ "More details can be found in \"Project Description.pdf\" in IVLE.");

		ByteArrayInputStream inStream = new ByteArrayInputStream(input.toString().getBytes("UTF-8"));

		try {
			fmtApp.readFromStdinAndWriteToStdout(outStream, 60, inStream);
			String inputStr = "";
			String[] inputStrArr = fmtApp.splitString(input.toString(), 60);
			for (int i = 0; i < inputStrArr.length; i++) {
				// System.out.println(inputStrArr[i]);
				inputStr += inputStrArr[i].trim() + System.lineSeparator();
			}

			// System.out.println(inputStr);
			// System.out.println(outStream.toString());
			assertEquals(inputStr + System.lineSeparator(), outStream.toString());
		} catch (FmtException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testReadFromStdinWithFlag() throws FmtException, IOException {

		StringBuilder expected = new StringBuilder();
		expected.append("CS4218 Shell 123").append(System.lineSeparator()).append("Apart from that,")
				.append(System.lineSeparator()).append(System.lineSeparator());
		String inputString = "CS4218 Shell 123 Apart from that, \n";

		ByteArrayInputStream inStream = new ByteArrayInputStream(inputString.getBytes("UTF-8"));

		args = new String[2];
		args[0] = "-w";
		args[1] = "17";

		try {
			fmtApp.run(args, inStream, outStream);
			assertEquals(expected.toString(), outStream.toString());
		} catch (FmtException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testInsufficentArgumentsException() {

		args = new String[] { "-n" };

		try {
			fmtApp.run(args, null, outStream);
			fail("Should have thrown exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "No such file exists";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testInvalidArgumentsForStdinException() {

		args = new String[] { "-", "15" };

		try {
			fmtApp.run(args, System.in, outStream);
			fail("Should have thrown exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid Fmt Command for reading from stdin";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testInvalidFmtCommandException() {

		args = new String[] { "-n", "15", tempFilePath, "lalala" };

		try {
			fmtApp.run(args, null, outStream);
			fail("Should have thrown SomeException but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid Fmt Command";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testIncorrectFlagUsedException() {

		args = new String[] { "-f", "15", tempFilePath };

		try {
			fmtApp.run(args, null, outStream);
			fail("Should have thrown SomeException but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "Incorrect flag used";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testFileNotExistException() {

		boolean flag = false;

		try {
			flag = fmtApp.checkIfFileIsReadable(Paths.get("fileNotExist"));
			assertFalse(flag);
			fail("Should have thrown no such file exist exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "No such file exists";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testFileIsDirException() {

		boolean flag = false;

		File fileDir = new File("tempHeadDir");
		fileDir.mkdir();

		try {
			flag = fmtApp.checkIfFileIsReadable(Paths.get("tempHeadDir"));
			assertFalse(flag);
			fail("Should have thrown file is directory exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "This is a directory";
			assertEquals(exceptionMsg, e.getMessage());
		}

		fileDir.delete();
	}

	@Test
	public void testFileIsValid() throws FmtException {

		boolean flag = false;
		try {
			flag = fmtApp.checkIfFileIsReadable(Paths.get(tempFilePath));
			assertTrue(flag);
		} catch (FmtException e) {
			fail("Path should be valid");
		}
	}

	@Test
	public void testNotANumberException() {

		try {
			fmtApp.checkNumberOfMaxWidth("ooo");
			fail("Should have thrown Not a number exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "Invalid command, not a number.";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testNegativeNumberException() {

		try {
			fmtApp.checkNumberOfMaxWidth("-5");
			fail("Should have thrown non-positive number exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "Number of lines cannot be non-positive";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testZeroNumberException() {
		args = new String[] { "-w", "0", tempFilePath };
		try {
			fmtApp.run(args, null, outStream);
			fail("Should have thrown non-positive number exception but did not!");
		} catch (FmtException e) {
			String exceptionMsg = APP_EXCEPTION + "Number of lines cannot be non-positive";
			assertEquals(exceptionMsg, e.getMessage());
		}
	}

	@Test
	public void testNumberIsValid() {

		try {
			int numWidth = fmtApp.checkNumberOfMaxWidth("10");
			assertEquals(10, numWidth);
		} catch (FmtException e) {
			fail("Number should be valid");
		}
	}

	@Test
	public void testReadEmptyFile() throws FmtException, IOException {

		File emptyFile = new File("HeadEmptyFileTest.txt");
		emptyFile.createNewFile();

		args = new String[] { "-w", "10", "HeadEmptyFileTest.txt" };

		try {
			fmtApp.readFromFileAndWriteToStdout(outStream, 10, emptyFile.toPath());
			assertEquals(System.lineSeparator(), outStream.toString());
		} catch (FmtException e) {
			fail(SHOULDNOT_FAIL);
		}
		emptyFile.delete();
	}

	@Test
	public void testReadFileAndWriteToStdoutException() throws FmtException, IOException {
		try {
			fmtApp.readFromFileAndWriteToStdout(null, 10, Paths.get(tempFilePath));
			;
		} catch (FmtException e) {
			String expected = APP_EXCEPTION + "Stdout is null";
			assertEquals(expected, e.getMessage());
		}

	}

	@Test
	public void testFileNotReadable() throws FmtException, IOException {

		StringBuilder expected = new StringBuilder();
		expected.append("CS4218");
		expected.append(System.lineSeparator());
		expected.append("Shell");
		expected.append(System.lineSeparator());
		expected.append("is a");
		expected.append(System.lineSeparator());
		expected.append(System.lineSeparator());
		expected.append("啦啦啦啦");
		expected.append(System.lineSeparator());
		expected.append(System.lineSeparator());

		// Verify that file is written correctly and CAT works
		args = new String[] { "-w", "20", tempFilePath };
		fmtApp.run(args, null, outStream);

		assertEquals(expected.toString(), outStream.toString());

		// Make file not readable
		file.setReadable(false); // Unix
		if (OSCheck.isWindows()) {
			WindowsPermission.setReadable(file, false); // Windows
		}

		// Try to head file again
		args = new String[] { "-w", "20", tempFilePath };
		try {
			fmtApp.run(args, null, outStream);
		} catch (FmtException e) {
			assertEquals(APP_EXCEPTION + "Could not read file", e.getLocalizedMessage());
		}
	}

	@Test
	public void testReadFileLessThanMaxWidthPerLine() throws FmtException, IOException {
		args = new String[] { "-w", "20", tempFilePath };
		StringBuilder expected = new StringBuilder();
		expected.append("CS4218").append(System.lineSeparator()).append("Shell").append(System.lineSeparator())
				.append("is a").append(System.lineSeparator()).append(System.lineSeparator()).append("啦啦啦啦")
				.append(System.lineSeparator()).append(System.lineSeparator());
		try {
			fmtApp.run(args, null, outStream);
			// System.out.println(outStream.toString());
			assertEquals(expected.toString(), outStream.toString());
		} catch (FmtException e) {
			fail(SHOULDNOT_FAIL);
		}
	}

	@Test
	public void testReadFileMoreThanMaxWidthPerLine() throws FmtException, IOException {
		args = new String[] { "-w", "2", tempFilePath };
		StringBuilder expected = new StringBuilder();
		expected.append("CS4218").append(System.lineSeparator())
				.append("Shell").append(System.lineSeparator())
				.append("is").append(System.lineSeparator())
				.append("a").append(System.lineSeparator())
				.append(System.lineSeparator())
				.append("啦啦啦啦").append(System.lineSeparator())
				.append(System.lineSeparator());

		try {
			fmtApp.run(args, null, outStream);
			// System.out.println(outStream.toString());
			assertEquals(expected.toString(), outStream.toString());
		} catch (FmtException e) {
			fail(SHOULDNOT_FAIL);
		}

	}
}
