package sg.edu.nus.comp.cs4218.impl.integrationTesting;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.*;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;

public class ChainOfInteractionsCommandSubstitution {
	private static ByteArrayOutputStream baos;
	private static ShellImpl shell;
	private static Scanner scanner;
	
	final static String TEST_STR = "CS4218 Shell is a command interpreter that provides a set of tools (applications): "
				+ System.lineSeparator()
				+ "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
				+ System.lineSeparator()
				+ "Apart from that, CS4218 Shell is a language for calling and combining these application. "
				+ System.lineSeparator()
				+ "The language supports quoting of input data, semicolon operator for calling sequences of "
				+ System.lineSeparator()
				+ "applications, command substitution and piping for connecting applications' inputs and "
				+ System.lineSeparator()
				+ "outputs, IO-redirection to load and save data processed by applications from/to files. "
				+ System.lineSeparator()
				+ "More details can be found in \"Project Description.pdf\" in IVLE.";
	final static String TEST_STR1 = "Testing Stream";
	final static String TEST_FILE_NAME = "testShell.txt";
	final static String TEST_FILE_NAME2 = "testShell2.txt";
	final static String TEST_FOLDER_NAME = "testShellFolder";

	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception for valid command.";
	final static String VALID_FILE_NO_EXP = "Not supposed to have exception for valid file.";
	final static String VALID_STRM_NO_EXP = "Not supposed to have exception for valid streams.";
	final static String READONLY_EXP = "Supposed to have exception opening outputstream to read-only file.";
	final static String VALID_EXP = "Valid Exception thrown";
	final static String MISSING_EXP = "Should have exception thrown";
	private static final String FIRST_LINE = "CS4218 Shell is a command interpreter that provides a set of tools (applications):  ";
	private static final String SECOND_LINE = "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc.";
	private static final String THIRD_LINE = "Apart from that, CS4218 Shell is a language for calling and combining these application. ";
	private static final String FOURTH_LINE = "The language supports quoting of input data, semicolon operator for calling sequences of ";
	private static final String FIFTH_LINE = "applications, command substitution and piping for connecting applications' inputs and ";
	private static final String SIXTH_LINE = "outputs, IO-redirection to load and save data processed by applications from/to files. ";
	private static final String LAST_LINE = "More details can be found in \"Project Description.pdf\" in IVLE.";
	static String originalFilePath;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		shell = new ShellImpl();
		baos = new ByteArrayOutputStream();
		createTestFile(TEST_FILE_NAME);
		createTestFile1("text1.txt");
		createTestFile1(TEST_FILE_NAME2);
		createTestFolder(TEST_FOLDER_NAME);
		originalFilePath = Environment.currentDirectory;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		removeTestFile(TEST_FILE_NAME);
		removeTestFile(TEST_FILE_NAME2);
		removeTestFile("text1.txt");
		removeTestFile("test.txt");
		removeTestFolder(TEST_FOLDER_NAME);
	}

	public static void createTestFile(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR.getBytes());
	}
	public static void createTestFile1(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR1.getBytes());
	}

	public static void removeTestFile(String fileName) throws IOException {
		File file = new File(fileName);
		file.setWritable(true);
		file.delete();
	}

	public static void createTestFolder(String folderName) throws IOException {
		new File(folderName).mkdir();
	}

	public static void removeTestFolder(String folderName) throws IOException {
		File file = new File(folderName);

		String[] entries = file.list();
		if (entries != null) {
			for (String s : entries) {
				File currentFile = new File(file.getPath(), s);
				currentFile.delete();
			}
		}
		file.delete();
		Environment.currentDirectory = originalFilePath;
	}

	public void writeToStream(OutputStream myoutputStream) throws IOException {
		myoutputStream.write(TEST_STR.getBytes());
		myoutputStream.flush();
		myoutputStream.close();
	}

	public String fileToString(String fileName) throws FileNotFoundException {
		scanner = new Scanner(new File(fileName));
		String fileStr = scanner.useDelimiter("\\Z").next();
		scanner.close();

		return fileStr;
	}
	
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////Negative Scenarios////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	public void testCommandSubWithSortInvalidFileAndCat() throws IOException, AbstractApplicationException, ShellException {
		
		try {
			baos.reset();
			String input = "cat `sort nosuchFile.txt`";
			shell.parseAndEvaluate(input, baos); 
		
		} catch (SortException e) {
			String expected = "sort: No such file exists";
			assertEquals(expected, e.getMessage());
		} catch (ShellException e) {
			
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	@Test
	public void testCommandSubWithInvalidCommand() throws IOException, AbstractApplicationException, ShellException {
		
		try {
			baos.reset();
			String input = "echo `abc`";
			shell.parseAndEvaluate(input, baos); 
		
		} catch (SortException e) {
			
		} catch (ShellException e) {
			String expected = "shell: abc: Invalid app.";
			assertEquals(expected, e.getMessage());
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
///////Chain of interactions integrating applications using command substitution/////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
	
	//echo, bc, command sub
	@Test
	public void testCommandSubWithEchoAndBc() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "echo `bc 7*7+2`";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = "51";
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n| ", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	//echo, sort, command sub
	@Test
	public void testCommandSubWithEchoAndSort() throws IOException, ShellException, AbstractApplicationException {
		try {
			baos.reset();
			String input = "echo `sort "+ TEST_FILE_NAME +" ` ";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = THIRD_LINE + FIRST_LINE + LAST_LINE + FOURTH_LINE + FIFTH_LINE + SECOND_LINE + SIXTH_LINE;;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replaceAll("\r|\n| ", "");
			
			assertEquals(expectedResult.replaceAll(" ", ""), result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	
	//echo, cat, command sub
	@Test
	public void testCommandSubWithEchoAndCat() throws IOException, AbstractApplicationException, ShellException {
		
		try {
			baos.reset();
			String input = "echo `cat " + TEST_FILE_NAME + "` ";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE + SECOND_LINE + THIRD_LINE + FOURTH_LINE + FIFTH_LINE + SIXTH_LINE + LAST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replaceAll("\r|\n| ", "");
			
			assertEquals(expectedResult.replaceAll(" ", ""), result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	
	//sort, cat, head, command sub
	@Test
	public void testCommandSubWithSortCatAndHead() throws IOException, ShellException, AbstractApplicationException {
		try {
			baos.reset();
			String input = "sort `cat "+ TEST_FILE_NAME +" | head -n 2 `";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE + SECOND_LINE;
			byte[] byteArray = baos.toByteArray();
			
			String result = new String(byteArray).replaceAll("\r|\n", "");
			assertEquals(expectedResult, result.trim());
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// tail, echo, pipe, command sub
		@Test
		public void testCommandSubWithTailAndEcho() throws IOException, AbstractApplicationException, ShellException {

			try {
				baos.reset();
				String input = "echo `tail -n 1 "+ TEST_FILE_NAME +" | tail -n 1 |tail -n 1` ";
				shell.parseAndEvaluate(input, baos);
				String expectedResult = LAST_LINE;
				byte[] byteArray = baos.toByteArray();
				String result = new String(byteArray).replace("\n", "").replace("\r", "");
				
				result = result.replaceAll("\r|\n", "");
				assertEquals(expectedResult, result);
			} catch (ShellException e) {
				fail(VALID_CMD_NO_EXP);
			} catch (AbstractApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(VALID_CMD_NO_EXP);
			}
		}
		
		// cat, echo, pipe, command sub
		@Test
		public void testCommandSubWithCalAndCatAndEcho()
				throws IOException, AbstractApplicationException, ShellException {

			try {
				baos.reset();
				String input = "echo `cal -m may 2016 | cat "+TEST_FILE_NAME+" | cat`";
				shell.parseAndEvaluate(input, baos);
				String expectedResult = TEST_STR;
				byte[] byteArray = baos.toByteArray();
				String result = new String(byteArray).replace("\n", "").replace("\r", "");
				expectedResult = expectedResult.replaceAll("\r|\n| ", "");
				result = result.replaceAll("\r|\n| ", "");
				assertEquals(expectedResult, result);
			} catch (ShellException e) {
				fail(VALID_CMD_NO_EXP);
			} catch (AbstractApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(VALID_CMD_NO_EXP);
			}
		}
		
		// cal, echo, cat, pipe, command sub
		@Test
		public void testCommandSubWithCalAndEchoAndCat() throws IOException, AbstractApplicationException, ShellException {

			try {
				// String command = "cat `cat resource-hack/examples-1/comsub1.txt| head -n 1`
				//; cat `cat resource-hack/examples-1/comsub2.txt| head -n 1`"
				baos.reset();
				String input = "echo `cal -m may 2016 | cat | cat`" ;
				shell.parseAndEvaluate(input, baos);
				String expectedResult = "May 2016Mo Tu We Th Fr Sa Su 1 2 3 4 5 6 7 8 9 10 "
									+ "11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31";
				byte[] byteArray = baos.toByteArray();
				String result = new String(byteArray).replace("\n", "").replace("\r", "");
				expectedResult = expectedResult.replaceAll("\r|\n| ", "");
				result = result.replaceAll("\r|\n| ", "");
				assertEquals(expectedResult, result);
			} catch (ShellException e) {
				fail(VALID_CMD_NO_EXP);
			} catch (AbstractApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(VALID_CMD_NO_EXP);
			}
		}
		
		// bc, cat, echo, pipe, command sub
		@Test
		public void testCommandSubWithBcAndCatAndEcho() throws IOException, AbstractApplicationException, ShellException {
			
			try {
				baos.reset();
				String input = "echo `bc 1.5 | cat| cat`"  ;
				shell.parseAndEvaluate(input, baos);
				String expectedResult = "1.5";
				byte[] byteArray = baos.toByteArray();
				String result = new String(byteArray).replace("\n", "").replace("\r", "");
				result = result.replaceAll("\r|\n| ", "");
				assertEquals(expectedResult, result);
			} catch (ShellException e) {
				fail(VALID_CMD_NO_EXP);
			} catch (AbstractApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(VALID_CMD_NO_EXP);
			}
		}
		
		// echo, date, cat, pipe, command sub
		@Test
		public void testCommandSubWithDateAndEchoAndCat()
				throws IOException, AbstractApplicationException, ShellException {

			try {
				baos.reset();
				String input = "echo ` date | cat | cat`";
				shell.parseAndEvaluate(input, baos);
				DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
				Calendar cal = Calendar.getInstance();
				String expectedResult = (dateFormat.format(cal.getTime()).toString()) + " " + System.lineSeparator();
				
				//byte[] byteArray = baos.toByteArray();
				//String result = new String(byteArray).replace("\n", "").replace("\r", "");
				String result = baos.toString();
				//System.out.println(result);
				result = result.replaceAll("\r|\n| ", "");
				assertEquals(expectedResult.replaceAll("\r|\n| ", ""), result);
			} catch (ShellException e) {
				fail(VALID_CMD_NO_EXP);
			} catch (AbstractApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(VALID_CMD_NO_EXP);
			}
		}
		
		// echo, date, cat, head, tail, pipe, command sub
		@Test
		public void testCommandSubWithDateAndEchoAndHeadAndTail()
				throws IOException, AbstractApplicationException, ShellException {

			try {
				baos.reset();
				String input = "echo ` date | cat | head -n 1 | tail -n 1`";
				shell.parseAndEvaluate(input, baos);
				DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
				Calendar cal = Calendar.getInstance();
				String expectedResult = (dateFormat.format(cal.getTime()).toString());
				
				byte[] byteArray = baos.toByteArray();
				String result = new String(byteArray).replace("\n", "").replace("\r", "");

				result = result.replaceAll("\r|\n", "");
				assertEquals(expectedResult, result);
			} catch (ShellException e) {
				fail(VALID_CMD_NO_EXP);
			} catch (AbstractApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail(VALID_CMD_NO_EXP);
			}
		}

}
