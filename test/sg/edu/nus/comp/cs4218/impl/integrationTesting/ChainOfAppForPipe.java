package sg.edu.nus.comp.cs4218.impl.integrationTesting;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.*;
import sg.edu.nus.comp.cs4218.impl.ShellImpl;
import sg.edu.nus.comp.cs4218.impl.app.CommApplication;
import sg.edu.nus.comp.cs4218.impl.app.FmtApplication;

public class ChainOfAppForPipe {

	private static ByteArrayOutputStream baos;
	private static ShellImpl shell;
	private static Scanner scanner;
	private static final String FIRST_LINE = "CS4218 Shell is a command interpreter that provides a set of tools (applications): ";
	private static final String LAST_LINE ="More details can be found in \"Project Description.pdf\" in IVLE.";
	private static final String SORTED_LAST_LINE = "outputs, IO-redirection to load and save data processed by applications from/to files. ";
	
	private static final String SORTED_FIRST_LINE =  "Apart from that, CS4218 Shell is a language for calling and combining these application. ";
	
	final static String TEST_FILE_NAME = "testShell.txt";
	final static String TEST_STR = "CS4218 Shell is a command interpreter that provides a set of tools (applications): "
				+ System.lineSeparator()
				+ "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
				+ System.lineSeparator()
				+ "Apart from that, CS4218 Shell is a language for calling and combining these application. "
				+ System.lineSeparator()
				+ "The language supports quoting of input data, semicolon operator for calling sequences of "
				+ System.lineSeparator()
				+ "applications, command substitution and piping for connecting applications' inputs and "
				+ System.lineSeparator()
				+ "outputs, IO-redirection to load and save data processed by applications from/to files. "
				+ System.lineSeparator()
				+ "More details can be found in \"Project Description.pdf\" in IVLE.";
	
	final static String TEST_FILE_NAME2 = "testShell2.txt";
	final static String TEST_STR1 = "Testing Stream";
	
    private static final String COMM_FILE1_CONTENT = "line1\nline2\nline3\nline8";
    private static final String COMM_FILE2_CONTENT = "line1\nline2\nline3\nline8";
    private static final String COMM_FILE1_FILE2 = CommApplication.DOUBLE_TAB + "line1"
            + System.lineSeparator() + CommApplication.DOUBLE_TAB + "line2" + System.lineSeparator()
            + CommApplication.DOUBLE_TAB + "line3" + System.lineSeparator()
            + CommApplication.DOUBLE_TAB + "line8" + System.lineSeparator() + System.lineSeparator();
	
	final static String TEST_FOLDER_NAME = "testShellFolder";

	final static String VALID_CMD_NO_EXP = "Not supposed to throw exception for valid command.";
	final static String VALID_FILE_NO_EXP = "Not supposed to have exception for valid file.";
	final static String VALID_STRM_NO_EXP = "Not supposed to have exception for valid streams.";
	final static String READONLY_EXP = "Supposed to have exception opening outputstream to read-only file.";
	final static String VALID_EXP = "Valid Exception thrown";
	final static String MISSING_EXP = "Should have exception thrown";
	static String originalFilePath;

	// find out sorting does not work
	// bc is not included in the shellImpl runApp function
	// single quoting has problem
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		shell = new ShellImpl();
		baos = new ByteArrayOutputStream();
		createTestFile(TEST_FILE_NAME);
		createTestFile1("text1.txt");
		createTestFile1(TEST_FILE_NAME2);
		createTestFile1("test.txt");
		createTestFile2("comm_file1.txt", "comm_file2.txt");
		createTestFolder(TEST_FOLDER_NAME);
		originalFilePath = Environment.currentDirectory;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		removeTestFile(TEST_FILE_NAME);
		removeTestFile(TEST_FILE_NAME2);
		removeTestFile("text1.txt");
		removeTestFile("test.txt");
		removeTestFile("comm_file1.txt");
		removeTestFile("comm_file2.txt");
		removeTestFolder(TEST_FOLDER_NAME);
	}

	public static void createTestFile(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR.getBytes());
	}
	public static void createTestFile1(String fileName) throws IOException {
		Files.write(Paths.get(fileName), TEST_STR1.getBytes());
	}
	public static void createTestFile2(String fileName1, String fileName2) throws IOException {
		Files.write(Paths.get(fileName1), COMM_FILE1_CONTENT.getBytes());
		Files.write(Paths.get(fileName2), COMM_FILE2_CONTENT.getBytes());
	}
	public static void removeTestFile(String fileName) throws IOException {
		File file = new File(fileName);
		file.setWritable(true);
		file.delete();
	}

	public static void createTestFolder(String folderName) throws IOException {
		new File(folderName).mkdir();
	}

	public static void removeTestFolder(String folderName) throws IOException {
		File file = new File(folderName);

		String[] entries = file.list();
		if (entries != null) {
			for (String s : entries) {
				File currentFile = new File(file.getPath(), s);
				currentFile.delete();
			}
		}
		file.delete();
		Environment.currentDirectory = originalFilePath;
	}


	public void writeToStream(OutputStream myoutputStream) throws IOException {
		myoutputStream.write(TEST_STR.getBytes());
		myoutputStream.flush();
		myoutputStream.close();
	}

	public String fileToString(String fileName) throws FileNotFoundException {
		scanner = new Scanner(new File(fileName));
		String fileStr = scanner.useDelimiter("\\Z").next();
		scanner.close();

		return fileStr;
	}

	
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////Negative Scenarios////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	// fmt, date, cat, pipe
	@Test(expected = FmtException.class)
	public void testIntegrateExceptionWithInvalidAppAndPipe()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "fmt " + TEST_FILE_NAME + "date token1 | cat";
		shell.parseAndEvaluate(input, baos);
	}

	// head, bc, cat, comm
	@Test(expected = HeadException.class)
	public void testIntegrateExceptionWithInvalidAppAndPipe2()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "head " + TEST_FILE_NAME + "bc \"1+2\" | comm";
		shell.parseAndEvaluate(input, baos);
	}

	// tail, fmt
	@Test(expected = TailException.class)
	public void testIntegrateExceptionWithNonExistFile()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "tail nonexist.txt | fmt";
		shell.parseAndEvaluate(input, baos);
	}
	
	// cat, nonexist
	@Test(expected = ShellException.class)
	public void testIntegrateExceptionWithNonExistApp()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "cat " + TEST_FILE_NAME + " | nonexist ";
		shell.parseAndEvaluate(input, baos);
	}
	
	// cat, pipe
	@Test(expected = ShellException.class)
	public void testIntegrateExceptionWithInvalidTwoPipe()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "cat " + TEST_FILE_NAME + "cat token1 | cat | ";
		shell.parseAndEvaluate(input, baos);
	}

	// cat, tail, pipe
	@Test(expected = CatException.class)
	public void testIntegrateExceptionWithInvalidTwoPipeOfCAndT() throws ShellException, AbstractApplicationException {
		String input = "cat token1 | tail -n 1 | cat";
		shell.parseAndEvaluate(input, baos);
	}

	// head, cat, pipe
	@Test(expected = sg.edu.nus.comp.cs4218.exception.HeadException.class)
	public void testIntegrateExceptionWithInvalidCmdOfHAndC() throws ShellException, AbstractApplicationException {
		String input = "head -n 10 token1 | cat | cat";
		shell.parseAndEvaluate(input, baos);
	}

	// tail, sort, cat, pipe
	@Test(expected = sg.edu.nus.comp.cs4218.exception.TailException.class)
	public void testIntegrationExceptionWithInvalidAppCmdOfTAndS() throws ShellException, AbstractApplicationException {
		String input = "tail -n 10 token1 | sort | cat";
		shell.parseAndEvaluate(input, baos);
	}
	

	// cat, head, pipe
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionWithInvalidDividerOfThreePipe()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "cat " + TEST_FILE_NAME + " | head -n 1 |";
		shell.parseAndEvaluate(input, baos);
	}

	// pipe
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionOfInvalidApp() throws ShellException, AbstractApplicationException {
		String input = "INVALIDAPP token1 | INVALIDAPP";
		shell.parseAndEvaluate(input, baos);
	}

	// pipe
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionOfInvalidApp2() throws ShellException, AbstractApplicationException {
		String input = "INVALIDAPP token1 | INVALIDAPP | INVALIDAPP";
		shell.parseAndEvaluate(input, baos);
	}

	// echo, cat, pipe, semicolon
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionOfInvalidSemicolon() throws ShellException, AbstractApplicationException {
		String input = "echo \" token1; | cat | cat";
		shell.parseAndEvaluate(input, baos);

	}

	// echo, cat, pipe, doublequote
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionOfInvalidDoubleQuote() throws ShellException, AbstractApplicationException {
		String input = "echo \" token1 | cat | cat";
		shell.parseAndEvaluate(input, baos);

	}

	// echo, cat, pipe, singlequote
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionOfInvalidSingleQuote()
			throws ShellException, AbstractApplicationException, IOException {

		String input = "echo ' cat " + TEST_FILE_NAME + " | cat | cat";
		shell.parseAndEvaluate(input, baos);
	}

	// echo, cat, pipe, backquote
	@Test(expected = ShellException.class)
	public void testIntegrationExceptionOfInvalidBackQuote()
			throws ShellException, AbstractApplicationException, IOException {
		String input = "echo ` cat test.txt | cat | cat";
		shell.parseAndEvaluate(input, baos);
	}

		
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	////////////Chain of interactions integrate applications using piping////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	
	// cat, sort, pipe
	@Test
	public void testPipeWithCatSort() throws IOException {
		try {
			// baos.flush();
			baos.reset();
			String input = "cat " + TEST_FILE_NAME + " | sort";
			shell.parseAndEvaluate(input, baos);
			String expected = "Apart from that, CS4218 Shell is a language for calling and combining these application. "
					+ System.lineSeparator()
					+ "CS4218 Shell is a command interpreter that provides a set of tools (applications): "
					+ System.lineSeparator() + "More details can be found in \"Project Description.pdf\" in IVLE."
					+ System.lineSeparator()
					+ "The language supports quoting of input data, semicolon operator for calling sequences of "
					+ System.lineSeparator()
					+ "applications, command substitution and piping for connecting applications' inputs and "
					+ System.lineSeparator() + "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
					+ System.lineSeparator()
					+ "outputs, IO-redirection to load and save data processed by applications from/to files. "
					+ System.lineSeparator();

			String output = new String(baos.toByteArray());
			//System.out.println("output: " + output);
			//System.out.println("expected: " + expected);
			assertEquals(expected, output);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, fmt, pipe
	@Test
	public void testPipeWithCatFmt() throws IOException {
		try {
			// baos.flush();
			baos.reset();
			String input = "cat " + TEST_FILE_NAME + " | fmt";
			shell.parseAndEvaluate(input, baos);
			String expected = "CS4218 Shell is a command interpreter that provides a set of tools"
					+ System.lineSeparator() + "(applications):" + System.lineSeparator()
					+ "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc." + System.lineSeparator()
					+ "Apart from that, CS4218 Shell is a language for calling and combining these"
					+ System.lineSeparator() + "application." + System.lineSeparator()
					+ "The language supports quoting of input data, semicolon operator for calling"
					+ System.lineSeparator() + "sequences of" + System.lineSeparator()
					+ "applications, command substitution and piping for connecting applications'"
					+ System.lineSeparator() + "inputs and" + System.lineSeparator()
					+ "outputs, IO-redirection to load and save data processed by applications from/to"
					+ System.lineSeparator() + "files." + System.lineSeparator()
					+ "More details can be found in \"Project Description.pdf\" in IVLE." + System.lineSeparator()
					+ System.lineSeparator();
			// System.out.println(baos);
			String output = new String(baos.toByteArray());
			//System.out.println(expected);
			//System.out.println(output);
			output = output.replaceAll("\\s+", "");
			expected = expected.replaceAll("\\s+", "");
			assertEquals(expected, output);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, head, pipe
	@Test
	public void testPipeWithCatHead() throws IOException {

		try {
			// baos.flush();
			baos.reset();
			String input = "cat " + TEST_FILE_NAME + " | head -n 2";
			shell.parseAndEvaluate(input, baos);
			String expected = "CS4218 Shell is a command interpreter that provides a set of tools " + "(applications): "
					+ System.lineSeparator() + "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
					+ System.lineSeparator();
			// System.out.println(baos);
			String output = new String(baos.toByteArray());
			assertEquals(expected, output);

		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, tail, pipe
	@Test
	public void testPipeWithCatTail() throws IOException {
		try {
			baos.reset();
			String input = "cat " + TEST_FILE_NAME + " | tail -n 2";
			shell.parseAndEvaluate(input, baos);
			String expected = "outputs, IO-redirection to load and save data processed by applications from/to files. "
					+ System.lineSeparator() + "More details can be found in \"Project Description.pdf\" in IVLE.";

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// cat, comm, pipe
	@Test
	public void testPipeWithCatComm() throws IOException {
		try {
			baos.reset();
			String input = "cat comm_file1.txt" + " | comm comm_file2.txt";
			shell.parseAndEvaluate(input, baos);
			String expected = COMM_FILE1_FILE2 ;

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	// cat, pipe
	@Test
	public void testPipeWithCatCat() throws IOException {
		try {
			baos.reset();
			String input = "cat " + TEST_FILE_NAME + " | cat";
			shell.parseAndEvaluate(input, baos);
			String expected = TEST_STR + System.lineSeparator();

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// echo, cat, pipe
	@Test
	public void testPipeWithEchoCat() throws IOException {
		try {
			baos.reset();
			String input = "echo " + TEST_FILE_NAME2 + " | cat";
			shell.parseAndEvaluate(input, baos);
			String expected = TEST_FILE_NAME2 + System.lineSeparator();

			String output = new String(baos.toByteArray());
			output = output.replaceAll("\\s+", "");
			expected = expected.replaceAll("\\s+", "");
			assertEquals(expected, output);		
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// echo, fmt, pipe
	@Test
	public void testPipeWithEchoFmt() throws IOException {
		try {
			baos.reset();
			
			String input = "echo " + FIRST_LINE + " | fmt -w 10";
			shell.parseAndEvaluate(input, baos);
			String expected = "CS4218"
							+ System.lineSeparator()
							+ "Shell is a"
							+ System.lineSeparator()
							+ "command"
							+ System.lineSeparator()
							+ "interpreter"
							+ System.lineSeparator()
							+ "that"
							+ System.lineSeparator()
							+ "provides a"
							+ System.lineSeparator()
							+ "set of"
							+ System.lineSeparator()
							+ "tools"
							+ System.lineSeparator()
							+ "(applications):"
							+ System.lineSeparator()
							+ System.lineSeparator();

			String output = new String(baos.toByteArray());
			output = output.replaceAll("\\s", "");
			expected = expected.replaceAll("\\s+", "");
			//System.out.println(output);
			assertEquals(expected, output);			
		} catch (ShellException e) {
			//System.out.println(e.toString());
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// head, pipe
	@Test
	public void testPipeWithHeadHead() throws IOException {
		try {
			baos.reset();
			String input = "head -n 3 " + TEST_FILE_NAME + " | head -n 2";
			shell.parseAndEvaluate(input, baos);
			String expected = "CS4218 Shell is a command interpreter that provides a set of tools " + "(applications): "
					+ System.lineSeparator() + "cd, pwd, ls, cat, echo, head, tail, grep, sed, find and wc. "
					+ System.lineSeparator();
			
			String output = new String(baos.toByteArray());
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// head, pipe
	@Test
	public void testPipeWithHeadTail() throws IOException {
		try {
			baos.reset();
			String input = "head -n 3 " + TEST_FILE_NAME + " | tail -n 1";
			shell.parseAndEvaluate(input, baos);
			String expected = "Apart from that, CS4218 Shell is a language for calling and combining these application. ";
			
			String output = new String(baos.toByteArray());
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// head, fmt, pipe
	@Test
	public void testPipeWithHeadFmt() throws IOException {
		try {
			baos.reset();
			String input = "head -n 1 " + TEST_FILE_NAME + " | fmt -w 10";
			shell.parseAndEvaluate(input, baos);
			String expected = "CS4218"
							+ System.lineSeparator()
							+ "Shell is a"
							+ System.lineSeparator()
							+ "command"
							+ System.lineSeparator()
							+ "interpreter"
							+ System.lineSeparator()
							+ "that"
							+ System.lineSeparator()
							+ "provides a"
							+ System.lineSeparator()
							+ "set of"
							+ System.lineSeparator()
							+ "tools"
							+ System.lineSeparator()
							+ "(applications):"
							+ System.lineSeparator()
							+ System.lineSeparator();
			
			String output = new String(baos.toByteArray());
			output = output.replaceAll("\\s+", "");
			expected = expected.replaceAll("\\s+", "");
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// sort, tail, pipe
	@Test
	public void testPipeWithTailSort() throws IOException {
		try {
			baos.reset();
			String input = "tail -n 3 " + TEST_FILE_NAME + " | sort";
			shell.parseAndEvaluate(input, baos);
			String expected =  "More details can be found in \"Project Description.pdf\" in IVLE." 
							+ System.lineSeparator() 
							+ "applications, command substitution and piping for connecting applications' inputs and "
							+ System.lineSeparator() 
							+ "outputs, IO-redirection to load and save data processed by applications from/to files. "
							+ System.lineSeparator();

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// date, fmt, pipe
	@Test
	public void testPipeWithDateFmt() throws IOException {
		try {
			baos.reset();
			String input = "date | fmt -w 10";
			shell.parseAndEvaluate(input, baos);
			Date currentT = Calendar.getInstance().getTime();
			String timeStamp = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy").format(currentT);
			ByteArrayInputStream inStream = new ByteArrayInputStream(timeStamp.getBytes());
			FmtApplication fmtApp = new FmtApplication();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			String[] args = new String[2];
			args[0] = "-w";
			args[1] = "10";
			fmtApp.run(args, inStream, outStream);				
			String expected = outStream.toString();		
			String output = new String(baos.toString());
			//System.out.println(expected + output);
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// sort, head, pipe
	@Test
	public void testPipeWithSortHead() throws IOException {
		try {
			baos.reset();
			String input = "sort " + TEST_FILE_NAME + " | head -n 1";
			shell.parseAndEvaluate(input, baos);
			String expected = "Apart from that, CS4218 Shell is a language for calling and combining these application. "
							+ System.lineSeparator();

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// bc, cat, pipe
	@Test
	public void testPipeWithBcCat() throws IOException {
		try {
			baos.reset();
			String input = "bc 1.5*2  | cat";
			shell.parseAndEvaluate(input, baos);
			String expected =  "3" + System.lineSeparator();

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// cal, head
	@Test
	public void testPipeWithCalHead() throws IOException {
		try {
			baos.reset();
			String input = "cal -m may 2016 | head -n 1";
			shell.parseAndEvaluate(input, baos);
			String expected = "   May 2016" + System.lineSeparator();

			String output = new String(baos.toByteArray());
			assertEquals(expected, output);			
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	////////////Chain of applications connected with at least two pipes//////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	@Test
	public void testPipeThreePipes() throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();

			String input = "cat " + TEST_FILE_NAME + "| head -n 1 | cat" ;
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE;
			
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");	
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}

	}

	// cat, head, tail, pipe
	@Test
	public void testPipeFourPipe() throws IOException, ShellException, AbstractApplicationException {
		try {
			baos.reset();
			String input = "cat " + TEST_FILE_NAME + " | head -n 1 | tail -n 1 | cat"   ;

			shell.parseAndEvaluate(input, baos);
			
			String expectedResult = FIRST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}	
	}

	// echo, cat, pipe, backquote
	@Test
	public void testMultiplePipeWithBQ() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "echo `cat " + TEST_FILE_NAME + " | cat | cat`";
			shell.parseAndEvaluate(input, baos);
			
			String expectedResult = TEST_STR;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n| ", "").replace("\r", "");
			result = result.replaceAll("\r|\n|\\s|\\s+", "");
			expectedResult = expectedResult.replaceAll("\r|\n|\\s|\\s+", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// echo, cat, pipe, semicolon
	@Test
	public void testMultiplePipeWithSemicolon() throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "echo showing contents of text1.txt ; cat " + TEST_FILE_NAME + "|cat | cat";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = "showing contents of text1.txt" + TEST_STR;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, head, pipe
	@Test
	public void testMultiplePipeWithCatAndSort() throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "cat " +TEST_FILE_NAME +" | sort | head -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = SORTED_FIRST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, head, tail, pipe
	@Test
	public void testMultiplePipeWithCatAndHeadAndTail()
			throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "head -n 1 " + TEST_FILE_NAME +" | cat | tail -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, tail, head, pipe
	@Test
	public void testMultiplePipeWithCatAndTailAndHead()
			throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "cat " + TEST_FILE_NAME +" | tail -n 1 |head -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = LAST_LINE;
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// cat, pipe [redundant?]
	@Test
	public void testMultiplePipeWithCat() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "cat " +TEST_FILE_NAME +" | cat | cat"   ;
			shell.parseAndEvaluate(input, baos);
			String expectedResult = TEST_STR;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}

	}

	// sort, tail, pipe
	@Test
	public void testMultiplePipeWithSortAndTail() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "sort " + TEST_FILE_NAME + "| tail -n 1 | tail -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = SORTED_LAST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n", "");
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// head, tail, sort, pipe
	@Test
	public void testMultiplePipeWithHeadAndTailAndSort()
			throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "head -n 1 "+ TEST_FILE_NAME+ " | tail -n 1 | sort";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}

	}

	// head, cat, pipe
	@Test
	public void testMultiplePipeWithHeadAndCat() throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "head -n 1 "+ TEST_FILE_NAME +" | cat | head -n 1 | cat";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// tail, cat, pipe
	@Test
	public void testMultiplePipeWithTailAndCat() throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "tail -n 1 "+ TEST_FILE_NAME +" | cat "+ TEST_FILE_NAME +" | tail -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = LAST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// head, sort, cat, pipe
	@Test
	public void testMultiplePipeWithHeadAndSortAndCat()
			throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "head -n 1 " +TEST_FILE_NAME+" | sort | cat";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = FIRST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// tail, sort, cat, pipe
	@Test
	public void testMultiplePipeWithTailAndSortAndCat()
			throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "tail -n 1 " + TEST_FILE_NAME +" | sort | cat";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = LAST_LINE;
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}
	
	// cal, head, cat, pipe
	@Test
	public void testMultiplePipeWithCalAndHeadAndCat() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "cal -m may 2016 | head -n 1 | cat" ;
			shell.parseAndEvaluate(input, baos);
			String expectedResult = "May 2016";
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replaceAll("\r|\n| ", "");
			result = result.replaceAll("\r|\n| ", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// bc, cat, pipe
	@Test
	public void testMultiplePipeWithBcAndCat() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "bc 1.5 | cat  | cat";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = "1.5";
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// bc, tail, pipe
	@Test
	public void testMultiplePipeWithBcAndTail() throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "bc 1.5 | tail -n 1 | tail -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = "1.5";
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// bc, head, pipe
	@Test
	public void testMultiplePipeWithBcAndhead() throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "bc 1.5 | head -n 1 | head -n 1";
			shell.parseAndEvaluate(input, baos);
			String expectedResult = "1.5";
			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			
			result = result.replaceAll("\r|\n", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// head, fmt, cat, pipe
	@Test
	public void testMultiplePipeWithfmtAndheadAndCat()
			throws IOException, AbstractApplicationException, ShellException {
		try {
			baos.reset();
			String input = "head -n 1 "+ TEST_FILE_NAME + " | fmt -w 1 | cat";
			shell.parseAndEvaluate(input, baos);

			String expectedResult = FIRST_LINE;

			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replace("\n", "").replace("\r", "").replace(" ", "");
			result = result.replaceAll("\r|\n| ", "");
			assertEquals(expectedResult, result);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}
	}

	// echo, head, cat, pipe
	@Test
	public void testMultiplePipeWithfmtAndtailAndCat()
			throws IOException, AbstractApplicationException, ShellException {

	//	try {
			baos.reset();
			String input = "echo `cat " +TEST_FILE_NAME +"|head -n 1`";
			shell.parseAndEvaluate(input, baos);

			String expectedResult = FIRST_LINE;

			byte[] byteArray = baos.toByteArray();
			String result = new String(byteArray).replace("\n", "").replace("\r", "");
			expectedResult = expectedResult.replace("\n", "").replace("\r", "").replace(" ", "");
			result = result.replaceAll("\r|\n| ", "");
			assertEquals(expectedResult, result);
	//	} catch (ShellException e) {
	//		fail(VALID_CMD_NO_EXP);
	//	} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
	//		fail(VALID_CMD_NO_EXP);
	//	}
	}

	// date, cat, head, pipe
	@Test
	public void testMultiplePipeWithDateAndCatAndHead()
			throws IOException, AbstractApplicationException, ShellException {

		try {
			baos.reset();
			String input = "date | cat | head -n 1";
			shell.parseAndEvaluate(input, baos);
		} catch (ShellException e) {
			fail(VALID_CMD_NO_EXP);
		} catch (AbstractApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(VALID_CMD_NO_EXP);
		}

	}

}